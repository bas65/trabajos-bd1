/*==============================================================*/
/* DBMS name:      ORACLE Version 19c                           */
/* Created on:     2/13/2022 7:24:32 PM                         */
/*==============================================================*/


alter table ACTOR
   drop constraint FK_ACTOR_RELATIONS_STUDENT;

alter table ACTORLIST
   drop constraint FK_ACTORLIS_RELATIONS_WORKGROU;

alter table ACTORLIST
   drop constraint FK_ACTORLIS_RELATIONS_ACTOR;

alter table ACTORSETTLEMENT
   drop constraint FK_ACTORSET_RELATIONS_ACTOR;

alter table CHARACTER
   drop constraint FK_CHARACTE_RELATIONS_ACTOR;

alter table EMPLOYEE
   drop constraint FK_EMPLOYEE_RELATIONS_PERSON;

alter table EMPLOYEELIST
   drop constraint FK_EMPLYEELI_RELATION_EMPLOYEE;

alter table EMPLOYEELIST
   drop constraint FK_EMPLOYEE_RELATIONS_WORKGROU;

alter table EMPLOYEESETTLEMENT
   drop constraint FK_EMPLOYEE_RELATIONS_FARE;

alter table EMPLOYEESETTLEMENT
   drop constraint FK_EMPLOYEESET_RELATI_EMPLOYEE;

alter table EXPENSELIST
   drop constraint FK_EXPENSEL_RELATIONS_PLAY;

alter table EXPENSELIST
   drop constraint FK_EXPENSEL_RELATIONS_EXPENSE;

alter table EXPENSELIST
   drop constraint FK_EXPENSEL_RELATIONS_SUPPLIER;

alter table FUNCTION
   drop constraint FK_FUNCTION_RELATIONS_MODALITY;

alter table FUNCTION
   drop constraint FK_FUNCTION_RELATIONS_THEATER;

alter table FUNCTION
   drop constraint FK_FUNCTION_RELATIONS_PLAY;

alter table PLAY
   drop constraint FK_PLAY_RELATIONS_TYPEPLAY;

alter table PLAY
   drop constraint FK_PLAY_RELATIONS_COUNTRY;

alter table PLAYWRIGHT
   drop constraint FK_PLAYWRIG_RELATIONS_PERSON;

alter table RELATIONSHIP_19
   drop constraint FK_RELATION_RELATIONS_PLAYWRIG;

alter table RELATIONSHIP_19
   drop constraint FK_PLAYWLISRELATION_PLAY;

alter table RELATIONSHIP_21
   drop constraint FK_CHARACTLIST_RELATION_PLAY;

alter table RELATIONSHIP_21
   drop constraint FK_RELATION_RELATIONS_CHARACTE;

alter table STUDENT
   drop constraint FK_STUDENT_RELATIONS_PERSON;

alter table WORKGROUP
   drop constraint FK_WORKGROU_RELATIONS_FUNCTION;

drop index RELATIONSHIP_8_FK;

drop table ACTOR cascade constraints;

drop index RELATIONSHIP_13_FK;

drop index RELATIONSHIP_12_FK;

drop table ACTORLIST cascade constraints;

drop index RELATIONSHIP_7_FK;

drop table ACTORSETTLEMENT cascade constraints;

drop index RELATIONSHIP_22_FK;

drop table CHARACTER cascade constraints;

drop table COUNTRY cascade constraints;

drop index RELATIONSHIP_15_FK;

drop table EMPLOYEE cascade constraints;

drop index RELATIONSHIP_11_FK;

drop index RELATIONSHIP_10_FK;

drop table EMPLOYEELIST cascade constraints;

drop index RELATIONSHIP_6_FK;

drop index RELATIONSHIP_5_FK;

drop table EMPLOYEESETTLEMENT cascade constraints;

drop table EXPENSE cascade constraints;

drop index RELATIONSHIP_26_FK;

drop index RELATIONSHIP_25_FK;

drop index RELATIONSHIP_24_FK;

drop table EXPENSELIST cascade constraints;

drop table FARE cascade constraints;

drop index RELATIONSHIP_23_FK;

drop index RELATIONSHIP_4_FK;

drop index RELATIONSHIP_3_FK;

drop table FUNCTION cascade constraints;

drop table MODALITY cascade constraints;

drop table PERSON cascade constraints;

drop index RELATIONSHIP_20_FK;

drop index RELATIONSHIP_17_FK;

drop table PLAY cascade constraints;

drop index RELATIONSHIP_18_FK;

drop table PLAYWRIGHT cascade constraints;

drop index RELATIONSHIP_19_FK;

drop index RELATIONSHIP_27_FK;

drop table RELATIONSHIP_19 cascade constraints;

drop index RELATIONSHIP_21_FK;

drop index RELATIONSHIP_28_FK;

drop table RELATIONSHIP_21 cascade constraints;

drop index RELATIONSHIP_9_FK;

drop table STUDENT cascade constraints;

drop table SUPPLIER cascade constraints;

drop table THEATER cascade constraints;

drop table TYPEPLAY cascade constraints;

drop index RELATIONSHIP_14_FK;

drop table WORKGROUP cascade constraints;

/*==============================================================*/
/* Table: ACTOR                                                 */
/*==============================================================*/
create table ACTOR (
   IDACTOR              NUMBER(4)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   CODE                 NUMBER(11)            not null,
   NUMBERFUCTIONS       NUMBER(3)             not null,
   NUMBERREHEARSALS     NUMBER(3)             not null,
   constraint PK_ACTOR primary key (IDACTOR)
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_8_FK on ACTOR (
   CODE ASC
);

/*==============================================================*/
/* Table: ACTORLIST                                             */
/*==============================================================*/
create table ACTORLIST (
   IDACTORLIST          NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDWORKGROUPFKACLI    INTEGER               not null,
   IDACTORFKACLI        INTEGER               not null,
   constraint PK_ACTORLIST primary key (IDACTORLIST)
);

/*==============================================================*/
/* Index: RELATIONSHIP_12_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_12_FK on ACTORLIST (
   IDWORKGROUPFKACLI ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_13_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_13_FK on ACTORLIST (
   IDACTORFKACLI ASC
);

/*==============================================================*/
/* Table: ACTORSETTLEMENT                                       */
/*==============================================================*/
create table ACTORSETTLEMENT (
   IDACTORSETTLEMENT    NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDACTORFKACSET       INTEGER               not null,
   ACTORPAYMENT         NUMBER(8,0),
   constraint PK_ACTORSETTLEMENT primary key (IDACTORSETTLEMENT)
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_7_FK on ACTORSETTLEMENT (
   IDACTORFKACSET ASC
);

/*==============================================================*/
/* Table: CHARACTER                                             */
/*==============================================================*/
create table CHARACTER (
   IDCHARACTER          NUMBER(5)             not null,
   IDACTOR              INTEGER               not null,
   CHARACTERNAME        VARCHAR2(50)          not null,
   constraint PK_CHARACTER primary key (IDCHARACTER)
);

/*==============================================================*/
/* Index: RELATIONSHIP_22_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_22_FK on CHARACTER (
   IDACTOR ASC
);

/*==============================================================*/
/* Table: COUNTRY                                               */
/*==============================================================*/
create table COUNTRY (
   COUNTRYCODE          VARCHAR2(8)           not null,
   COUNTRYNAME          VARCHAR2(30)          not null,
   constraint PK_COUNTRY primary key (COUNTRYCODE)
);

/*==============================================================*/
/* Table: EMPLOYEE                                              */
/*==============================================================*/
create table EMPLOYEE (
   IDNUMBERFKEMP        NUMBER(20)            not null,
   IDEMPLOYEE           NUMBER(3)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   HOURSWORKED          NUMBER(3),
   ROLEEMPLOYEE         VARCHAR2(20)          not null,
   constraint PK_EMPLOYEE primary key (IDEMPLOYEE)
);

/*==============================================================*/
/* Index: RELATIONSHIP_15_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_15_FK on EMPLOYEE (
   IDNUMBERFKEMP ASC
);

/*==============================================================*/
/* Table: EMPLOYEELIST                                          */
/*==============================================================*/
create table EMPLOYEELIST (
   IDEMPLOYEELIST       NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDEMPLOYEEFKEMPLIS   INTEGER               not null,
   IDWORKGROUPFKEMPLIS  INTEGER               not null,
   constraint PK_EMPLOYEELIST primary key (IDEMPLOYEELIST)
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_10_FK on EMPLOYEELIST (
   IDEMPLOYEEFKEMPLIS ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_11_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_11_FK on EMPLOYEELIST (
   IDWORKGROUPFKEMPLIS ASC
);

/*==============================================================*/
/* Table: EMPLOYEESETTLEMENT                                    */
/*==============================================================*/
create table EMPLOYEESETTLEMENT (
   IDEMPLOYEESETTLEMENT NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDFAREFKEMPSET       INTEGER               not null,
   IDEMPLOYEEFKEMPSET   INTEGER               not null,
   EMPLOYEEPAYMENT      NUMBER(8,0),
   constraint PK_EMPLOYEESETTLEMENT primary key (IDEMPLOYEESETTLEMENT)
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_5_FK on EMPLOYEESETTLEMENT (
   IDFAREFKEMPSET ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_6_FK on EMPLOYEESETTLEMENT (
   IDEMPLOYEEFKEMPSET ASC
);

/*==============================================================*/
/* Table: EXPENSE                                               */
/*==============================================================*/
create table EXPENSE (
   IDEXPENSE            NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   EXPENSEDESCRIPTION   NVARCHAR2(50)         not null,
   CUANTITY             NUMBER(3,0)           not null,
   TYPEEXPENSE          VARCHAR2(30),
   constraint PK_EXPENSE primary key (IDEXPENSE)
);

/*==============================================================*/
/* Table: EXPENSELIST                                           */
/*==============================================================*/
create table EXPENSELIST (
   IDEXPENSELIST        NUMBER(4)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDPLAY               INTEGER               not null,
   IDEXPENSE            INTEGER               not null,
   IDSUPPLIER           INTEGER               not null,
   VALUEEXPENSE         NUMBER(8,2)           not null,
   constraint PK_EXPENSELIST primary key (IDEXPENSELIST)
);

/*==============================================================*/
/* Index: RELATIONSHIP_24_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_24_FK on EXPENSELIST (
   IDPLAY ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_25_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_25_FK on EXPENSELIST (
   IDEXPENSE ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_26_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_26_FK on EXPENSELIST (
   IDSUPPLIER ASC
);

/*==============================================================*/
/* Table: FARE                                                  */
/*==============================================================*/
create table FARE (
   IDFARE               NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   HOURLYRATE           NUMBER(8)             not null,
   NAMEACTIVITY         VARCHAR2(50)          not null,
   constraint PK_FARE primary key (IDFARE)
);

/*==============================================================*/
/* Table: FUNCTION                                              */
/*==============================================================*/
create table FUNCTION (
   IDFUNCTION           NUMBER(5)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDTHEATERFKFUN       INTEGER               not null,
   IDPLAYFKFUN          INTEGER               not null,
   IDMODALITY           INTEGER               not null,
   DATEFUNCTION         DATE                  not null,
   STARTTIME            DATE                  not null,
   ENDTIME              DATE                  not null,
   TYPEFUNCTION         VARCHAR2(12)          not null,
   constraint PK_FUNCTION primary key (IDFUNCTION)
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_3_FK on FUNCTION (
   IDTHEATERFKFUN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_4_FK on FUNCTION (
   IDPLAYFKFUN ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_23_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_23_FK on FUNCTION (
   IDMODALITY ASC
);

/*==============================================================*/
/* Table: MODALITY                                              */
/*==============================================================*/
create table MODALITY (
   IDMODALITY           NUMBER(2)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   TYPEFUNCTION         VARCHAR2(12)          not null,
   constraint PK_MODALITY primary key (IDMODALITY)
);

/*==============================================================*/
/* Table: PERSON                                                */
/*==============================================================*/
create table PERSON (
   IDNUMBER             NUMBER(20)            not null,
   NAMES                VARCHAR2(40)          not null,
   SURNAME              VARCHAR2(40)          not null,
   IDTYPE               VARCHAR2(20)          not null,
   EMAIL                VARCHAR2(70)          not null,
   BIRTH                DATE,
   constraint PK_PERSON primary key (IDNUMBER)
);

/*==============================================================*/
/* Table: PLAY                                                  */
/*==============================================================*/
create table PLAY (
   IDPLAY               NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDTYPEPLAY           INTEGER               not null,
   COUNTRYCODE          VARCHAR2(8)           not null,
   TITLE                VARCHAR2(80)          not null,
   RELEASEDATE          DATE                  not null,
   constraint PK_PLAY primary key (IDPLAY)
);

/*==============================================================*/
/* Index: RELATIONSHIP_17_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_17_FK on PLAY (
   IDTYPEPLAY ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_20_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_20_FK on PLAY (
   COUNTRYCODE ASC
);

/*==============================================================*/
/* Table: PLAYWRIGHT                                            */
/*==============================================================*/
create table PLAYWRIGHT (
   IDNUMBER             NUMBER(20)            not null,
   IDPLAYWRIGHT         NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   constraint PK_PLAYWRIGHT primary key (IDPLAYWRIGHT)
);

/*==============================================================*/
/* Index: RELATIONSHIP_18_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_18_FK on PLAYWRIGHT (
   IDNUMBER ASC
);

/*==============================================================*/
/* Table: RELATIONSHIP_19                                       */
/*==============================================================*/
create table RELATIONSHIP_19 (
   IDPLAYWRIGHT         INTEGER               not null,
   IDPLAY               INTEGER               not null,
   constraint AK_PLAYWRIGHTLISTPK_RELATION unique (IDPLAY, IDPLAYWRIGHT)
);

/*==============================================================*/
/* Index: RELATIONSHIP_27_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_27_FK on RELATIONSHIP_19 (
   IDPLAY ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_19_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_19_FK on RELATIONSHIP_19 (
   IDPLAYWRIGHT ASC
);

/*==============================================================*/
/* Table: RELATIONSHIP_21                                       */
/*==============================================================*/
create table RELATIONSHIP_21 (
   IDPLAY               INTEGER               not null,
   IDCHARACTER          INTEGER               not null,
   constraint AK_CHARACTERLISTPK_RELATION unique (IDCHARACTER, IDPLAY)
);

/*==============================================================*/
/* Index: RELATIONSHIP_28_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_28_FK on RELATIONSHIP_21 (
   IDCHARACTER ASC
);

/*==============================================================*/
/* Index: RELATIONSHIP_21_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_21_FK on RELATIONSHIP_21 (
   IDPLAY ASC
);

/*==============================================================*/
/* Table: STUDENT                                               */
/*==============================================================*/
create table STUDENT (
   IDNUMBER             NUMBER(20)            not null,
   CODE                 NUMBER(11)            not null,
   CAREER               VARCHAR2(25)          not null,
   constraint PK_STUDENT primary key (CODE)
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create index RELATIONSHIP_9_FK on STUDENT (
   IDNUMBER ASC
);

/*==============================================================*/
/* Table: SUPPLIER                                              */
/*==============================================================*/
create table SUPPLIER (
   IDSUPPLIER           NUMBER(3)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   SUPPLIERNAME         VARCHAR2(20)          not null,
   constraint PK_SUPPLIER primary key (IDSUPPLIER)
);

/*==============================================================*/
/* Table: THEATER                                               */
/*==============================================================*/
create table THEATER (
   IDTHEATER            NUMBER(3)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   "DESC"               VARCHAR2(200),
   NAMETHEATER          VARCHAR2(50)          not null,
   constraint PK_THEATER primary key (IDTHEATER)
);

/*==============================================================*/
/* Table: TYPEPLAY                                              */
/*==============================================================*/
create table TYPEPLAY (
   IDTYPEPLAY           NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   NAMETYPEPLAY         VARCHAR2(50)          not null,
   constraint PK_TYPEPLAY primary key (IDTYPEPLAY)
);

/*==============================================================*/
/* Table: WORKGROUP                                             */
/*==============================================================*/
create table WORKGROUP (
   IDWORKGROUP          NUMBER(6)           
      generated as identity ( start with 1 nocycle noorder)  not null,
   IDFUNCTIONFKWORGRO   INTEGER               not null,
   constraint PK_WORKGROUP primary key (IDWORKGROUP)
);

/*==============================================================*/
/* Index: RELATIONSHIP_14_FK                                    */
/*==============================================================*/
create index RELATIONSHIP_14_FK on WORKGROUP (
   IDFUNCTIONFKWORGRO ASC
);

alter table ACTOR
   add constraint FK_ACTOR_RELATIONS_STUDENT foreign key (CODE)
      references STUDENT (CODE);

alter table ACTORLIST
   add constraint FK_ACTORLIS_RELATIONS_WORKGROU foreign key (IDWORKGROUPFKACLI)
      references WORKGROUP (IDWORKGROUP);

alter table ACTORLIST
   add constraint FK_ACTORLIS_RELATIONS_ACTOR foreign key (IDACTORFKACLI)
      references ACTOR (IDACTOR);

alter table ACTORSETTLEMENT
   add constraint FK_ACTORSET_RELATIONS_ACTOR foreign key (IDACTORFKACSET)
      references ACTOR (IDACTOR);

alter table CHARACTER
   add constraint FK_CHARACTE_RELATIONS_ACTOR foreign key (IDACTOR)
      references ACTOR (IDACTOR);

alter table EMPLOYEE
   add constraint FK_EMPLOYEE_RELATIONS_PERSON foreign key (IDNUMBERFKEMP)
      references PERSON (IDNUMBER);

alter table EMPLOYEELIST
   add constraint FK_EMPLYEELI_RELATION_EMPLOYEE foreign key (IDEMPLOYEEFKEMPLIS)
      references EMPLOYEE (IDEMPLOYEE);

alter table EMPLOYEELIST
   add constraint FK_EMPLOYEE_RELATIONS_WORKGROU foreign key (IDWORKGROUPFKEMPLIS)
      references WORKGROUP (IDWORKGROUP);

alter table EMPLOYEESETTLEMENT
   add constraint FK_EMPLOYEE_RELATIONS_FARE foreign key (IDFAREFKEMPSET)
      references FARE (IDFARE);

alter table EMPLOYEESETTLEMENT
   add constraint FK_EMPLOYEESET_RELATI_EMPLOYEE foreign key (IDEMPLOYEEFKEMPSET)
      references EMPLOYEE (IDEMPLOYEE);

alter table EXPENSELIST
   add constraint FK_EXPENSEL_RELATIONS_PLAY foreign key (IDPLAY)
      references PLAY (IDPLAY);

alter table EXPENSELIST
   add constraint FK_EXPENSEL_RELATIONS_EXPENSE foreign key (IDEXPENSE)
      references EXPENSE (IDEXPENSE);

alter table EXPENSELIST
   add constraint FK_EXPENSEL_RELATIONS_SUPPLIER foreign key (IDSUPPLIER)
      references SUPPLIER (IDSUPPLIER);

alter table FUNCTION
   add constraint FK_FUNCTION_RELATIONS_MODALITY foreign key (IDMODALITY)
      references MODALITY (IDMODALITY);

alter table FUNCTION
   add constraint FK_FUNCTION_RELATIONS_THEATER foreign key (IDTHEATERFKFUN)
      references THEATER (IDTHEATER);

alter table FUNCTION
   add constraint FK_FUNCTION_RELATIONS_PLAY foreign key (IDPLAYFKFUN)
      references PLAY (IDPLAY);

alter table PLAY
   add constraint FK_PLAY_RELATIONS_TYPEPLAY foreign key (IDTYPEPLAY)
      references TYPEPLAY (IDTYPEPLAY);

alter table PLAY
   add constraint FK_PLAY_RELATIONS_COUNTRY foreign key (COUNTRYCODE)
      references COUNTRY (COUNTRYCODE);

alter table PLAYWRIGHT
   add constraint FK_PLAYWRIG_RELATIONS_PERSON foreign key (IDNUMBER)
      references PERSON (IDNUMBER);

alter table RELATIONSHIP_19
   add constraint FK_RELATION_RELATIONS_PLAYWRIG foreign key (IDPLAYWRIGHT)
      references PLAYWRIGHT (IDPLAYWRIGHT);

alter table RELATIONSHIP_19
   add constraint FK_PLAYWLISRELATION_PLAY foreign key (IDPLAY)
      references PLAY (IDPLAY);

alter table RELATIONSHIP_21
   add constraint FK_CHARACTLIST_RELATION_PLAY foreign key (IDPLAY)
      references PLAY (IDPLAY);

alter table RELATIONSHIP_21
   add constraint FK_RELATION_RELATIONS_CHARACTE foreign key (IDCHARACTER)
      references CHARACTER (IDCHARACTER);

alter table STUDENT
   add constraint FK_STUDENT_RELATIONS_PERSON foreign key (IDNUMBER)
      references PERSON (IDNUMBER);

alter table WORKGROUP
   add constraint FK_WORKGROU_RELATIONS_FUNCTION foreign key (IDFUNCTIONFKWORGRO)
      references FUNCTION (IDFUNCTION);

