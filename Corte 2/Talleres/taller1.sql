-- 1) Liste toda la informacion sobre los departamentos
SELECT * FROM S_DEPT;
-- 2) Liste toda la informacion sobre las regiones
SELECT * FROM S_REGION;
-- 3) Liste los fiferentes nombres de departamentos
SELECT NAME FROM S_DEPT;
-- 4) Liste los cargos(titles) de la tabla empleado
SELECT title FROM S_EMP;
-- 5) Liste los diferentes cargos de la tabla empleado
SELECT DISTINCT title FROM S_EMP;
-- 6) Liste los nombres de los departamentos bajo la etiqueta "Dptos diferentes"
/*SELECT name as "Dptos diferentes" FROM S_DEPT;*/
SELECT name "Dptos diferentes" FROM S_DEPT;
-- 7) Liste los nombres completos de los empleados
/* SELECT first_name||last_name FROM S_EMP;*/
/* SELECT first_name ||' '||last_name FROM S_EMP;*/
SELECT (CONCAT(first_name,CONCAT(' ',last_name))) FROM S_EMP;
-- 8) Liste el Id del departamento, el primero y segundo nombre de los empleados del dpto 40
SELECT DISTINCT S_DEPT.ID, S_EMP.first_name,S_EMP.last_name FROM S_DEPT,S_EMP WHERE S_DEPT.ID=40 AND S_EMP.dept_id=40;
/* 9) Liste el apellido, el salario, el porcentaje de comisión y el valor de la comisión de cada uno de los 
Representantes de ventas. Asumiendo que la comisión es el porcentaje sobre el salario básico. */
SELECT S_EMP.last_name, S_EMP.salary, S_EMP.commission_pct,(S_EMP.salary*nvl(S_EMP.commission_pct,0))/100 "valor comision" FROM S_EMP WHERE S_EMP.title = 'Sales Representative';
/* 10) Liste el apellido, el salario, el porcentaje de comision y el total de la comision de cada uno de los
Representantes de ventas, cuya comision excede $200, ordenado por comision */
SELECT S_EMP.last_name, S_EMP.salary, S_EMP.commission_pct,(S_EMP.salary*nvl(S_EMP.commission_pct,0))/100 "total comision" FROM S_EMP WHERE S_EMP.title = 'Sales Representative' AND (S_EMP.salary*nvl(S_EMP.commission_pct,0))/100 > 200 order by (S_EMP.salary*nvl(S_EMP.commission_pct,0))/100 ASC;
/* 11) Liste el apellido, el salario y la compensacion anual de los empleados del departamento 40.
Dicha compensacion equivale a 12 veces el salario + 100*/
SELECT S_EMP.last_name, S_EMP.salary, (12*S_EMP.salary)+100 "compensacion anual" FROM S_EMP WHERE S_EMP.dept_id = 40;
-- 12) Para los empleados del dpto 50, liste el apellido, el salario dividido entre 22 redondeado a 0 decimales.
SELECT S_EMP.last_name, S_EMP.salary, round(S_EMP.salary/22,0) FROM S_EMP WHERE S_EMP.dept_id = 50;
-- 13) Liste el apellido, salario, porcentaje comision de los empleados con salario superior a 1500
SELECT S_EMP.last_name, S_EMP.salary, S_EMP.commission_pct FROM S_EMP WHERE S_EMP.salary > 1500;
-- 14) El mismo listado anterior pero convierta los nulos en 0
SELECT S_EMP.last_name, S_EMP.salary, nvl(S_EMP.commission_pct,0) "porcentaje" FROM S_EMP WHERE S_EMP.salary > 1500;
-- 15) Liste el apellido, la fecha de ingreso, la fecha de ingreso + 90 de los empleados del depertamento 42
SELECT S_EMP.last_name,S_EMP.start_date, S_EMP.start_date + 90 FROM S_EMP WHERE S_EMP.dept_id = 40;
-- 16) De acuerdo al anterior resultado, liste el apellido y el numero de semanas trabajadas del empleado del dpto 43
SELECT S_EMP.last_name, round(((S_EMP.start_date + 90) - S_EMP.start_date)/7) "semanas trabajadas" FROM S_EMP WHERE S_EMP.dept_id = 40;
-- 17) Para todas las ordenes, liste el ID, la fecha de entrada y el numero de dias transcurridos desde que hizo la orden
SELECT S_ORD.id, S_ORD.date_ordered, S_ORD.date_shipped-S_ORD.date_ordered "dias" FROM S_ORD;
-- 18) Para los empleados del dpto 45, liste el apellido, la fecha de entrada y 6 meses despues de la fecha de entrada.
SELECT S_EMP.last_name, S_EMP.start_date, add_months(S_EMP.start_date,6) "6 meses dp"FROM S_EMP WHERE S_EMP.dept_id = 40;
-- 19) Para cada uno de los empleados, liste el ID, la fecha de entrada y la fecha en que recibio su primer cheque (se paga ultimo dia del mes)
SELECT S_EMP.id, S_EMP.start_date, LAST_DAY(S_EMP.start_date) "recibio cheque" FROM S_EMP;
/* 20) Para cada uno de los empleados liste el ID, la fecha de ingreso y el # de meses transcurridos desde que el empleado ingreso*/
SELECT S_EMP.id, S_EMP.start_date, round(months_between(sysdate,S_EMP.start_date)) "meses transcurrido" FROM S_EMP,DUAL;
/* 21) Liste el Id y la fecha de todas las ordenes de pedidos con un formato “08/92” y 
un label Orden de los representantes de ventas 11*/
SELECT S_ORD.id, to_char(S_ORD.date_ordered,'MM/YY') "Orden de los representantes de ventas 11" FROM S_ORD WHERE S_ORD.sales_rep_id=11;
-- 22) Liste los nombres y fechas de ingreso en formato “27 de Febrero de 2001"
SELECT S_EMP.first_name||' '||S_EMP.last_name,to_char(S_EMP.start_date,'DD " de "MONTH" de "YYYY') "formato" FROM S_EMP;
-- 23)  Liste el cargo de los Vicepresidentes con su nombre y apellido (La primera en mayúscula) 
SELECT SUBSTR(S_EMP.title,5) "cargo",S_EMP.first_name, S_EMP.last_name FROM S_EMP WHERE SUBSTR(S_EMP.title,0,2) = 'VP';
-- 24) Liste el nombre y el apellido de los empleados con apellido PATEL
SELECT S_EMP.first_name,S_EMP.last_name FROM S_EMP WHERE S_EMP.last_name = initcap(lower('PATEL'));
-- 25) Liste los nombres de los productos, cuyas primeras tres letras con ‘ACE
SELECT S_PRODUCT.name FROM S_PRODUCT WHERE SUBSTR(S_PRODUCT.name,0,3) = 'Ace';
/* 26) Seleccione el string ‘departamento, nombre completo del vicepresidente’ de los vicepresidentes de la compañía. Ejm 
Operations, LaDoris Ngao.*/
SELECT SUBSTR(S_EMP.title,5)||', '||S_EMP.first_name||' '|| S_EMP.last_name||'.' "string" FROM S_EMP WHERE SUBSTR(S_EMP.title,0,2) = 'VP';
-- 27) Para cada uno de los empleados del departamento 50, selecciones el apellido y el número de caracteres del apellido.
SELECT S_EMP.last_name, length(S_EMP.last_name) "# caracteres apellido" FROM S_EMP WHERE S_EMP.dept_id=50;
-- 28) Liste los nombres de productos con letra en mayúscula.
SELECT upper(S_PRODUCT.name) FROM S_PRODUCT;
-- 29) Liste los nombres y apellidos de los empleados que contengan una “s” y una “a” en el apellido o el nombre
SELECT S_EMP.first_name, S_EMP.last_name FROM S_EMP WHERE (instr(lower(S_EMP.last_name), 'a', 1, 1) != 0 AND instr(lower(S_EMP.last_name), 's', 1, 1) != 0) OR (instr(lower(S_EMP.first_name), 'a', 1, 1) != 0 AND instr(lower(S_EMP.first_name), 's', 1, 1) != 0);
-- 30)  Liste los nombres y apellido de los empleados que no contengan una “a” en el apellido y el nombre.
SELECT S_EMP.first_name, S_EMP.last_name FROM S_EMP WHERE instr(S_EMP.last_name, 'a', 1, 1) = 0 AND instr(lower(S_EMP.last_name), 'a', 1, 1) = 0 AND instr(lower(S_EMP.first_name), 'a', 1, 1) = 0;
-- 31) Liste los nombres y apellidos de los empleados que no ganan comisión
SELECT S_EMP.first_name, S_EMP.last_name FROM S_EMP WHERE S_EMP.commission_pct is null;
-- 32) Liste los nombres y apellidos de los empleados que ganan comisión
SELECT S_EMP.first_name, S_EMP.last_name FROM S_EMP WHERE S_EMP.commission_pct is not null;
-- 33) Liste los nombres y apellidos de los empleados que ganan más de US 540
SELECT S_EMP.first_name, S_EMP.last_name FROM S_EMP WHERE S_EMP.salary > 540;
-- 34) Liste los nombres y apellidos de los vicepresidentes que ganan por lo menos US760
SELECT S_EMP.first_name, S_EMP.last_name FROM S_EMP WHERE SUBSTR(S_EMP.title,0,2) = 'VP' AND S_EMP.salary >760;
-- 35) Generar un listado con “código, nombre y apellido ……………………….. sueldo
SELECT S_EMP.id, S_EMP.first_name||' '|| S_EMP.last_name,'……………………….. '||S_EMP.salary "sueldo" FROM S_EMP;
/* 36) Generar un listado con “nombre apellido nuevaClave”, donde nuevaClave corresponde a el apellido cambiando 
la “a” por el “1”, la “e” por el “2”, la “i” por el “3”, la “o” por el “4”, la “u” por el “5”.*/
-- SELECT S_EMP.first_name, S_EMP.last_name,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(S_EMP.last_name,'a',1),'e',2),'i',3),'o',4),'u',5) "nueva clave" FROM S_EMP;
SELECT S_EMP.first_name, S_EMP.last_name, TRANSLATE(S_EMP.last_name,'aeiou','12345') "Nueva clase" FROM S_EMP;
-- 37) Generar un listado con los clientes que tengan una "s" en la octava posición
SELECT S_CUSTOMER.name FROM S_CUSTOMER lower WHERE (SUBSTR(S_CUSTOMER.name,8,1))='s';
-- 38) Generar un listado indicando los clientes que ganan y los que no ganan comisión
SELECT S_EMP.first_name, S_EMP.last_name, nvl2(S_EMP.commission_pct,'Ganan comision','No ganan comision') FROM S_EMP;