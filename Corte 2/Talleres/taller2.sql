-- Andres Felipe Wilches Torres - 20172020114
-- 1) Liste el apellido, el id del departamento y el nombre del departamento de todos los empleados
SELECT DISTINCT S_EMP.last_name, S_EMP.dept_Id, S_DEPT.name FROM S_EMP JOIN S_DEPT ON S_EMP.dept_Id = S_DEPT.ID;
-- 2) Liste el id del departamento, el id de la region y el nombre de la region de todos los departamentos
SELECT S_DEPT.id, S_REGION.ID, S_REGION.name FROM S_REGION JOIN S_DEPT ON S_DEPT.region_id = S_REGION.id;
-- 3) Liste el apellido, el id del depertamento y el nombre del departamento de los empleados con apellido 'Menchu'
SELECT DISTINCT S_EMP.last_name, S_EMP.dept_Id,S_DEPT.name FROM S_DEPT JOIN S_EMP ON S_EMP.last_name like '%Menchu%' AND S_DEPT.id = S_EMP.dept_Id;
/* 4) Liste el id de la region, el nombre de la region, el ide del departamento y el nombre del
departamento de todos los departamentos que pertenecen a la region de 'North America'*/
SELECT S_REGION.id "region id", S_REGION.name "region name", S_DEPT.id "dept id", S_DEPT.name "dept name" FROM S_DEPT JOIN S_REGION ON S_REGION.name like '%North America%' AND S_DEPT.region_id = S_REGION.id;
-- 5) Liste los nombres de los representantes de ventas y de todos los clientes, el id y los nombres
SELECT S_EMP.first_name||' '||S_EMP.last_name, S_CUSTOMER.id, S_CUSTOMER.name FROM S_CUSTOMER JOIN S_EMP ON (S_EMP.id = S_CUSTOMER.sales_rep_id) AND S_EMP.title like '%Sales Representative%';
-- 6) Liste el id de los clientes, los nombres de los clientes y el id de las ordenes. De todos los clientes con o sin ordenes
SELECT S_CUSTOMER.id, S_CUSTOMER.name, S_ORD.id FROM S_CUSTOMER LEFT JOIN S_ORD ON S_CUSTOMER.id = S_ORD.CUSTOMER_ID;
-- 7) Selecciona los nombres de los productos con su fecha de recargue
SELECT S_PRODUCT.name, NVL2(S_INVENTORY.restock_date,to_char(S_INVENTORY.restock_date),'No tiene') "fecha recargue" FROM S_PRODUCT JOIN S_INVENTORY ON S_PRODUCT.ID = S_INVENTORY.product_id;
-- 8) Seleccionar los empleados con sus departamentos y regiones
SELECT S_EMP.first_name||' '||S_EMP.last_name "empleado", S_DEPT.name "departamento", S_REGION.name "region" FROM S_EMP JOIN S_DEPT ON S_EMP.dept_Id = S_DEPT.id JOIN S_REGION ON S_EMP.region_id = S_REGION.id AND S_DEPT.region_id = S_REGION.id;
-- 9) Seleccionar los productos con sus regiones
SELECT DISTINCT S_PRODUCT.name "producto", S_REGION.name "region" FROM S_PRODUCT JOIN S_INVENTORY ON S_PRODUCT.id = S_INVENTORY.product_id JOIN S_WAREHOUSE ON S_INVENTORY.warehouse_id = S_WAREHOUSE.id JOIN S_REGION ON S_WAREHOUSE.region_id = S_REGION.id; 
-- 10) Seleccionar los representantes de ventas que han hecho ordenes de items con mas de 500 unidades
SELECT DISTINCT S_EMP.first_name, S_EMP.last_name FROM S_EMP JOIN S_ORD ON S_EMP.id = S_ORD.sales_rep_id AND S_EMP.title like '%Sales Representative%' JOIN S_ITEM ON S_ORD.id = S_ITEM.ord_id AND S_ITEM.quantity > 500;
-- 11) No se hace
-- 12) Seleccionar los productos que se han vendido y cuentan con un stock de mas de 130 unidades
SELECT DISTINCT S_PRODUCT.name FROM S_PRODUCT  JOIN S_INVENTORY ON S_PRODUCT.id = S_INVENTORY.product_id AND S_INVENTORY.amount_in_stock >130 JOIN S_ITEM ON S_INVENTORY.product_id = S_ITEM.product_id;
-- 13) Seleccionar los nombres completos de los representantes de ventas
SELECT S_EMP.first_name||' '||S_EMP.last_name "representantes de ventas" FROM S_EMP WHERE S_EMP.title like '%Sales Representative%';
-- 14) Seleccionar los nombre completos de los empleados que no son representantes de ventas
SELECT S_EMP.first_name||' '||S_EMP.last_name "representantes de ventas" FROM S_EMP WHERE S_EMP.title not like '%Sales Representative%';
-- 15) Seleccionar los nombres completos y cargo tanto de los empleados con sus subalternos (listar empleado y subalterno)
SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo",S_EMP2.first_name||' '||S_EMP2.last_name "Jefe", S_EMP2.title "posicion" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;
-- 16) Seleccionar los nombres completos y cargo de los empleados que tienen subalternos
SELECT DISTINCT S_EMP2.first_name||' '||S_EMP2.last_name "Jefe", S_EMP2.title "posicion" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;
-- 17) Seleccionar los nombres completos y cargos de todos los empleados con sus subalternos tengan o no tengan subalternos
SELECT S_EMP.first_name||' '||S_EMP.last_name "Nombre", S_EMP.title "Posicion",S_EMP2.first_name||' '||S_EMP2.last_name "Subalterno", S_EMP2.title "posicion" FROM S_EMP LEFT JOIN S_EMP S_EMP2 ON S_EMP.id = S_EMP2.Manager_id;
-- 18) Seleccionar los nombres completos y cargos de todos los empleados que no tienen subalternos
SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_EMP LEFT JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id MINUS SELECT DISTINCT S_EMP2.first_name||' '||S_EMP2.last_name "Jefe", S_EMP2.title "posicion" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;
-- 19) Seleccionar los nombres completos y cargo de los subalternos que no tienen jefes
-- SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_emp WHERE S_EMP.Manager_id is null;
SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_EMP LEFT JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id MINUS SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;
-- 20) Seleccionar los productos que no estan en el inventario
SELECT DISTINCT S_PRODUCT.name FROM S_PRODUCT JOIN S_INVENTORY ON S_PRODUCT.id = S_INVENTORY.product_id AND S_INVENTORY.amount_in_stock = 0;
-- 21) Seleccionar los productos que nunca se han pedido
SELECT DISTINCT S_PRODUCT.name FROM S_PRODUCT JOIN S_ITEM ON S_PRODUCT.id = S_ITEM.product_id(+) AND S_ITEM.product_id is NULL;
-- 22) Seleccionar las regiones que no tienen productos en el inventario
SELECT DISTINCT S_REGION.name FROM S_REGION JOIN S_WAREHOUSE ON S_REGION.id = S_WAREHOUSe.region_id JOIN S_INVENTORY ON S_WAREHOUSE.id = S_INVENTORY.warehouse_id JOIN S_PRODUCT ON S_INVENTORY.product_id = S_PRODUCT.id AND S_INVENTORY.amount_in_stock = 0;
-- 23) No se hace
-- 24) Generar un listado de los empleados indicando si ganan o no comision
SELECT S_EMP.first_name, S_EMP.last_name, nvl2(S_EMP.commission_pct,'Ganan comision','No ganan comision') FROM S_EMP;