-- Taller 3
-- Andres Felipe Wilches Torres - 20172020114
-- 1) Liste el promedio de salario de todos los empleados
SELECT e.first_name || ' ' || e.last_name "Empleado", AVG(s.payment) AS "Salario Promedio"
FROM s_emp e, s_salary s
WHERE e.id = s.id
GROUP BY (e.first_name,e.last_name);
-- 2) Liste el m ximo salario de todos los empleados
SELECT e.first_name || ' ' || e.last_name "Empleado", MAX(s.payment) AS "Salario Maximo"
FROM s_emp e, s_salary s
WHERE e.id = s.id
GROUP BY e.first_name,e.last_name;
-- 3) Liste el n£mero de empleados que ganan comisi¢n
SELECT COUNT(e.id) "Empleados que ganan comision"
FROM s_emp e
WHERE e.commission_pct IS NOT NULL;
--  4) Liste el numero total de empleaos de la compañia
SELECT COUNT(e.id) AS "numero de empleados"
FROM s_emp e;
-- 5)( Liste el n£mero de item por orden
SELECT o.id AS Orden, COUNT(i.item_id) "# de item"
FROM s_item i, s_ord o
WHERE i.ord_id = o.id
GROUP BY o.id;
-- 6) Liste el n£mero de clientes por cada uno de los representantes de ventas
SELECT e.first_name ||' '|| e.last_name AS Representante, COUNT(c.id) AS "Numero de clientes"
FROM s_emp e, s_customer c
WHERE e.id = c.sales_rep_id
GROUP BY e.first_name, e.last_name;
-- 7) Liste el n£mero de departamentos por region
SELECT r.name AS Region, COUNT(d.id) "Numero de departartamentos"
FROM s_region r, s_dept d
WHERE d.region_id = r.id
GROUP BY r.name;
-- 8) Liste el salario (sum(salario) + sum( ventas)*(min(comisi¢n)/100)) de cada representante de ventas
SELECT e.first_name ||' '|| e.last_name AS Representante, (SUM(payment) + (SUM(o.total) * (MIN(e.commission_pct) / 100))) AS "salario"
FROM s_emp e, s_salary s, s_ord o
WHERE e.id = o.sales_rep_id AND e.id = s.id
GROUP BY e.first_name,e.last_name;
-- 9) Liste el promedio de salario por nombre de cargo
SELECT e.title AS "Cargo", AVG(s.payment) AS "Salario promedio"
FROM s_emp e, s_salary s
WHERE e.id = s.id
GROUP BY e.title;
-- 10) Liste por producto las veces que ha sido ordenado
SELECT p.name AS "Producto", COUNT(i.item_id) AS "veces ordenado"
FROM s_product p, s_item i
WHERE i.product_id = p.id
GROUP BY p.name;
-- 11) Liste el promedio de salario, el maximo, el total del salario y el numero de empleados de cada uno de los nombres de departamentos de la compañia
SELECT d.name AS "Departamento", AVG(s.payment) AS "Promedio", MAX(s.payment) AS "salario maximo", SUM(S.payment) AS "Total", COUNT(DISTINCT e.id) AS "# Empleados"
FROM s_emp e, s_salary s, s_dept d, s_region r
WHERE e.dept_id = d.id AND e.region_id = d.region_id AND e.id = s.id AND d.region_id = r.id
GROUP BY d.name;
-- 12) Liste el numero de clientes por calificacion del credito (credit rating).
SELECT credit_rating AS "rating", COUNT(id) AS "# clientes"
FROM s_customer
GROUP BY credit_rating;
-- 13) Liste el promedio de salario total por nombre departamento y nombre de cargo
SELECT d.name AS "Departamento", e.title AS "Cargo", AVG(s.payment) AS "Promedio"
FROM s_emp e, s_salary s, s_dept d
WHERE e.dept_id = d.id AND e.region_id = d.region_id AND e.id = s.id
GROUP BY d.name, e.title;
-- 14) Liste el promedio de salario total por nombre de cargo y nombre de departamento
SELECT e.title AS "Cargo", d.name AS "Departamento", AVG(s.payment) AS "Promedio salarial"
FROM s_emp e, s_salary s, s_dept d
WHERE e.dept_id = d.id AND e.region_id = d.region_id AND e.id = s.id
GROUP BY e.title, d.name;
-- 15) Liste el promedio anual del salario y numero de trabajadores de todos los cargos (nombres) que tienen mas de 2 empleados
SELECT e.title AS "Cargo", AVG(s.payment) AS "Promedio anual", COUNT(DISTINCT(e.id)) AS "# empleados"
FROM s_emp e, s_salary s
WHERE e.id = s.id
GROUP BY e.title, TO_CHAR(s.datepayment, 'YYYY')
HAVING COUNT(DISTINCT(e.id)) > 2;
-- 16) Liste la suma de salarios anuales y el n£mero de empleados de todos los departamentos (nombres) con mas de un empleado.
SELECT d.name AS "Departamento", SUM(s.payment) AS "Salario Anual", COUNT(DISTINCT(e.id)) AS "# Empleados"
FROM s_emp e, s_salary s, s_dept d
WHERE e.id = s.id AND e.dept_id = d.id AND e.region_id = d.region_id
GROUP BY d.name, TO_CHAR(s.datepayment, 'YYYY')
HAVING COUNT(DISTINCT(e.id)) > 1;
-- 17) Liste por cargo (nombres) el total de nomina anual superior a 5000. Con etiquetas
SELECT e.title AS "Cargo", SUM(s.payment) AS "Nomina Anual"
FROM s_emp e, s_salary s
WHERE e.id = s.id
GROUP BY e.title, TO_CHAR(s.datepayment, 'YYYY')
HAVING SUM(s.payment) > 5000;
-- 18) Liste los productos que han sido ordenados mas de dos veces (muestre el numero de veces)
SELECT p.name AS "Producto", COUNT(i.item_id) AS "#"
FROM s_product p, s_item i
WHERE i.product_id = p.id
GROUP BY p.name
HAVING COUNT(i.item_id) > 2;
-- 19) Liste el numero de clientes por region
SELECT r.name AS "Region", COUNT(*) AS "# Clientes"
FROM s_region r, s_customer c
WHERE r.id = c.region_id
GROUP BY r.name;
-- 20) Por region y producto ordenado liste los clientes que han hecho mas de dos ordenes
SELECT r.name AS "Region", p.name AS "Producto", c.name AS "Cliente"
FROM s_region r, s_customer c, s_ord o, s_item i, s_product p
WHERE p.id = i.product_id AND i.ord_id = o.id AND o.customer_id = c.id AND c.region_id = r.id
GROUP BY r.name, p.name, c.name
HAVING COUNT(DISTINCT(o.id)) > 2;
-- 21) Liste los empleados cuyo promedio anual de salario supera los 3000
SELECT e.first_name||' '||e.last_name AS "Nombre", AVG(s.payment) as "Salario"
FROM s_emp e, s_salary s
WHERE e.id = s.id
GROUP BY e.first_name,e.last_name,TO_CHAR(s.datepayment, 'YYYY')
HAVING AVG(s.payment) > 3000;

-- 22) Por orden liste el total de lvalor, el promedio del valor y el # de productos expedidos en 2011
SELECT o.id AS "Orden", SUM(o.total) AS "valor total", AVG(o.total) AS "Promedio del valor", COUNT(p.id) AS "# Producto", TO_CHAR(o.date_shipped, 'YYYY') AS "Fecha expedicion"
FROM s_product p, s_item i, s_ord o
WHERE p.id = i.product_id AND i.ord_id = o.id AND TO_CHAR(o.date_shipped,'YYYY') = 2011
GROUP BY o.id, TO_CHAR(o.date_shipped, 'YYYY');                   
