-- 1) Seleccione el nombre y apellido de todos los empleados que tienen el mismo cargo
-- que Carmen Velásquez
-- Subconsulta cargo Carmen Velasquez
SELECT emp.title as "Cargo" 
FROM S_EMP emp 
WHERE emp.first_name||' '||emp.last_name like 'Carmen Velasquez';
-- Select principal
SELECT e.first_name||' '||e.last_name AS "Empleados" 
FROM S_EMP e 
WHERE e.title = (SELECT emp.title as "Cargo" 
                FROM S_EMP emp 
                WHERE emp.first_name||' '||emp.last_name like 'Carmen Velasquez'); 

-- 2)Liste el apellido, el cargo y el Id del depto de todos los empleados que trabajan en el
--   mismo depto que Colin
-- Subconsulta departamento de Colin
SELECT DISTINCT dept.name 
FROM S_EMP emp, S_DEPT dept 
WHERE emp.dept_id = dept.id AND emp.first_name like 'Colin';
-- Select principal
SELECT DISTINCT e.last_name AS "apellido", e.title AS "Cargo", d.id AS "ID dpto"
FROM S_EMP e, S_DEPT d
WHERE e.dept_id = d.id AND d.name = (SELECT DISTINCT dept.name 
                FROM S_EMP emp, S_DEPT dept 
                WHERE emp.dept_id = dept.id AND emp.first_name like 'Colin');

-- 3) Liste los empleados que ganan el máximo salario
-- Subconsulta del maximo salario
SELECT max(sal.payment) AS "Salario"
FROM S_SALARY sal, S_EMP emp
WHERE emp.id = sal.id;
-- Select principal
SELECT e.first_name||' '||e.last_name AS "Empleado"
FROM S_EMP e,S_SALARY s
WHERE e.id = s.id AND s.payment = (SELECT max(sal.payment) AS "Salario"
                    FROM S_SALARY sal, S_EMP emp
                    WHERE emp.id = sal.id);

-- 4) Liste los empleados cuyo salario es al menos como el promedio de salario
-- Subconsulta promedio de salario
SELECT AVG(sal.payment) AS "Promedio salario"
FROM S_SALARY sal, S_EMP emp
WHERE emp.id = sal.id;
-- Select principal
SELECT DISTINCT e.first_name||' '||e.last_name AS "Empleado"
FROM S_SALARY s, S_EMP e
WHERE e.id = s.id AND s.payment >= (SELECT AVG(sal.payment) AS "Promedio salario"
                                    FROM S_SALARY sal, S_EMP emp
                                    WHERE emp.id = sal.id);

-- 5) Liste los departamentos cuyo promedio de salario es superior al promedio general
-- Subconsulta promedio general por Dpto
SELECT AVG(sal.payment)
FROM S_SALARY sal, S_EMP emp
WHERE sal.id = emp.id;
-- Select principal
SELECT d.name 
FROM S_DEPT d, S_EMP e, S_REGION r, S_SALARY s
WHERE e.id = s.id AND e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id
HAVING AVG(s.payment) >= (SELECT AVG(sal.payment)
                        FROM S_SALARY sal, S_EMP emp
                        WHERE sal.id = emp.id)                       
GROUP BY d.name;

-- 6) Liste los empleados que ganan el máximo salario por departamento.
-- Subconsulta maximo salario por departamento
SELECT dept.name dpto, max(sal.payment) maxSal
FROM S_DEPT dept, S_EMP emp, S_SALARY sal, S_REGION reg
WHERE emp.id = sal.id AND emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id
GROUP BY dept.name;
-- Select principal
SELECT DISTINCT e.first_name||' '||e.last_name AS "Empleados", d.name AS "Departamento"
FROM S_EMP e, S_SALARY s, S_DEPT d, (SELECT dept.name dpto, max(sal.payment) maxSal
                            FROM S_DEPT dept, S_EMP emp, S_SALARY sal, S_REGION reg
                            WHERE emp.id = sal.id AND emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id
                            GROUP BY dept.name) dpto 
WHERE e.id = s.id AND e.dept_id = d.id AND d.name = dpto.dpto AND s.payment = dpto.maxSal;

-- 7) Liste el ID, el apellido y el nombre del departamento de todos los empleados que
-- trabajan en un departamento que tengan al menos un empleado de apellido PATEL
-- Subconsulta con los departamentos que tengan un empleado de apellido PATEL
SELECT DISTINCT dept.name 
FROM S_DEPT dept, S_EMP emp, S_REGION reg
WHERE emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id AND LOWER(emp.last_name) LIKE '%patel%';
-- SELECT principal
SELECT e.first_name||' '||e.last_name AS "Empleado", d.name
FROM S_EMP e,S_DEPT d,S_REGION r, (SELECT dept.id dpto, reg.id region
                                   FROM S_DEPT dept, S_EMP emp, S_REGION reg
                                   WHERE emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id AND LOWER(emp.last_name) LIKE '%patel%') dpto
WHERE e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id AND d.id = dpto.dpto AND r.id =dpto.region;
-- 8) Liste el ID, el apellido y la fecha de entrada de todos los empleados cuyos salarios
-- son menores que el promedio general de salario y trabajan en algún departamento
-- que cuente con un empleado de nombre PATEL
-- Subconsulta con los departamentos con empleado de nombre PATEL

SELECT dept.id dpto, reg.id region
FROM S_DEPT dept, S_REGION reg, S_EMP emp
WHERE emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id
                            AND LOWER(emp.last_name) like '%patel%';

-- Subconsulta del promedio general de salario
SELECT AVG(sal.payment)
FROM S_EMP empe, S_SALARY sal
WHERE empe.id = sal.id;    

-- Select principal
SELECT e.id AS "ID", e.last_name AS "Apellido", e.start_date AS "Fecha entrada"
FROM S_EMP e, S_SALARY s, S_DEPT d, S_REGION r, (SELECT dept.id dpto, reg.id region
                                                FROM S_DEPT dept, S_REGION reg, S_EMP emp
                                                WHERE emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id
                                                      AND LOWER(emp.last_name) like '%patel%') depatel
WHERE e.id = s.id AND e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id AND d.id = depatel.dpto AND r.id = depatel.region
HAVING AVG(s.payment) < (SELECT AVG(sal.payment)
                        FROM S_EMP empe, S_SALARY sal
                        WHERE empe.id = sal.id)
GROUP BY e.id, e.last_name, e.start_date;       

-- 9) Liste el Id del cliente, el nombre y el record de ventas de todos los clientes que están
-- localizados en North Americam o tienen a Magee como representante de ventas.
-- Subconsulta clientes con Magee como representante de ventas
SELECT cli.id
FROM S_CUSTOMER cli, S_EMP emp
WHERE emp.id = cli.sales_rep_id AND lower(emp.last_name) like '%magee%';
-- Subconsulta clientes localizados en norte america
SELECT clie.id
FROM S_CUSTOMER clie, S_REGION region
WHERE clie.region_id = region.id AND LOWER(region.name) like '%north america%';
-- Select principal
SELECT c.id, c.name, COUNT(DISTINCT o.id)
FROM S_CUSTOMER c, S_ORD o, S_REGION r, S_EMP e, (SELECT cli.id cliente
                                                FROM S_CUSTOMER cli, S_EMP emp
                                                WHERE emp.id = cli.sales_rep_id AND lower(emp.last_name) like '%magee%') salesRep,
                                                 (SELECT clie.id cliente
                                                FROM S_CUSTOMER clie, S_REGION region
                                                WHERE clie.region_id = region.id AND LOWER(region.name) like '%north america%') cliNor
WHERE c.id = o.customer_id AND c.id = salesRep.cliente AND c.id = cliNor.cliente
GROUP BY c.id, c.name;

-- 10) Liste los empleados que ganan en promedio más que el promedio de salario de su
--     departamento
-- Subconsulta promedio de salario por departamento
SELECT dept.id id,dept.name dpto, AVG(sal.payment) promedio
FROM S_SALARY sal, S_EMP emp, S_DEPT dept, S_REGION reg
WHERE emp.id = sal.id AND emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id
GROUP BY dept.name,dept.id;
-- Select principal
SELECT e.first_name||' '||e.last_name AS "Nombre", d.name AS "Depto" ,AVG(s.payment) AS "Promedio"
FROM S_EMP e, S_SALARY s, S_DEPT d, S_REGION r, (SELECT dept.id id,dept.name dpto, AVG(sal.payment) promedio
                                                FROM S_SALARY sal, S_EMP emp, S_DEPT dept, S_REGION reg
                                                WHERE emp.id = sal.id AND emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id
                                                GROUP BY dept.name,dept.id) promDepto
WHERE e.id = s.id AND e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id AND d.id = promDepto.id
HAVING AVG(s.payment) > promDepto.promedio
GROUP BY e.first_name||' '||e.last_name, promDepto.promedio,d.name;

-- 11) Listar los empleados a cargo de los vicepresidentes
-- Subconsulta id de los empleados que son vicepresidentes
SELECT emp.id id
FROM S_EMP emp
WHERE LOWER(SUBSTR(emp.title,0,2)) LIKE '%vp%';
-- SELECT principal
SELECT e.first_name||' '||e.last_name AS "Empelado"
FROM S_EMP e, (SELECT emp.id id
                FROM S_EMP emp
                WHERE LOWER(SUBSTR(emp.title,0,2)) LIKE '%vp%') viceId
WHERE e.manager_id = viceId.id;        

-- 12) Listar los empleados que trabajan en el mismo departamento que Ngao
-- Subconsulta Departamento de NGAO
SELECT dept.id 
FROM S_DEPT dept, S_EMP emp, S_REGION reg
WHERE emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id AND LOWER(emp.last_name) LIKE '%ngao';

-- Select principal
SELECT e.first_name||' '||e.last_name AS "Empleado"
FROM S_DEPT d, S_EMP e, S_REGION r
WHERE e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id AND (d.id,r.id) = (SELECT dept.id, reg.id 
                                                                                FROM S_DEPT dept, S_EMP emp, S_REGION reg
                                                                                WHERE emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id AND LOWER(emp.last_name) LIKE '%ngao');

-- 13) Liste el promedio de salario de todos los empleados que tienen el mismo cargo que
-- Havel                                                                                
-- Subconsulta para saber el cargo de Havel
SELECT emp.title cargo
FROM S_EMP emp
WHERE LOWER(emp.last_name) like '%havel%';

-- Subconsulta para saber el promedio de salario por cargo
SELECT empe.title cargo, AVG(sal.payment) promedio
FROM S_EMP empe, S_SALARY sal
WHERE empe.id = sal.id
GROUP BY empe.title;
-- Select principal
SELECT havCargo.cargo AS "Cargo de Havel", promCargo.promedio AS "Promedio"
FROM (SELECT emp.title cargo
    FROM S_EMP emp
    WHERE LOWER(emp.last_name) like '%havel%') havCargo,
    (SELECT empe.title cargo, AVG(sal.payment) promedio
    FROM S_EMP empe, S_SALARY sal
    WHERE empe.id = sal.id
    GROUP BY empe.title) promCargo
WHERE promCargo.cargo = havCargo.cargo;

-- 14) Cuantos empleados ganan igual que Giljum Henry
-- Subconsulta de salario de giljum Henry
SELECT sal.payment salario
FROM S_SALARY sal, S_EMP emp
WHERE sal.id = emp.id AND LOWER(emp.last_Name||' '||emp.first_name) like '%giljum henry%';
-- Select principal
SELECT COUNT(DISTINCT e.id)
FROM S_EMP e, S_SALARY s
WHERE e.id = s.id AND s.payment IN (SELECT sal.payment salario
                                    FROM S_SALARY sal, S_EMP emp
                                    WHERE sal.id = emp.id AND LOWER(emp.last_Name||' '||emp.first_name) like '%giljum henry%');

-- 15) Liste todos los empleados que no están a cargo de un Administrador de bodega
-- Subconsulta de jefes con empleados administradores de bodegas
SELECT DISTINCT jef.id
FROM S_EMP emp, S_WAREHOUSE bod, S_EMP jef
WHERE emp.manager_id = bod.manager_id AND jef.id = emp.manager_id;
-- Select principal
SELECT DISTINCT e.first_name||' '||e.last_name AS "Jefe no a cargo de administradores de bod"
FROM S_EMP e, S_EMP empe
WHERE e.id = empe.manager_id AND e.id NOT IN (SELECT  DISTINCT jef.id
                    FROM S_EMP emp, S_WAREHOUSE bod, S_EMP jef
                    WHERE emp.manager_id = bod.manager_id AND jef.id = emp.manager_id);

-- 16) Calcule el promedio de salario por departamento de todos los empleados que
-- ingresaron a la compañía en el mimo año que Smith George
-- Subconsulta ingreso a la compañia el año de George Smith
SELECT to_char(emp.start_date,'YY') año
FROM S_EMP emp
WHERE LOWER(emp.first_name||' '||emp.last_name) LIKE '%george smith%';
-- Subconulta promedio de salario por departamento
SELECT AVG(sal.payment) promedio, dept.id depto, reg.id region
FROM S_SALARY sal, S_EMP empe, S_DEPT dept, S_REGION reg
WHERE empe.id = sal.id AND empe.dept_id = dept.id AND empe.region_id = reg.id AND dept.region_id = reg.id
GROUP BY dept.id, reg.id;
-- Select principal
SELECT DISTINCT d.id, r.id, promDepto.promedio 
FROM S_EMP e, S_SALARY s, S_DEPT d, S_REGION r, (SELECT to_char(emp.start_date,'YY') ano
                                                FROM S_EMP emp
                                                WHERE LOWER(emp.first_name||' '||emp.last_name) LIKE '%george smith%') anoGeorge,
                                                (SELECT AVG(sal.payment) promedio, dept.id depto, reg.id region
                                                FROM S_SALARY sal, S_EMP empe, S_DEPT dept, S_REGION reg
                                                WHERE empe.id = sal.id AND empe.dept_id = dept.id AND empe.region_id = reg.id AND dept.region_id = reg.id
                                                GROUP BY dept.id, reg.id) promDepto
WHERE e.id = s.id AND e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id AND to_char(e.start_date,'YY') = anoGeorge.ano AND d.id = promDepto.depto AND promDepto.region = r.id;

-- 17) Liste el promedio de ventas de los empleados que están a cargo de los
-- vicepresidentes comerciales.
-- Subconsulta  ventas por empleado
SELECT emp.id empleado, COUNT(ord.id) ventas 
FROM S_ORD ord, S_EMP emp
WHERE ord.sales_rep_id = emp.id
GROUP BY emp.id;
-- Select principal suponiendo que se trata de la cantidad
SELECT e.first_name||' '||e.last_name AS jefe, AVG(empleVentas.ventas) AS "Promedio"
FROM S_EMP e, S_ORD o, S_EMP emple, (SELECT emp.id empleado, COUNT(ord.id) ventas 
                                    FROM S_ORD ord, S_EMP emp
                                    WHERE ord.sales_rep_id = emp.id
                                    GROUP BY emp.id) empleVentas
WHERE e.id = emple.manager_id AND emple.id = empleVentas.empleado
GROUP BY e.first_name||' '||e.last_name;

-- Subconsulta total de ventas por empleado
SELECT emp.id empleado, SUM(ord.total) ventas
FROM S_ORD ord, S_EMP emp
WHERE ord.sales_rep_id = emp.id
GROUP BY emp.id;
-- Subconsulta jefes de los warehouse manager
SELECT DISTINCT jef.id jefe
FROM S_EMP jef, S_EMP empl
WHERE jef.id = empl.manager_id AND LOWER(empl.title) LIKE '%warehouse manager%'; 
-- Select principal suponiendo que se trata de los ingresos venta total
SELECT emple.first_name||' '||emple.last_name AS empleado, AVG(venEmple.ventas) AS "Promedio", e.first_name||' '||e.last_name AS "Encargado"
FROM S_EMP e, S_EMP emple, (SELECT emp.id empleado, SUM(ord.total) ventas
                            FROM S_ORD ord, S_EMP emp
                            WHERE ord.sales_rep_id = emp.id
                            GROUP BY emp.id) venEmple,
                            (SELECT DISTINCT jef.id jefe
                            FROM S_EMP jef, S_EMP empl
                            WHERE jef.id = empl.manager_id AND LOWER(empl.title) LIKE '%sales representative%') jefe
WHERE e.id = emple.manager_id AND e.id = jefe.jefe AND emple.id = venEmple.empleado
GROUP BY emple.first_name||' '||emple.last_name,e.first_name||' '||e.last_name;     

-- 18) Liste el salario mensual de cada uno de los empelados teniendo en cuenta la comision por venta
-- Subconsulta con el salario mensual de cada uno de los empleados que no tienen comission
SELECT emp.id empleado, sal.payment salarioMensual, to_char(sal.datepayment,'MM') mes
FROM S_EMP emp, S_SALARY sal
WHERE emp.id = sal.id AND emp.commission_pct is null;
-- Subconsulta con el salario mensual de cada uno de los empleados que tienen comission
SELECT emp.id empleado, to_char(SUM(sal.payment)+SUM(ord.total)*(MIN(emp.commission_pct)/100)) salarioMensual, to_char(sal.datepayment,'MM') mes
FROM S_EMP emp, S_SALARY sal, S_ORD ord
WHERE emp.id = sal.id AND emp.id = ord.sales_rep_id AND emp.commission_pct is not null
GROUP BY emp.id,to_char(sal.datepayment,'MM');
-- consulta de teniendo en cuenta la comision o no
SELECT DISTINCT e.first_name||' '||e.last_name AS empleado, nvl2(e.commission_pct,salarioCon.salarioMensual,salarioSin.salarioMensual)AS salario
FROM S_EMP e, S_SALARY s, (SELECT emp.id empleado, sal.payment salarioMensual, to_char(sal.datepayment,'MM') mes
                                    FROM S_EMP emp, S_SALARY sal
                                    WHERE emp.id = sal.id AND emp.commission_pct is null) salarioSin,
                                    (SELECT empe.id empleado, to_char(SUM(sala.payment)+SUM(ord.total)*(MIN(empe.commission_pct)/100)) salarioMensual, to_char(sala.datepayment,'MM') mes
                                    FROM S_EMP empe, S_SALARY sala, S_ORD ord
                                    WHERE empe.id = sala.id AND empe.id = ord.sales_rep_id AND empe.commission_pct is not null
                                    GROUP BY empe.id,to_char(sala.datepayment,'MM')) salarioCon
WHERE e.id = s.id ORDER BY e.first_name||' '||e.last_name;

-- 20) Liste el numero de orden e item de las ordenes con mas de dos item
-- Subconsulta con ordenes con mas de dos item
SELECT DISTINCT ord.id orden
FROM S_ORD ord, S_ITEM it
WHERE ord.id = it.ord_id
HAVING COUNT(it.item_id) > 2
GROUP BY ord.id;
-- Select principal
SELECT o.id AS "Orden", i.item_id AS "Item"
FROM S_ORD o, S_ITEM i
WHERE o.id = i.ord_id AND o.id IN (SELECT DISTINCT ord.id orden
                                    FROM S_ORD ord, S_ITEM it
                                    WHERE ord.id = it.ord_id
                                    HAVING COUNT(it.item_id) > 2
                                    GROUP BY ord.id);

-- 21) Liste el promedio de salario por nombre de cargo, de los cargos con mas de dos trabajadores
-- Subconsulta con el promedio de salario por nombre de cargo
SELECT emp.title cargo, AVG(sal.payment) salario
FROM S_EMP emp, S_SALARY sal
WHERE emp.id = sal.id
GROUP BY emp.title;
-- Subconsulta con cargos de mas de 2 trabajadores
SELECT empe.title Cargo, COUNT(empe.id) Cantidad
FROM S_EMP empe
HAVING COUNT(empe.id) > 2
GROUP BY empe.title;      
-- Select principal
SELECT DISTINCT e.title, promSal.salario  
FROM S_EMP e, (SELECT emp.title cargo, AVG(sal.payment) salario
                FROM S_EMP emp, S_SALARY sal
                WHERE emp.id = sal.id
                GROUP BY emp.title) promSal,
                (SELECT empe.title cargo, COUNT(empe.id) Cantidad
                FROM S_EMP empe
                HAVING COUNT(empe.id) > 2
                GROUP BY empe.title) cargos   
WHERE e.title = cargos.cargo AND promSal.cargo = cargos.cargo AND e.title = promSal.cargo;

-- 22) Liste los clientes y sus representantes de ventas que tienen mas de 2 clientes
-- Subconsulta con representantes de ventas con mas de 2 clientes
SELECT emp.id empleado
FROM S_EMP emp, S_CUSTOMER cust
WHERE emp.id = cust.sales_rep_id
HAVING COUNT(cust.id) > 2
GROUP BY emp.id;
-- Select princiapl
SELECT c.name AS "Cliente", e.id AS "ID rep", e.first_name||' '||e.last_name AS "Empleado"
FROM S_EMP e, S_CUSTOMER c
WHERE e.id = c.sales_rep_id AND e.id IN (SELECT emp.id empleado
                                        FROM S_EMP emp, S_CUSTOMER cust
                                        WHERE emp.id = cust.sales_rep_id
                                        HAVING COUNT(cust.id) > 2
                                        GROUP BY emp.id) ORDER BY e.first_name||' '||e.last_name;

-- 23) Liste el salario promedio de cada representante de ventas (id y nombre), teniendo en cuenta
-- la comision se asigna sobre las ventas al mes
-- Subconsulta del id de los representantes de ventas
SELECT DISTINCT emp.id rep, sal.payment salario, to_char(sal.datepayment,'MM YYYY') fecha
FROM S_EMP emp, S_SALARY sal
WHERE emp.id = sal.id AND LOWER(emp.title) LIKE '%sales representative%';    -- pagosRep
-- Subconsulta del salario de los rep id
SELECT empl.id empleado, (SUM(ord.total)*MIN(empl.commission_pct)/100) comision, to_char(ord.date_ordered,'MM YYYY') fecha
FROM S_EMP empl, S_ORD ord, S_SALARY sal
WHERE empl.id = ord.sales_rep_id AND empl.id = sal.id AND LOWER(empl.title) LIKE '%sales representative%'
GROUP BY empl.id, to_char(ord.date_ordered,'MM YYYY');                         -- comRep
-- Select principal
SELECT DISTINCT e.first_name||' '||e.last_name AS "Representante", (pagosRep.salario + comRep.comision) AS "Promedio pago", comRep.fecha AS "Fecha pagos"
FROM S_EMP e, S_SALARY s,(SELECT DISTINCT emp.id rep, sal.payment salario, to_char(sal.datepayment,'MM YYYY') fecha
                FROM S_EMP emp, S_SALARY sal
                WHERE emp.id = sal.id AND LOWER(emp.title) LIKE '%sales representative%')  pagosRep,
                (SELECT empl.id empleado, (SUM(ord.total)*MIN(empl.commission_pct)/100) comision, to_char(ord.date_ordered,'MM YYYY') fecha
                FROM S_EMP empl, S_ORD ord, S_SALARY sal    
                WHERE empl.id = ord.sales_rep_id AND empl.id = sal.id AND LOWER(empl.title) LIKE '%sales representative%'
                GROUP BY empl.id, to_char(ord.date_ordered,'MM YYYY')) comRep
WHERE comRep.fecha = pagosRep.fecha AND e.id = s.id AND e.id = comRep.empleado AND e.id = pagosRep.rep AND pagosRep.rep = comRep.empleado
ORDER BY e.first_name||' '||e.last_name;

-- 25 ) Seleccionar los empleados (nombres) que han ganado en algun mes, menos que el promedio del mes
-- Subconsulta promedio por mes
SELECT AVG(sal.payment) pago, to_char(sal.datepayment,'MM YYYY') salario
FROM S_EMP emp, S_SALARY sal
WHERE emp.id = sal.id
GROUP BY to_char(sal.datepayment,'MM YYYY');
-- Select principal
Select DISTINCT e.first_name||' '||e.last_name AS "Empleado"
FROM S_EMP e, S_SALARY s, (SELECT AVG(sal.payment) pago, to_char(sal.datepayment,'MM YYYY') fecha
                            FROM S_EMP emp, S_SALARY sal
                            WHERE emp.id = sal.id
                            GROUP BY to_char(sal.datepayment,'MM YYYY')) promMes
WHERE e.id = s.id AND s.payment < promMes.pago AND promMes.Fecha = to_char(s.datepayment,'MM YYYY');

-- 26) Seleccionar los empleados que han ganado menos que el promedio de algun depto
-- Subconsulta promedio de los dptos
SELECT dept.id, reg.id, AVG(sal.payment)
FROM S_DEPT dept, S_REGION reg, S_EMP emp,S_SALARY sal
WHERE dept.id = emp.dept_id AND emp.id = sal.id AND emp.region_id = reg.id AND dept.region_id = reg.id
GROUP BY dept.id, reg.id ORDER BY dept.id;
-- Select principal
SELECT DISTINCT e.first_name||' '||e.last_name AS "Empleado", AVG(s.payment) AS "Promedio"
FROM S_EMP e, S_SALARY s, S_REGION r, S_DEPT d, (SELECT dept.id dpto, reg.id region, AVG(sal.payment) promedio
                                                FROM S_DEPT dept, S_REGION reg, S_EMP emp,S_SALARY sal
                                                WHERE dept.id = emp.dept_id AND emp.id = sal.id AND emp.region_id = reg.id AND dept.region_id = reg.id
                                                GROUP BY dept.id, reg.id ORDER BY dept.id) promDept
WHERE e.id = s.id AND e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id AND r.id = promDept.region AND d.id = promDept.dpto 
HAVING AVG(s.payment) < promDept.promedio
GROUP BY e.first_name||' '||e.last_name, promDept.promedio;                

-- 27 ) Seleccionar los productos que han pedido menos que alguna cantidad en stock
-- productos cantidad en stock
SELECT prod.id producto, COUNT(inv.amount_in_stock)
FROM S_PRODUCT prod, S_INVENTORY inv
WHERE prod.id = inv.product_id
HAVING COUNT(inv.amount_in_stock) < 2
GROUP BY prod.id;

-- Select principal
SELECT DISTINCT p.name AS "Productos"
FROM S_PRODUCT p, S_ITEM i, S_ORD o, (SELECT prod.id producto, SUM(inv.amount_in_stock) cant
                    FROM S_PRODUCT prod, S_INVENTORY inv
                    WHERE prod.id = inv.product_id
                    GROUP BY prod.id) prodCant
WHERE p.id = prodCant.producto AND p.id = i.product_id AND i.ord_id = o.id
HAVING COUNT(i.product_id) < prodCant.cant
GROUP BY p.name,prodCant.cant ORDER BY p.name;

-- ULTIMA PRACTICA
-- --15. Liste todos los empleados que no están a cargo de un Administrador de bodega
-- Jefes de un administrador de bodega
SELECT DISTINCT jef.id jefe
FROM S_EMP emp, S_EMP jef
WHERE LOWER(emp.title) LIKE '%warehouse manager%' AND jef.id = emp.manager_id;
-- Jefes que no estan a cargo de un administrador de bodegas
SELECT DISTINCT emp.first_name||' '||emp.last_name AS "Empleado", emp.title AS "Cargo"
FROM S_EMP e, S_EMP emp
WHERE e.id = emp.manager_id AND SUBSTR(e.title,0,2) NOT LIKE '%VP%' AND e.id NOT IN (SELECT DISTINCT jef.id jefe
                                            FROM S_EMP emp, S_EMP jef
                                            WHERE LOWER(emp.title) LIKE '%warehouse manager%' AND jef.id = emp.manager_id);




