-- Practica examen SQL basico
SELECT * FROM S_DEPT;

SELECT e.first_name ||' '||e.last_name "Nombre" FROM S_EMP e;

SELECT e.first_name||' '||e.last_name "Empleado", e.dept_id "Departamento" FROM S_EMP e WHERE e.dept_id = 40;

SELECT p.name "Producto", r.name FROM S_PRODUCT p JOIN S_INVENTORY i ON p.id = i.product_id JOIN S_WAREHOUSE w ON i.warehouse_id = w.id JOIN S_REGION r ON r.id = w.region_id AND r.name like '%North America%';
-- nvl2
SELECT e.first_name||' '||e.last_name, nvl2(e.commission_pct,to_char(e.commission_pct),'-') "comision" FROM S_EMP e;
-- nvl
SELECT e.first_name||' '||e.last_name, nvl(e.commission_pct,0) FROM S_EMP e;
-- cambiar valores
SELECT TRANSLATE(e.userid,'aeiou','12345') FROM S_EMP e;
-- clientes que tengan un valor en alguna posicion (s - 7)
SELECT c.name FROM S_CUSTOMER c WHERE lower(SUBSTR(c.name,7,1)) like '%s%';
-- Empelados que son vicepresidentes
SELECT e.first_name||' '||e.last_name, lower(SUBSTR(e.title,5)) FROM S_EMP e WHERE SUBSTR(e.title,0,2) like '%VP%';
-- Seleccionar los nombres completos de los representantes de ventas
SELECT e.first_name||' '||e.last_name "Representantes de ventas" FROM S_EMP e WHERE e.title like '%Sales Representative%';
-- Seleccionar los nombres completos de los que no son representantes de ventas
SELECT e.first_name||' '||e.last_name "Representantes de ventas" FROM S_EMP e WHERE e.title not like '%Sales Representative%';
-- Seleccionar los nombres completos y cargo, tanto de los empleados con sus subalterno (listar empleado y subalterno)
SELECT e.first_name||' '||e.last_name "Nombre completo", e.title "cargo", sub.first_name||' '||sub.last_name "subalterno",sub.title "cargo" FROM S_EMP e JOIN S_EMP sub ON e.id = sub.manager_id;
-- Seleccionar los nombres completos y cargo de los empelados que tienen subalternos
SELECT DISTINCT a.id "ID", a.first_name||' '||a.last_name "Nombre completo", a.title "cargo" FROM S_EMP a RIGHT JOIN S_EMP b ON a.id = b.manager_id;
-- Seleccionar los nombres completos y cargos de todos los empleados con sus subalternos tengan o no
SELECT s.first_name||' '||s.last_name "Nombre completo",s.title "cargo", e.first_name||' '||e.last_name,e.title FROM S_EMP s LEFT JOIN S_EMP e ON s.id = e.manager_id;
-- Seleccionar los cargos de los empleados que no tienen jefes
SELECT e.first_name||' '||e.last_name "Nombre completo",e.title "cargo" FROM S_EMP e MINUS SELECT DISTINCT e.first_name||' '||e.last_name "Nombre completo", e.title "cargo" FROM S_EMP e RIGHT JOIN S_EMP s ON e.id = s.manager_id;

SELECT s.first_name||' '||s.last_name "Nombre completo", s.title "cargo" FROM S_EMP s MINUS SELECT DISTINCT s.first_name||' '||s.last_name "nombre",s.title FROM S_EMP s JOIN S_EMP e ON s.id = e.manager_id;
-- Fechas
--- FORMATO
SELECT to_char(o.date_shipped,'DD "de" MONTH "de" YY') "Fecha de orden" FROM S_ORD o;
SELECT to_char(o.date_shipped,'MM/YYYY') "Formato" FROM S_ORD o;
-- Agregar meses
SELECT o.date_ordered "fecha de orden", ADD_MONTHS(o.date_ordered,5) "5 meses despues" FROM S_ORD o; 
-- Ultimo dia del mes
SELECT o.date_ordered "fecha de orden", LAST_DAY(o.date_ordered) "Ultimo dia del mes" FROM S_ORD o;

SELECT o.date_ordered "fecha de orden", o.date_ordered + 50 FROM S_ORD o;