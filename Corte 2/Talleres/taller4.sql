-- Taller 4
-- Andres Felipe Wilches Torres - 20172020114
-- 1) Seleccione el nombre y apellido de todos los empleados que tienen el mismo cargo que Carmen Velasquez

SET LINESIZE 200;
SELECT e.first_name||' '||e.last_name as Nombre
FROM S_EMP e
WHERE e.title in (SELECT e.title 
                    FROM S_EMP e
                    WHERE e.first_name||' '||e.last_name like '%Carmen Velasquez%');

-- 2) Liste el apellido, el cargo y el ID del dpto de todos los empleados que trabajan en el mismo departamento que Colin

SELECT e.first_name as Nombre, e.last_name as Apellido, e.title as Cargo, d.id "ID depto"
FROM S_EMP e, S_DEPT d, S_REGION r
WHERE d.id = e.dept_id AND d.region_id = e.region_id AND r.id = d.region_id AND (r.id,d.id) in (SELECT r.id,d.id
                FROM S_DEPT d,S_EMP e, s_region r
                WHERE e.dept_id = d.id AND d.region_id = r.id AND e.region_id = r.id AND lower(e.first_name) like '%colin%');

-- 3) Liste los empleados que ganan el maximo salario

SELECT e.first_name as Nombre, e.last_name as Apellido, s.payment as Salario
FROM S_EMP e,S_SALARY s
WHERE e.id = s.id AND s.payment IN (SELECT MAX(s.payment)
                                    FROM S_SALARY s, S_EMP e
                                    WHERE s.id = e.id);

-- 4) Liste los empelados cuyo salario es al menos como el promedio de salario

SELECT DISTINCT e.first_name as Nombre, e.last_name as Apelldio
FROM S_EMP e, S_SALARY s
WHERE e.id = s.id AND s.payment >= (SELECT AVG(s.payment)
                                    FROM S_SALARY s, S_EMP e
                                    WHERE s.id = e.id);

-- 5) Liste los departamentos cuyo promedio de salario es superior al promedio general

SELECT DISTINCT d.name as Departamento
FROM S_EMP e, S_DEPT d, S_SALARY s, S_REGION r
WHERE d.region_id = r.id AND d.id = e.dept_id 
                         AND r.id = e.region_id 
                         AND e.id = s.id AND s.payment >= (SELECT AVG(s.payment)
                                         FROM S_SALARY s, S_EMP e
                                         WHERE s.id = e.id);

-- 6) Liste los empleados que ganan el maximo salario por departamento
SELECT DISTINCT e.first_name AS Nombre, e.last_name AS apellido, d.name AS Departamento
FROM S_EMP e, S_DEPT d, S_SALARY s, S_REGION r
WHERE d.region_id = r.id AND d.id = e.dept_id
                         AND r.id = e.region_id
                         AND e.id = s.id AND (s.payment,d.name) IN (SELECT MAX(s.payment), d.name
                                                          FROM S_SALARY s, S_EMP e, S_DEPT d, S_REGION r
                                                          WHERE s.id = e.id AND d.id = e.dept_id
                                                          AND r.id = e.region_id GROUP BY d.name);

-- 7) Liste el ID, apellido y el nombre del departamento de todos los empleados
--    que trabajan en un departamento que tenan al menos un empleado de apellido PATEL

SELECT e.id as ID, e.last_Name AS Apellido, d.name AS Departamento
FROM S_EMP e, S_DEPT d, S_REGION r, (SELECT S_REGION.id region, S_DEPT.id dpto
                                     FROM S_EMP, S_DEPT, S_REGION
                                     WHERE S_REGION.id = S_DEPT.region_id
                                     AND S_DEPT.id = S_EMP.dept_id
                                     AND S_DEPT.region_id = S_EMP.region_id
                                     AND LOWER(S_EMP.last_Name) LIKE '%patel%') tabla
WHERE tabla.region= r.id AND tabla.dpto = d.id AND r.id = d.region_id
AND d.id = e.dept_id AND d.region_id = e.region_id;

-- 8) Liste el ID, el apellido y la fecha de entrada de todos los empleados cuyos salarios son menores
--    que el promedio general de salario y trabajan en algun departamento que cuente con un empelado de nombre PATEL

SELECT e.id as ID, e.last_Name AS Apellido, e.start_date AS "Fecha de entrada"
FROM S_EMP e, S_DEPT d, S_REGION r, S_SALARY s, (SELECT S_REGION.id region, S_DEPT.id dpto
                                     FROM S_EMP, S_DEPT, S_REGION
                                     WHERE S_REGION.id = S_DEPT.region_id
                                     AND S_DEPT.id = S_EMP.dept_id
                                     AND S_DEPT.region_id = S_EMP.region_id
                                     AND LOWER(S_EMP.last_Name) LIKE '%patel%') tabla
WHERE tabla.region= r.id AND tabla.dpto = d.id AND r.id = d.region_id
AND d.id = e.dept_id AND d.region_id = e.region_id AND e.id = s.id HAVING AVG(s.payment) < (SELECT AVG(s.payment)
                                                                                            FROM S_SALARY s, S_EMP e
                                                                                            WHERE s.id = e.id)
GROUP BY e.id, e.last_name, e.start_date;

-- 9) Liste el ID del cliente, el nombre y el record de ventas de todos los clientes que estan localizados en North America
--    o tienen a Magee como representante de ventas.

SELECT c.id AS ID, c.name AS Nombre, COUNT(o.id)
FROM S_CUSTOMER c, S_ORD o, S_REGION r, S_EMP e
WHERE c.region_id = r.id AND R.id = (SELECT S_REGION.id FROM S_REGION
                                    WHERE LOWER(S_REGION.name) LIKE '%north america%')
                        AND c.sales_rep_id = (SELECT S_EMP.id FROM S_EMP
                                    WHERE LOWER(S_EMP.last_name) LIKE '%magee%')
                        AND c.sales_rep_id = e.id AND c.id = o.customer_id
GROUP BY c.id, c.name;

-- 10) Liste los empleados que ganan en promedio mas que el promedio de salario de su departamento

SELECT EMPE.emple, EMPE.PROM_EMP
FROM (SELECT R.id REG, D.id DEP, AVG(S.payment) PROM
                  FROM s_emp E, s_salary S, s_region R, s_dept D
                  WHERE E.id = S.id AND R.id = D.region_id AND D.id = E.dept_id
                  AND D.region_id = E.region_id
                  GROUP BY R.id, D.id) DEPA,
                  (SELECT R.id regi, D.id depto, E.first_name emple, AVG(S.payment) PROM_EMP
                  FROM s_emp E, s_salary S, s_region R, s_dept D
                  WHERE E.id = S.id AND D.id = E.dept_id AND R.id = D.region_id
                  AND D.region_id = E.region_id
                  GROUP BY R.id, D.id, E.first_name) EMPE
   WHERE DEPA.REG = EMPE.regi AND DEPA.DEP = EMPE.depto AND EMPE.PROM_EMP > DEPA.PROM;


-- 11) Listar los empleados a cargo de los vicepresidentes.

SELECT EMP.NOMBRE Nombre, MAN.JEFE Jefe
FROM (SELECT F.id JE, F.title JEFE
      FROM s_emp F WHERE LOWER(F.title) LIKE 'vp%') MAN,
     (SELECT (E.first_name || ' ' || E.last_name) NOMBRE, F.id EM
      FROM s_emp E, s_emp F
      WHERE F.id = E.manager_id) EMP
      WHERE EMP.EM = MAN.JE;

--12) Listar los empleados que trabajan en el mismo departamento que Ngao.

SELECT (E.first_name || ' ' || E.last_name) NOMBRE
FROM s_emp E, s_dept D, s_region R
WHERE D.id = E.dept_id AND D.region_id = E.region_id AND R.id = D.region_id
AND (R.id, D.id) IN (SELECT R.id, D.id
                     FROM s_emp E, s_dept D, s_region R
                     WHERE D.id = E.dept_id AND D.region_id = E.region_id AND R.id = D.region_id
                     AND LOWER(E.last_name) LIKE 'ngao%');


--13) Liste el promedio de salario de todos los empelados que tienen el mismo cargo que Havel.

SELECT TA.PROMEDIO promedio, TA.TI
FROM (SELECT E.title TI, AVG(S.payment) PROMEDIO
      FROM s_emp E, s_salary S
      WHERE E.id = S.id
      GROUP BY E.title) TA, (SELECT E.title TL
                             FROM s_emp E
                             WHERE LOWER(E.last_name) LIKE 'havel%') PRO
WHERE TA.TI = PRO.TL;

-- 14) Cuantos empleados ganan igual que Giljum Henry.

SELECT COUNT(DISTINCT(E.last_name))
FROM s_emp E, s_salary S
WHERE E.id = S.id AND S.payment IN (SELECT S.payment
     			                    FROM s_salary S, s_emp E
                          			WHERE E.id = S.id AND
                          			LOWER(E.last_name) LIKE 'giljum%');
COUNT(DISTINCT(E.LAST_NAME)) 

-- 15) Liste todos los empleados que no estan a cargo de un Administrador de bodega.

SELECT (E.first_name || ' ' || E.last_name) EMPLEADOS, F.title "JEFE A CARGO"
FROM s_emp E, s_emp F
WHERE F.id = E.manager_id AND (E.first_name || ' ' || E.last_name) NOT IN (SELECT (E.first_name || ' ' || E.last_name)
                                                                           FROM s_emp E, s_emp F
                                                                           WHERE F.id = E.manager_id AND LOWER(F.title)LIKE 'warehouse manager%');
 
-- 16) Calcule el promedio de salario por departamento de todos los empleados que ingresaron a la compania en el mismo ano que Smith George.

SELECT TA.DE DEPARTAMENTO, TA.RE REGION, TRUNC(SUM(TA.Promedio)/COUNT(TA.DE),2) PROMEDIO
FROM (SELECT D.id DE, R.id RE, (E.first_name || ' ' || E.last_name) EMPLEADO, AVG(S.payment) Promedio
      FROM s_emp E, s_dept D, s_region R, s_salary S
      WHERE E.id = S.id AND D.id = E.dept_id AND D.region_id = E.region_id AND R.id = D.region_id
      AND TO_CHAR(E.start_date,'YYYY')=(SELECT TO_CHAR(E.start_date,'YYYY') fecha
                                        FROM s_emp E
                                        WHERE LOWER(E.first_name)LIKE 'george%')
      GROUP BY D.id, R.id, (E.first_name || ' ' || E.last_name)) TA
GROUP BY TA.DE, TA.RE;

-- 17) Liste el promedio de ventas de los empleados que estan a cargo de los vicepresidentes comerciales.

SELECT vendedores.COD "CODIGO VENDEDOR", TRUNC(AVG(O.total),2) "PROMEDIO VENTAS"
FROM (SELECT E.id COD
      FROM s_emp E
      WHERE E.manager_id = 3) vendedores, s_emp E, s_ord O
WHERE O.sales_rep_id = vendedores.COD
GROUP BY vendedores.COD;

-- 18) Liste el salario mensual de cada uno de los empleados teniendo en cuenta la comisión por venta agrupar ordenes por mes

SELECT E.first_name || ' '||E.last_name Empleado, E.title Cargo, to_char(S.datepayment, 'mm/yyyy') mesAyo,
Sal.SalarioTotal + Ven.Venta Salario
FROM S_emp E, S_salary S, (SELECT E.id Repre, to_char(S.datepayment, 'mm/yyyy') mesAyo,
                  			sum(S.payment) SalarioTotal
                 			FROM s_emp E, s_salary S
                  			WHERE E.id = S.id AND
          					E.commission_pct IS NOT NULL
                  			GROUP BY E.id, to_char(S.datepayment, 'mm/yyyy')
                 			ORDER BY to_char(S.datepayment, 'mm/yyyy')) Sal,
               			  (SELECT O.sales_rep_id Repre, to_char(O.date_shipped, 'mm/yyyy') mesAyo,
              			  sum(O.total)*Min(E.commission_pct)/100 Venta
              			  FROM s_ord O, s_emp E
              			  WHERE E.id = O.sales_rep_id
              			  GROUP BY O.sales_rep_id, to_char(O.date_shipped, 'mm/yyyy')) Ven
WHERE E.id = S.id AND
      E.id = Sal.Repre AND
      E.id = Ven.Repre AND
      to_char(S.datepayment, 'mm/yyyy') = Sal.mesAyo AND
 	  to_char(S.datepayment, 'mm/yyyy') = Ven.mesAyo
UNION
SELECT 	E.first_name || ' '||E.last_name Empleado, E.title Cargo,
        to_char(S.datepayment , 'mm/yyyy') mesAyo, sum(S.payment) Salario
FROM s_emp E, s_salary S
WHERE E.id = S.id AND
      E.commission_pct IS NOT NULL AND
      (E.id, to_char(S.datepayment , 'mm/yyyy')) NOT IN
      (SELECT O.sales_rep_id, to_char(O.date_shipped , 'mm/yyyy')
       FROM s_ord O, s_emp E
       WHERE E.id = O.sales_rep_id
       GROUP BY O.sales_rep_id, to_char(O.date_shipped , 'mm/yyyy'))
GROUP BY first_name, E.last_name, E.title, to_char(S.datepayment , 'mm/yyyy')
UNION
SELECT E.first_name || ' '||E.last_name Empleado, E.title Cargo,
       to_char(S.datepayment, 'mm/yyyy') mesAyo,
       sum(S.payment) Salario
FROM s_emp E, s_salary S
WHERE E.id = S.id AND
      E.commission_pct IS NULL
GROUP BY E.first_name, E.last_name, E.title, to_char(S.datepayment, 'mm/yyyy')
ORDER BY Empleado, Cargo, MesAyo, Salario;

-- 19) liste todos los empleados que atienden una bodega y que han hecho una venta.

SELECT (E.first_name || E.last_name) NOMBRE
FROM (SELECT E.id bodega
      FROM s_emp E
      WHERE LOWER(E.title)LIKE 'warehouse manager%') BO,
      (SELECT E.id vendedor
      FROM s_emp E, s_ord O
      WHERE E.id = O.stock_clerk) VE, s_emp E
WHERE BO.bodega = VE.vendedor;
 
-- 20) Liste el numero de orden e item de las ordenes con mas de 2 item.

SELECT P.orden ORDEN, R.ITE
FROM  (SELECT O.id orden, COUNT(O.id)
       FROM s_ord O, s_item IT
       WHERE O.id = IT.ord_id
       HAVING COUNT(O.id) > 2
       GROUP BY O.id) P, (SELECT O.id ORD, IT.item_id ITE
                          FROM s_ord O, s_item IT
                          WHERE O.id = IT.ord_id) R
WHERE P.orden = R.ORD
ORDER BY P.orden;
 
-- 21) Liste el promedio de salario por nombre de cargo, de los cargos con mas de dos trabajadores.

SELECT A.cargo CARGO, B.PROM "PROMEDIO SALARIO"
FROM (SELECT E.title cargo, COUNT(E.title) C
      FROM s_emp E
      HAVING COUNT(E.title)>2
      GROUP BY E.title) A, (SELECT E.title CA, AVG(S.payment) PROM
                            FROM s_emp E, s_salary S
                            WHERE E.id = S.id
                            GROUP BY E.title) B
WHERE A.cargo = B.CA
ORDER BY A.cargo;

-- 22) Liste los clientes y sus representantes de ventas que tienen mas de 2 clientes.
 
SELECT A.nombre NOMBRE_REP, B.clientes CLIENTE
FROM (SELECT (E.first_name ||' '|| E.last_name) nombre, COUNT(E.id) CLI
      FROM s_emp E, s_customer C
      WHERE E.id = C.sales_rep_id
      HAVING COUNT(E.id) > 2
      GROUP BY (E.first_name ||' '|| E.last_name))A,
      (SELECT (E.first_name ||' '|| E.last_name) nom, C.name clientes
       FROM s_emp E, s_customer C
       WHERE E.id = C.sales_rep_id)B
WHERE A.nombre = B.nom
ORDER BY A.nombre;

-- 23) Liste el salario promedio de cada representante de ventas (id y nombre), teniendo en cuenta que la comision se asigna sobre las ventas del mes.

SELECT A.id, A.nombre, (A.AA+B.PAGO)/12 AWS
FROM (SELECT E.id id, E.first_name nombre, SUM(S.payment) AA
      FROM s_emp E, s_salary S
      WHERE E.id = S.id AND LOWER(E.title) LIKE 'sales representative%'
      GROUP BY E.first_name, E.id) A,
      (SELECT O.sales_rep_id I, SUM(O.total)*MIN(E.commission_pct)/100 pago
      FROM s_emp E, s_ord O
      WHERE E.id = O. sales_rep_id
      GROUP BY O.sales_rep_id) B
WHERE A.id = B.I;

-- 24) Generar un listado con el promedio devengado (incluidas comisiones) en el 2011 de todos los empleados.

SELECT A.id CODIGO, (SUM(A.AA)/MIN(V.BB)) promedio
FROM (SELECT E.id id, SUM(S.payment) AA
      FROM s_emp E, s_salary S
      WHERE E.id = S.id
      GROUP BY E.id
      UNION
      SELECT E.id id, SUM(O.total)*MIN(E.commission_pct)/100 AA
      FROM s_emp E, s_ord O
      WHERE E.id = O. sales_rep_id
      GROUP BY E.id) A, (SELECT E.id WE, COUNT(E.id) BB
                         FROM s_emp E, s_salary S
                         WHERE E.id = S.id
                         GROUP BY E.id) V
WHERE V.WE = A.id
GROUP BY A.id
ORDER BY A.id;

-- 25) Seleccionar los empleados (nombres) que han ganado en algun mes menos que el promedio del mes.

SELECT DISTINCT(A.n) EMPLEADO
FROM (SELECT E.first_name n, TO_CHAR(S.datepayment,'MM') m, S.payment pago
      FROM s_emp E, s_salary S
      WHERE E.id = S.id) A,
      (SELECT TO_CHAR(S.datepayment,'MM') MES, AVG(S.payment) prom
      FROM s_emp E, s_salary S
      WHERE E.id = S.id
      GROUP BY TO_CHAR(S.datepayment,'MM')
      ORDER BY TO_CHAR(S.datepayment,'MM')) B
WHERE A.m = B.MES AND A.pago < B.prom;

-- 26) Seleccionar los empleados que han ganado menos que el promedio de algun departamento.

SELECT B.NOM EMPLEADOS, B.P PROMEDIO_EM
FROM (SELECT R.id RE, D.name DE, AVG(S.payment) PRO
      FROM s_salary S, s_emp E, s_region R, s_dept D
      WHERE S.id = E.id AND R.id = D.region_id AND D.id = E.dept_id
      AND D.region_id = E.region_id
      GROUP BY R.id, D.name) A, (SELECT R.id REG, D.name DEP, E.first_name NOM, AVG(S.payment) P
                                 FROM s_salary S, s_emp E, s_region R, s_dept D
                                 WHERE S.id = E.id AND R.id = D.region_id AND D.id = E.dept_id
                                 AND D.region_id = E.region_id
                                 GROUP BY R.id, D.name, E.first_name) B
WHERE A.RE = B.REG AND A.DE = B.DEP AND B.P <ANY A.PRO;

-- 27) Seleccionar los productos que han pedido menos que alguna cantidad en stock.

SELECT DISTINCT(p.name) AS "Producto"
FROM s_product p, s_item i, (SELECT p.id id, i.amount_in_stock cant
                             FROM s_product p, s_inventory i
    	                     WHERE p.id = product_id
                             ORDER BY p.id) t
WHERE p.id = i.product_id AND p.id = t.id
GROUP BY p.name, t.cant
HAVING COUNT(i.quantity) < t.cant;

-- 28) Seleccionar los clientes que han pedido cantidades mayores que el promedio en US de pedido por orden, por mes o por producto.

SELECT DISTINCT C.name Cliente
FROM s_customer C, s_ord O, s_item I,
     (SELECT avg(I.quantity) Pro1
     FROM s_customer C, s_ord O, s_item I
 	 WHERE C.id = O.customer_id AND
           O.id = I.ord_id AND
     	   Upper(C.country) LIKE 'USA%'
      	   GROUP BY O.id) TEP1
WHERE C.id = O.customer_id AND
      O.id = I.ord_id
GROUP BY C.name, TEP1.Pro1
HAVING sum(I.quantity) >  TEP1.Pro1;
WHERE C.id = O.customer_id
UNION
SELECT DISTINCT C.name Cliente
FROM s_customer C, s_ord O, s_item I,
    (SELECT avg(I.quantity) pro2
   	 FROM s_customer C, s_ord O, s_item I
	 WHERE C.id = O.customer_id AND
           O.id = I.ord_id AND
     	   Upper(C.country) LIKE 'USA%'
     	   GROUP BY to_char(O.date_ordered, 'MM')) TEP2
WHERE C.id = O.customer_id AND
      O.id = I.ord_id
GROUP BY C.name, TEP2.Pro2
HAVING sum(I.quantity) >  TEP2.Pro2
UNION
SELECT DISTINCT C.name Cliente
FROM s_customer C, s_ord O, s_item I,
    (SELECT avg(I.quantity) Pro3
    FROM s_customer C, s_ord O, s_item I
    WHERE C.id = O.customer_id AND
      O.id = I.ord_id AND
  	  Upper(C.country) LIKE 'USA%'
  	  GROUP BY I.product_id) TEP3
WHERE C.id = O.customer_id AND
      O.id = I.ord_id
GROUP BY C.name, TEP3.pro3
HAVING sum(I.quantity) > TEP3.pro3;                           