SQL> -- Taller 4
SQL> -- Andres Felipe Wilches Torres - 20172020114
SQL> -- 1) Seleccione el nombre y apellido de todos los empleados que tienen el mismo cargo que Carmen Velasquez
SQL> 
SQL> SET LINESIZE 200;
SQL> SELECT e.first_name||' '||e.last_name as Nombre
  2  FROM S_EMP e
  3  WHERE e.title in (SELECT e.title
  4                      FROM S_EMP e
  5                      WHERE e.first_name||' '||e.last_name like '%Carmen Velasquez%');

NOMBRE                                                                                                                                                                                                  
-------------------------------                                                                                                                                                                         
Carmen Velasquez                                                                                                                                                                                        

SQL> 
SQL> -- 2) Liste el apellido, el cargo y el ID del dpto de todos los empleados que trabajan en el mismo departamento que Colin
SQL> 
SQL> SELECT e.first_name as Nombre, e.last_name as Apellido, e.title as Cargo, d.id "ID depto"
  2  FROM S_EMP e, S_DEPT d, S_REGION r
  3  WHERE d.id = e.dept_id AND d.region_id = e.region_id AND r.id = d.region_id AND (r.id,d.id) in (SELECT r.id,d.id
  4                  FROM S_DEPT d,S_EMP e, s_region r
  5                  WHERE e.dept_id = d.id AND d.region_id = r.id AND e.region_id = r.id AND lower(e.first_name) like '%colin%');

NOMBRE          APELLIDO        CARGO                       ID depto                                                                                                                                    
--------------- --------------- ------------------------- ----------                                                                                                                                    
Midori          Nagayama        VP, Sales                         30                                                                                                                                    
Colin           Magee           Sales Representative              30                                                                                                                                    

SQL> 
SQL> -- 3) Liste los empleados que ganan el maximo salario
SQL> 
SQL> SELECT e.first_name as Nombre, e.last_name as Apellido, s.payment as Salario
  2  FROM S_EMP e,S_SALARY s
  3  WHERE e.id = s.id AND s.payment IN (SELECT MAX(s.payment)
  4                                      FROM S_SALARY s, S_EMP e
  5                                      WHERE s.id = e.id);

NOMBRE          APELLIDO           SALARIO                                                                                                                                                              
--------------- --------------- ----------                                                                                                                                                              
LaDoris         Ngao                 37000                                                                                                                                                              

SQL> 
SQL> -- 4) Liste los empelados cuyo salario es al menos como el promedio de salario
SQL> 
SQL> SELECT DISTINCT e.first_name as Nombre, e.last_name as Apelldio
  2  FROM S_EMP e, S_SALARY s
  3  WHERE e.id = s.id AND s.payment >= (SELECT AVG(s.payment)
  4                                      FROM S_SALARY s, S_EMP e
  5                                      WHERE s.id = e.id);

NOMBRE          APELLDIO                                                                                                                                                                                
--------------- ---------------                                                                                                                                                                         
Carmen          Velasquez                                                                                                                                                                               
LaDoris         Ngao                                                                                                                                                                                    
Midori          Nagayama                                                                                                                                                                                
Mark            Quick-To-See                                                                                                                                                                            
Audry           Ropeburn                                                                                                                                                                                

SQL> 
SQL> -- 5) Liste los departamentos cuyo promedio de salario es superior al promedio general
SQL> 
SQL> SELECT DISTINCT d.name as Departamento
  2  FROM S_EMP e, S_DEPT d, S_SALARY s, S_REGION r
  3  WHERE d.region_id = r.id AND d.id = e.dept_id
  4                           AND r.id = e.region_id
  5                           AND e.id = s.id AND s.payment >= (SELECT AVG(s.payment)
  6                                           FROM S_SALARY s, S_EMP e
  7                                           WHERE s.id = e.id);

DEPARTAMENTO                                                                                                                                                                                            
---------------                                                                                                                                                                                         
Administration                                                                                                                                                                                          
Operations                                                                                                                                                                                              
Sales                                                                                                                                                                                                   
Finance                                                                                                                                                                                                 

SQL> 
SQL> -- 6) Liste los empleados que ganan el maximo salario por departamento
SQL> SELECT DISTINCT e.first_name AS Nombre, e.last_name AS apellido, d.name AS Departamento
  2  FROM S_EMP e, S_DEPT d, S_SALARY s, S_REGION r
  3  WHERE d.region_id = r.id AND d.id = e.dept_id
  4                           AND r.id = e.region_id
  5                           AND e.id = s.id AND (s.payment,d.name) IN (SELECT MAX(s.payment), d.name
  6                                                            FROM S_SALARY s, S_EMP e, S_DEPT d, S_REGION r
  7                                                            WHERE s.id = e.id AND d.id = e.dept_id
  8                                                            AND r.id = e.region_id GROUP BY d.name);

NOMBRE          APELLIDO        DEPARTAMENTO                                                                                                                                                            
--------------- --------------- ---------------                                                                                                                                                         
Carmen          Velasquez       Administration                                                                                                                                                          
LaDoris         Ngao            Operations                                                                                                                                                              
Midori          Nagayama        Sales                                                                                                                                                                   
Mark            Quick-To-See    Finance                                                                                                                                                                 

SQL> 
SQL> -- 7) Liste el ID, apellido y el nombre del departamento de todos los empleados
SQL> --    que trabajan en un departamento que tenan al menos un empleado de apellido PATEL
SQL> 
SQL> SELECT e.id as ID, e.last_Name AS Apellido, d.name AS Departamento
  2  FROM S_EMP e, S_DEPT d, S_REGION r, (SELECT S_REGION.id region, S_DEPT.id dpto
  3                                       FROM S_EMP, S_DEPT, S_REGION
  4                                       WHERE S_REGION.id = S_DEPT.region_id
  5                                       AND S_DEPT.id = S_EMP.dept_id
  6                                       AND S_DEPT.region_id = S_EMP.region_id
  7                                       AND LOWER(S_EMP.last_Name) LIKE '%patel%') tabla
  8  WHERE tabla.region= r.id AND tabla.dpto = d.id AND r.id = d.region_id
  9  AND d.id = e.dept_id AND d.region_id = e.region_id;

        ID APELLIDO        DEPARTAMENTO                                                                                                                                                                 
---------- --------------- ---------------                                                                                                                                                              
         7 Menchu          Operations                                                                                                                                                                   
        18 Nozaki          Operations                                                                                                                                                                   
        19 Patel           Operations                                                                                                                                                                   
        14 Nguyen          Sales                                                                                                                                                                        
        23 Patel           Sales                                                                                                                                                                        

SQL> 
SQL> -- 8) Liste el ID, el apellido y la fecha de entrada de todos los empleados cuyos salarios son menores
SQL> --    que el promedio general de salario y trabajan en algun departamento que cuente con un empelado de nombre PATEL
SQL> 
SQL> SELECT e.id as ID, e.last_Name AS Apellido, e.start_date AS "Fecha de entrada"
  2  FROM S_EMP e, S_DEPT d, S_REGION r, S_SALARY s, (SELECT S_REGION.id region, S_DEPT.id dpto
  3                                       FROM S_EMP, S_DEPT, S_REGION
  4                                       WHERE S_REGION.id = S_DEPT.region_id
  5                                       AND S_DEPT.id = S_EMP.dept_id
  6                                       AND S_DEPT.region_id = S_EMP.region_id
  7                                       AND LOWER(S_EMP.last_Name) LIKE '%patel%') tabla
  8  WHERE tabla.region= r.id AND tabla.dpto = d.id AND r.id = d.region_id
  9  AND d.id = e.dept_id AND d.region_id = e.region_id AND e.id = s.id HAVING AVG(s.payment) < (SELECT AVG(s.payment)
 10                                                                                              FROM S_SALARY s, S_EMP e
 11                                                                                              WHERE s.id = e.id)
 12  GROUP BY e.id, e.last_name, e.start_date;

        ID APELLIDO        Fecha de                                                                                                                                                                     
---------- --------------- --------                                                                                                                                                                     
         7 Menchu          14/05/09                                                                                                                                                                     
        14 Nguyen          22/01/01                                                                                                                                                                     
        18 Nozaki          09/02/10                                                                                                                                                                     
        19 Patel           06/08/10                                                                                                                                                                     
        23 Patel           17/10/09                                                                                                                                                                     

SQL> 
SQL> -- 9) Liste el ID del cliente, el nombre y el record de ventas de todos los clientes que estan localizados en North America
SQL> --    o tienen a Magee como representante de ventas.
SQL> 
SQL> SELECT c.id AS ID, c.name AS Nombre, COUNT(o.id)
  2  FROM S_CUSTOMER c, S_ORD o, S_REGION r, S_EMP e
  3  WHERE c.region_id = r.id AND R.id = (SELECT S_REGION.id FROM S_REGION
  4                                      WHERE LOWER(S_REGION.name) LIKE '%north america%')
  5                          AND c.sales_rep_id = (SELECT S_EMP.id FROM S_EMP
  6                                      WHERE LOWER(S_EMP.last_name) LIKE '%magee%')
  7                          AND c.sales_rep_id = e.id AND c.id = o.customer_id
  8  GROUP BY c.id, c.name;

        ID NOMBRE                         COUNT(O.ID)                                                                                                                                                   
---------- ------------------------------ -----------                                                                                                                                                   
       204 Womansport                               2                                                                                                                                                   
       209 Beisbol Si!                              1                                                                                                                                                   
       213 Big John's Sports Emporium               1                                                                                                                                                   
       214 Ojibway Retail                           1                                                                                                                                                   

SQL> 
SQL> -- 10) Liste los empleados que ganan en promedio mas que el promedio de salario de su departamento
SQL> 
SQL> SELECT EMPE.emple, EMPE.PROM_EMP
  2  FROM (SELECT R.id REG, D.id DEP, AVG(S.payment) PROM
  3                    FROM s_emp E, s_salary S, s_region R, s_dept D
  4                    WHERE E.id = S.id AND R.id = D.region_id AND D.id = E.dept_id
  5                    AND D.region_id = E.region_id
  6                    GROUP BY R.id, D.id) DEPA,
  7                    (SELECT R.id regi, D.id depto, E.first_name emple, AVG(S.payment) PROM_EMP
  8                    FROM s_emp E, s_salary S, s_region R, s_dept D
  9                    WHERE E.id = S.id AND D.id = E.dept_id AND R.id = D.region_id
 10                    AND D.region_id = E.region_id
 11                    GROUP BY R.id, D.id, E.first_name) EMPE
 12     WHERE DEPA.REG = EMPE.regi AND DEPA.DEP = EMPE.depto AND EMPE.PROM_EMP > DEPA.PROM;

EMPLE             PROM_EMP                                                                                                                                                                              
--------------- ----------                                                                                                                                                                              
Carmen          25181.8182                                                                                                                                                                              
LaDoris         10230.7692                                                                                                                                                                              
Midori          8333.33333                                                                                                                                                                              
Mai             979.916667                                                                                                                                                                              
Akira               788.25                                                                                                                                                                              
Vikram              788.25                                                                                                                                                                              
Chad                788.25                                                                                                                                                                              
Alexander           788.25                                                                                                                                                                              
Eddie               788.25                                                                                                                                                                              
Bela                788.25                                                                                                                                                                              
Sylvie              788.25                                                                                                                                                                              

11 filas seleccionadas.

SQL> 
SQL> 
SQL> -- 11) Listar los empleados a cargo de los vicepresidentes.
SQL> 
SQL> SELECT EMP.NOMBRE Nombre, MAN.JEFE Jefe
  2  FROM (SELECT F.id JE, F.title JEFE
  3        FROM s_emp F WHERE LOWER(F.title) LIKE 'vp%') MAN,
  4       (SELECT (E.first_name || ' ' || E.last_name) NOMBRE, F.id EM
  5        FROM s_emp E, s_emp F
  6        WHERE F.id = E.manager_id) EMP
  7        WHERE EMP.EM = MAN.JE;

NOMBRE                          JEFE                                                                                                                                                                    
------------------------------- -------------------------                                                                                                                                               
Molly Urguhart                  VP, Operations                                                                                                                                                          
Roberta Menchu                  VP, Operations                                                                                                                                                          
Ben Biri                        VP, Operations                                                                                                                                                          
Antoinette Catchpole            VP, Operations                                                                                                                                                          
Marta Havel                     VP, Operations                                                                                                                                                          
Colin Magee                     VP, Sales                                                                                                                                                               
Henry Giljum                    VP, Sales                                                                                                                                                               
Yasmin Sedeghi                  VP, Sales                                                                                                                                                               
Mai Nguyen                      VP, Sales                                                                                                                                                               
Andre Dumas                     VP, Sales                                                                                                                                                               

10 filas seleccionadas.

SQL> 
SQL> --12) Listar los empleados que trabajan en el mismo departamento que Ngao.
SQL> 
SQL> SELECT (E.first_name || ' ' || E.last_name) NOMBRE
  2  FROM s_emp E, s_dept D, s_region R
  3  WHERE D.id = E.dept_id AND D.region_id = E.region_id AND R.id = D.region_id
  4  AND (R.id, D.id) IN (SELECT R.id, D.id
  5                       FROM s_emp E, s_dept D, s_region R
  6                       WHERE D.id = E.dept_id AND D.region_id = E.region_id AND R.id = D.region_id
  7                       AND LOWER(E.last_name) LIKE 'ngao%');

NOMBRE                                                                                                                                                                                                  
-------------------------------                                                                                                                                                                         
LaDoris Ngao                                                                                                                                                                                            
Molly Urguhart                                                                                                                                                                                          
Elena Mandell                                                                                                                                                                                           
George Smith                                                                                                                                                                                            

SQL> 
SQL> 
SQL> --13) Liste el promedio de salario de todos los empelados que tienen el mismo cargo que Havel.
SQL> 
SQL> SELECT TA.PROMEDIO promedio, TA.TI
  2  FROM (SELECT E.title TI, AVG(S.payment) PROMEDIO
  3        FROM s_emp E, s_salary S
  4        WHERE E.id = S.id
  5        GROUP BY E.title) TA, (SELECT E.title TL
  6                               FROM s_emp E
  7                               WHERE LOWER(E.last_name) LIKE 'havel%') PRO
  8  WHERE TA.TI = PRO.TL;

  PROMEDIO TI                                                                                                                                                                                           
---------- -------------------------                                                                                                                                                                    
    470.15 Warehouse Manager                                                                                                                                                                            

SQL> 
SQL> -- 14) Cuantos empleados ganan igual que Giljum Henry.
SQL> 
SQL> SELECT COUNT(DISTINCT(E.last_name))
  2  FROM s_emp E, s_salary S
  3  WHERE E.id = S.id AND S.payment IN (SELECT S.payment
  4       			                    FROM s_salary S, s_emp E
  5                            			WHERE E.id = S.id AND
  6                            			LOWER(E.last_name) LIKE 'giljum%');

COUNT(DISTINCT(E.LAST_NAME))                                                                                                                                                                            
----------------------------                                                                                                                                                                            
                          14                                                                                                                                                                            

SQL> COUNT(DISTINCT(E.LAST_NAME))
SP2-0734: inicio "COUNT(DIST..." de comando desconocido - resto de la l�nea ignorado.
SQL> 
SQL> -- 15) Liste todos los empleados que no estan a cargo de un Administrador de bodega.
SQL> 
SQL> SELECT (E.first_name || ' ' || E.last_name) EMPLEADOS, F.title "JEFE A CARGO"
  2  FROM s_emp E, s_emp F
  3  WHERE F.id = E.manager_id AND (E.first_name || ' ' || E.last_name) NOT IN (SELECT (E.first_name || ' ' || E.last_name)
  4                                                                             FROM s_emp E, s_emp F
  5                                                                             WHERE F.id = E.manager_id AND LOWER(F.title)LIKE 'warehouse manager%');

EMPLEADOS                       JEFE A CARGO                                                                                                                                                            
------------------------------- -------------------------                                                                                                                                               
Marta Havel                     VP, Operations                                                                                                                                                          
LaDoris Ngao                    President                                                                                                                                                               
Henry Giljum                    VP, Sales                                                                                                                                                               
Roberta Menchu                  VP, Operations                                                                                                                                                          
Ben Biri                        VP, Operations                                                                                                                                                          
Yasmin Sedeghi                  VP, Sales                                                                                                                                                               
Midori Nagayama                 President                                                                                                                                                               
Audry Ropeburn                  President                                                                                                                                                               
Colin Magee                     VP, Sales                                                                                                                                                               
Molly Urguhart                  VP, Operations                                                                                                                                                          
Mai Nguyen                      VP, Sales                                                                                                                                                               

EMPLEADOS                       JEFE A CARGO                                                                                                                                                            
------------------------------- -------------------------                                                                                                                                               
Andre Dumas                     VP, Sales                                                                                                                                                               
Mark Quick-To-See               President                                                                                                                                                               
Antoinette Catchpole            VP, Operations                                                                                                                                                          

14 filas seleccionadas.

SQL> 
SQL> -- 16) Calcule el promedio de salario por departamento de todos los empleados que ingresaron a la compania en el mismo ano que Smith George.
SQL> 
SQL> SELECT TA.DE DEPARTAMENTO, TA.RE REGION, TRUNC(SUM(TA.Promedio)/COUNT(TA.DE),2) PROMEDIO
  2  FROM (SELECT D.id DE, R.id RE, (E.first_name || ' ' || E.last_name) EMPLEADO, AVG(S.payment) Promedio
  3        FROM s_emp E, s_dept D, s_region R, s_salary S
  4        WHERE E.id = S.id AND D.id = E.dept_id AND D.region_id = E.region_id AND R.id = D.region_id
  5        AND TO_CHAR(E.start_date,'YYYY')=(SELECT TO_CHAR(E.start_date,'YYYY') fecha
  6                                          FROM s_emp E
  7                                          WHERE LOWER(E.first_name)LIKE 'george%')
  8        GROUP BY D.id, R.id, (E.first_name || ' ' || E.last_name)) TA
  9  GROUP BY TA.DE, TA.RE;

DEPARTAMENTO     REGION   PROMEDIO                                                                                                                                                                      
------------ ---------- ----------                                                                                                                                                                      
          40          1     5509.5                                                                                                                                                                      
          10          1    8233.33                                                                                                                                                                      
          50          1    6316.66                                                                                                                                                                      
          40          2     476.91                                                                                                                                                                      
          40          3     494.41                                                                                                                                                                      
          30          1     913.25                                                                                                                                                                      
          40          4     788.25                                                                                                                                                                      
          30          4     788.25                                                                                                                                                                      

8 filas seleccionadas.

SQL> 
SQL> -- 17) Liste el promedio de ventas de los empleados que estan a cargo de los vicepresidentes comerciales.
SQL> 
SQL> SELECT vendedores.COD "CODIGO VENDEDOR", TRUNC(AVG(O.total),2) "PROMEDIO VENTAS"
  2  FROM (SELECT E.id COD
  3        FROM s_emp E
  4        WHERE E.manager_id = 3) vendedores, s_emp E, s_ord O
  5  WHERE O.sales_rep_id = vendedores.COD
  6  GROUP BY vendedores.COD;

CODIGO VENDEDOR PROMEDIO VENTAS                                                                                                                                                                         
--------------- ---------------                                                                                                                                                                         
             11       325813.27                                                                                                                                                                         
             14         5452.86                                                                                                                                                                         
             15        45828.25                                                                                                                                                                         
             12        33394.66                                                                                                                                                                         
             13          149570                                                                                                                                                                         

SQL> 
SQL> -- 18) Liste el salario mensual de cada uno de los empleados teniendo en cuenta la comisi�n por venta agrupar ordenes por mes
SQL> 
SQL> SELECT E.first_name || ' '||E.last_name Empleado, E.title Cargo, to_char(S.datepayment, 'mm/yyyy') mesAyo,
  2  Sal.SalarioTotal + Ven.Venta Salario
  3  FROM S_emp E, S_salary S, (SELECT E.id Repre, to_char(S.datepayment, 'mm/yyyy') mesAyo,
  4                    			sum(S.payment) SalarioTotal
  5                   			FROM s_emp E, s_salary S
  6                    			WHERE E.id = S.id AND
  7            					E.commission_pct IS NOT NULL
  8                    			GROUP BY E.id, to_char(S.datepayment, 'mm/yyyy')
  9                   			ORDER BY to_char(S.datepayment, 'mm/yyyy')) Sal,
 10                 			  (SELECT O.sales_rep_id Repre, to_char(O.date_shipped, 'mm/yyyy') mesAyo,
 11                			  sum(O.total)*Min(E.commission_pct)/100 Venta
 12                			  FROM s_ord O, s_emp E
 13                			  WHERE E.id = O.sales_rep_id
 14                			  GROUP BY O.sales_rep_id, to_char(O.date_shipped, 'mm/yyyy')) Ven
 15  WHERE E.id = S.id AND
 16        E.id = Sal.Repre AND
 17        E.id = Ven.Repre AND
 18        to_char(S.datepayment, 'mm/yyyy') = Sal.mesAyo AND
 19   	  to_char(S.datepayment, 'mm/yyyy') = Ven.mesAyo
 20  UNION
 21  SELECT 	E.first_name || ' '||E.last_name Empleado, E.title Cargo,
 22          to_char(S.datepayment , 'mm/yyyy') mesAyo, sum(S.payment) Salario
 23  FROM s_emp E, s_salary S
 24  WHERE E.id = S.id AND
 25        E.commission_pct IS NOT NULL AND
 26        (E.id, to_char(S.datepayment , 'mm/yyyy')) NOT IN
 27        (SELECT O.sales_rep_id, to_char(O.date_shipped , 'mm/yyyy')
 28         FROM s_ord O, s_emp E
 29         WHERE E.id = O.sales_rep_id
 30         GROUP BY O.sales_rep_id, to_char(O.date_shipped , 'mm/yyyy'))
 31  GROUP BY first_name, E.last_name, E.title, to_char(S.datepayment , 'mm/yyyy')
 32  UNION
 33  SELECT E.first_name || ' '||E.last_name Empleado, E.title Cargo,
 34         to_char(S.datepayment, 'mm/yyyy') mesAyo,
 35         sum(S.payment) Salario
 36  FROM s_emp E, s_salary S
 37  WHERE E.id = S.id AND
 38        E.commission_pct IS NULL
 39  GROUP BY E.first_name, E.last_name, E.title, to_char(S.datepayment, 'mm/yyyy')
 40  ORDER BY Empleado, Cargo, MesAyo, Salario;

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Akira Nozaki                    Stock Clerk               01/2011        700                                                                                                                            
Akira Nozaki                    Stock Clerk               02/2011        826                                                                                                                            
Akira Nozaki                    Stock Clerk               03/2011        750                                                                                                                            
Akira Nozaki                    Stock Clerk               04/2011        882                                                                                                                            
Akira Nozaki                    Stock Clerk               05/2011        787                                                                                                                            
Akira Nozaki                    Stock Clerk               06/2011        880                                                                                                                            
Akira Nozaki                    Stock Clerk               07/2011        782                                                                                                                            
Akira Nozaki                    Stock Clerk               08/2011        817                                                                                                                            
Akira Nozaki                    Stock Clerk               09/2011        700                                                                                                                            
Akira Nozaki                    Stock Clerk               10/2011        810                                                                                                                            
Akira Nozaki                    Stock Clerk               11/2011        724                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Akira Nozaki                    Stock Clerk               12/2011        801                                                                                                                            
Alexander Markarian             Stock Clerk               01/2011        700                                                                                                                            
Alexander Markarian             Stock Clerk               02/2011        826                                                                                                                            
Alexander Markarian             Stock Clerk               03/2011        750                                                                                                                            
Alexander Markarian             Stock Clerk               04/2011        882                                                                                                                            
Alexander Markarian             Stock Clerk               05/2011        787                                                                                                                            
Alexander Markarian             Stock Clerk               06/2011        880                                                                                                                            
Alexander Markarian             Stock Clerk               07/2011        782                                                                                                                            
Alexander Markarian             Stock Clerk               08/2011        817                                                                                                                            
Alexander Markarian             Stock Clerk               09/2011        700                                                                                                                            
Alexander Markarian             Stock Clerk               10/2011        810                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Alexander Markarian             Stock Clerk               11/2011        724                                                                                                                            
Alexander Markarian             Stock Clerk               12/2011        801                                                                                                                            
Andre Dumas                     Sales Representative      01/2011        700                                                                                                                            
Andre Dumas                     Sales Representative      02/2011        826                                                                                                                            
Andre Dumas                     Sales Representative      03/2011        750                                                                                                                            
Andre Dumas                     Sales Representative      04/2011        882                                                                                                                            
Andre Dumas                     Sales Representative      05/2011        787                                                                                                                            
Andre Dumas                     Sales Representative      06/2011        880                                                                                                                            
Andre Dumas                     Sales Representative      07/2011        782                                                                                                                            
Andre Dumas                     Sales Representative      08/2011        817                                                                                                                            
Andre Dumas                     Sales Representative      09/2011  32779.775                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Andre Dumas                     Sales Representative      10/2011        810                                                                                                                            
Andre Dumas                     Sales Representative      11/2011        724                                                                                                                            
Andre Dumas                     Sales Representative      12/2011        801                                                                                                                            
Antoinette Catchpole            Warehouse Manager         01/2011        314                                                                                                                            
Antoinette Catchpole            Warehouse Manager         02/2011        226                                                                                                                            
Antoinette Catchpole            Warehouse Manager         03/2011        550                                                                                                                            
Antoinette Catchpole            Warehouse Manager         04/2011        482                                                                                                                            
Antoinette Catchpole            Warehouse Manager         05/2011        287                                                                                                                            
Antoinette Catchpole            Warehouse Manager         06/2011        300                                                                                                                            
Antoinette Catchpole            Warehouse Manager         07/2011        312                                                                                                                            
Antoinette Catchpole            Warehouse Manager         08/2011        517                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Antoinette Catchpole            Warehouse Manager         09/2011        500                                                                                                                            
Antoinette Catchpole            Warehouse Manager         10/2011        510                                                                                                                            
Antoinette Catchpole            Warehouse Manager         11/2011        524                                                                                                                            
Antoinette Catchpole            Warehouse Manager         12/2011        501                                                                                                                            
Audry Ropeburn                  VP, Administration        01/2011       5000                                                                                                                            
Audry Ropeburn                  VP, Administration        02/2011       5500                                                                                                                            
Audry Ropeburn                  VP, Administration        03/2011       5500                                                                                                                            
Audry Ropeburn                  VP, Administration        04/2011       5500                                                                                                                            
Audry Ropeburn                  VP, Administration        05/2011       6500                                                                                                                            
Audry Ropeburn                  VP, Administration        06/2011       6000                                                                                                                            
Audry Ropeburn                  VP, Administration        07/2011       6700                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Audry Ropeburn                  VP, Administration        08/2011       6700                                                                                                                            
Audry Ropeburn                  VP, Administration        09/2011       7000                                                                                                                            
Audry Ropeburn                  VP, Administration        10/2011       7000                                                                                                                            
Audry Ropeburn                  VP, Administration        11/2011       7000                                                                                                                            
Audry Ropeburn                  VP, Administration        12/2011       7400                                                                                                                            
Bela Dancs                      Stock Clerk               01/2011        700                                                                                                                            
Bela Dancs                      Stock Clerk               02/2011        826                                                                                                                            
Bela Dancs                      Stock Clerk               03/2011        750                                                                                                                            
Bela Dancs                      Stock Clerk               04/2011        882                                                                                                                            
Bela Dancs                      Stock Clerk               05/2011        787                                                                                                                            
Bela Dancs                      Stock Clerk               06/2011        880                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Bela Dancs                      Stock Clerk               07/2011        782                                                                                                                            
Bela Dancs                      Stock Clerk               08/2011        817                                                                                                                            
Bela Dancs                      Stock Clerk               09/2011        700                                                                                                                            
Bela Dancs                      Stock Clerk               10/2011        810                                                                                                                            
Bela Dancs                      Stock Clerk               11/2011        724                                                                                                                            
Bela Dancs                      Stock Clerk               12/2011        801                                                                                                                            
Ben Biri                        Warehouse Manager         01/2011        324                                                                                                                            
Ben Biri                        Warehouse Manager         02/2011        326                                                                                                                            
Ben Biri                        Warehouse Manager         03/2011        350                                                                                                                            
Ben Biri                        Warehouse Manager         04/2011        382                                                                                                                            
Ben Biri                        Warehouse Manager         05/2011        387                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Ben Biri                        Warehouse Manager         06/2011        500                                                                                                                            
Ben Biri                        Warehouse Manager         07/2011        512                                                                                                                            
Ben Biri                        Warehouse Manager         08/2011        517                                                                                                                            
Ben Biri                        Warehouse Manager         09/2011        700                                                                                                                            
Ben Biri                        Warehouse Manager         10/2011        610                                                                                                                            
Ben Biri                        Warehouse Manager         11/2011        624                                                                                                                            
Ben Biri                        Warehouse Manager         12/2011        701                                                                                                                            
Carmen Velasquez                President                 01/2011      15000                                                                                                                            
Carmen Velasquez                President                 02/2011      15000                                                                                                                            
Carmen Velasquez                President                 03/2011      25000                                                                                                                            
Carmen Velasquez                President                 04/2011      25000                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Carmen Velasquez                President                 05/2011      25000                                                                                                                            
Carmen Velasquez                President                 06/2011      25000                                                                                                                            
Carmen Velasquez                President                 07/2011      25000                                                                                                                            
Carmen Velasquez                President                 08/2011      30000                                                                                                                            
Carmen Velasquez                President                 09/2011      30000                                                                                                                            
Carmen Velasquez                President                 10/2011      31000                                                                                                                            
Carmen Velasquez                President                 11/2011      31000                                                                                                                            
Chad Newman                     Stock Clerk               01/2011        700                                                                                                                            
Chad Newman                     Stock Clerk               02/2011        826                                                                                                                            
Chad Newman                     Stock Clerk               03/2011        750                                                                                                                            
Chad Newman                     Stock Clerk               04/2011        882                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Chad Newman                     Stock Clerk               05/2011        787                                                                                                                            
Chad Newman                     Stock Clerk               06/2011        880                                                                                                                            
Chad Newman                     Stock Clerk               07/2011        782                                                                                                                            
Chad Newman                     Stock Clerk               08/2011        817                                                                                                                            
Chad Newman                     Stock Clerk               09/2011        700                                                                                                                            
Chad Newman                     Stock Clerk               10/2011        810                                                                                                                            
Chad Newman                     Stock Clerk               11/2011        724                                                                                                                            
Chad Newman                     Stock Clerk               12/2011        801                                                                                                                            
Colin Magee                     Sales Representative      01/2011        800                                                                                                                            
Colin Magee                     Sales Representative      02/2011        826                                                                                                                            
Colin Magee                     Sales Representative      03/2011        850                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Colin Magee                     Sales Representative      04/2011        982                                                                                                                            
Colin Magee                     Sales Representative      05/2011        987                                                                                                                            
Colin Magee                     Sales Representative      06/2011        980                                                                                                                            
Colin Magee                     Sales Representative      07/2011        982                                                                                                                            
Colin Magee                     Sales Representative      08/2011        917                                                                                                                            
Colin Magee                     Sales Representative      09/2011 163806.637                                                                                                                            
Colin Magee                     Sales Representative      10/2011        910                                                                                                                            
Colin Magee                     Sales Representative      11/2011        924                                                                                                                            
Colin Magee                     Sales Representative      12/2011        901                                                                                                                            
Eddie Chang                     Stock Clerk               01/2011        700                                                                                                                            
Eddie Chang                     Stock Clerk               02/2011        826                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Eddie Chang                     Stock Clerk               03/2011        750                                                                                                                            
Eddie Chang                     Stock Clerk               04/2011        882                                                                                                                            
Eddie Chang                     Stock Clerk               05/2011        787                                                                                                                            
Eddie Chang                     Stock Clerk               06/2011        880                                                                                                                            
Eddie Chang                     Stock Clerk               07/2011        782                                                                                                                            
Eddie Chang                     Stock Clerk               08/2011        817                                                                                                                            
Eddie Chang                     Stock Clerk               09/2011        700                                                                                                                            
Eddie Chang                     Stock Clerk               10/2011        810                                                                                                                            
Eddie Chang                     Stock Clerk               11/2011        724                                                                                                                            
Eddie Chang                     Stock Clerk               12/2011        801                                                                                                                            
Elena Mandell                   Stock Clerk               01/2011        700                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Elena Mandell                   Stock Clerk               02/2011        826                                                                                                                            
Elena Mandell                   Stock Clerk               03/2011        750                                                                                                                            
Elena Mandell                   Stock Clerk               04/2011        882                                                                                                                            
Elena Mandell                   Stock Clerk               05/2011        787                                                                                                                            
Elena Mandell                   Stock Clerk               06/2011        880                                                                                                                            
Elena Mandell                   Stock Clerk               07/2011        782                                                                                                                            
Elena Mandell                   Stock Clerk               08/2011        817                                                                                                                            
Elena Mandell                   Stock Clerk               09/2011        700                                                                                                                            
Elena Mandell                   Stock Clerk               10/2011        810                                                                                                                            
Elena Mandell                   Stock Clerk               11/2011        724                                                                                                                            
Elena Mandell                   Stock Clerk               12/2011        801                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
George Smith                    Stock Clerk               01/2011        700                                                                                                                            
George Smith                    Stock Clerk               02/2011        826                                                                                                                            
George Smith                    Stock Clerk               03/2011        750                                                                                                                            
George Smith                    Stock Clerk               04/2011        882                                                                                                                            
George Smith                    Stock Clerk               05/2011        787                                                                                                                            
George Smith                    Stock Clerk               06/2011        880                                                                                                                            
George Smith                    Stock Clerk               07/2011        782                                                                                                                            
George Smith                    Stock Clerk               08/2011        817                                                                                                                            
George Smith                    Stock Clerk               09/2011        700                                                                                                                            
George Smith                    Stock Clerk               10/2011        810                                                                                                                            
George Smith                    Stock Clerk               11/2011        724                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
George Smith                    Stock Clerk               12/2011        801                                                                                                                            
Henry Giljum                    Sales Representative      02/2011        802                                                                                                                            
Henry Giljum                    Sales Representative      03/2011        802                                                                                                                            
Henry Giljum                    Sales Representative      04/2011        800                                                                                                                            
Henry Giljum                    Sales Representative      05/2011        800                                                                                                                            
Henry Giljum                    Sales Representative      06/2011        780                                                                                                                            
Henry Giljum                    Sales Representative      07/2011        782                                                                                                                            
Henry Giljum                    Sales Representative      08/2011        717                                                                                                                            
Henry Giljum                    Sales Representative      09/2011      13223                                                                                                                            
Henry Giljum                    Sales Representative      10/2011        710                                                                                                                            
Henry Giljum                    Sales Representative      11/2011        824                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Henry Giljum                    Sales Representative      12/2011        801                                                                                                                            
LaDoris Ngao                    VP, Operations            01/2011       7000                                                                                                                            
LaDoris Ngao                    VP, Operations            02/2011       7000                                                                                                                            
LaDoris Ngao                    VP, Operations            03/2011       7000                                                                                                                            
LaDoris Ngao                    VP, Operations            04/2011       7000                                                                                                                            
LaDoris Ngao                    VP, Operations            05/2011       7000                                                                                                                            
LaDoris Ngao                    VP, Operations            06/2011       7000                                                                                                                            
LaDoris Ngao                    VP, Operations            07/2011       9000                                                                                                                            
LaDoris Ngao                    VP, Operations            08/2011       9000                                                                                                                            
LaDoris Ngao                    VP, Operations            09/2011       9000                                                                                                                            
LaDoris Ngao                    VP, Operations            10/2011       9000                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
LaDoris Ngao                    VP, Operations            11/2011       9000                                                                                                                            
LaDoris Ngao                    VP, Operations            12/2011      46000                                                                                                                            
Mai Nguyen                      Sales Representative      01/2011        900                                                                                                                            
Mai Nguyen                      Sales Representative      02/2011        926                                                                                                                            
Mai Nguyen                      Sales Representative      03/2011        950                                                                                                                            
Mai Nguyen                      Sales Representative      04/2011        982                                                                                                                            
Mai Nguyen                      Sales Representative      05/2011        987                                                                                                                            
Mai Nguyen                      Sales Representative      06/2011        980                                                                                                                            
Mai Nguyen                      Sales Representative      07/2011        982                                                                                                                            
Mai Nguyen                      Sales Representative      08/2011       1017                                                                                                                            
Mai Nguyen                      Sales Representative      09/2011    3453.79                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Mai Nguyen                      Sales Representative      10/2011       1010                                                                                                                            
Mai Nguyen                      Sales Representative      11/2011       1024                                                                                                                            
Mai Nguyen                      Sales Representative      12/2011       1001                                                                                                                            
Mark Quick-To-See               VP, Finance               01/2011       6000                                                                                                                            
Mark Quick-To-See               VP, Finance               02/2011       7500                                                                                                                            
Mark Quick-To-See               VP, Finance               03/2011       7500                                                                                                                            
Mark Quick-To-See               VP, Finance               04/2011       7500                                                                                                                            
Mark Quick-To-See               VP, Finance               05/2011       8500                                                                                                                            
Mark Quick-To-See               VP, Finance               06/2011       8000                                                                                                                            
Mark Quick-To-See               VP, Finance               07/2011       8700                                                                                                                            
Mark Quick-To-See               VP, Finance               08/2011       8700                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Mark Quick-To-See               VP, Finance               09/2011       9000                                                                                                                            
Mark Quick-To-See               VP, Finance               10/2011       9000                                                                                                                            
Mark Quick-To-See               VP, Finance               11/2011       9000                                                                                                                            
Mark Quick-To-See               VP, Finance               12/2011       9400                                                                                                                            
Marta Havel                     Warehouse Manager         01/2011        414                                                                                                                            
Marta Havel                     Warehouse Manager         02/2011        426                                                                                                                            
Marta Havel                     Warehouse Manager         03/2011        450                                                                                                                            
Marta Havel                     Warehouse Manager         04/2011        482                                                                                                                            
Marta Havel                     Warehouse Manager         05/2011        487                                                                                                                            
Marta Havel                     Warehouse Manager         06/2011        400                                                                                                                            
Marta Havel                     Warehouse Manager         07/2011        412                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Marta Havel                     Warehouse Manager         08/2011        417                                                                                                                            
Marta Havel                     Warehouse Manager         09/2011        600                                                                                                                            
Marta Havel                     Warehouse Manager         10/2011        610                                                                                                                            
Marta Havel                     Warehouse Manager         11/2011        624                                                                                                                            
Marta Havel                     Warehouse Manager         12/2011        601                                                                                                                            
Midori Nagayama                 VP, Sales                 01/2011       7500                                                                                                                            
Midori Nagayama                 VP, Sales                 02/2011       7500                                                                                                                            
Midori Nagayama                 VP, Sales                 03/2011       7500                                                                                                                            
Midori Nagayama                 VP, Sales                 04/2011       7500                                                                                                                            
Midori Nagayama                 VP, Sales                 05/2011       8000                                                                                                                            
Midori Nagayama                 VP, Sales                 06/2011       8000                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Midori Nagayama                 VP, Sales                 07/2011       9000                                                                                                                            
Midori Nagayama                 VP, Sales                 08/2011       9000                                                                                                                            
Midori Nagayama                 VP, Sales                 09/2011       9000                                                                                                                            
Midori Nagayama                 VP, Sales                 10/2011       9000                                                                                                                            
Midori Nagayama                 VP, Sales                 11/2011       9000                                                                                                                            
Midori Nagayama                 VP, Sales                 12/2011       9000                                                                                                                            
Molly Urguhart                  Warehouse Manager         01/2011        320                                                                                                                            
Molly Urguhart                  Warehouse Manager         02/2011        320                                                                                                                            
Molly Urguhart                  Warehouse Manager         03/2011        320                                                                                                                            
Molly Urguhart                  Warehouse Manager         04/2011        320                                                                                                                            
Molly Urguhart                  Warehouse Manager         05/2011        320                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Molly Urguhart                  Warehouse Manager         06/2011        512                                                                                                                            
Molly Urguhart                  Warehouse Manager         07/2011        512                                                                                                                            
Molly Urguhart                  Warehouse Manager         08/2011        513                                                                                                                            
Molly Urguhart                  Warehouse Manager         09/2011        610                                                                                                                            
Molly Urguhart                  Warehouse Manager         10/2011        610                                                                                                                            
Molly Urguhart                  Warehouse Manager         11/2011        620                                                                                                                            
Molly Urguhart                  Warehouse Manager         12/2011        630                                                                                                                            
Radha Patel                     Stock Clerk               01/2011        700                                                                                                                            
Radha Patel                     Stock Clerk               02/2011        826                                                                                                                            
Radha Patel                     Stock Clerk               03/2011        750                                                                                                                            
Radha Patel                     Stock Clerk               04/2011        882                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Radha Patel                     Stock Clerk               05/2011        787                                                                                                                            
Radha Patel                     Stock Clerk               06/2011        880                                                                                                                            
Radha Patel                     Stock Clerk               07/2011        782                                                                                                                            
Radha Patel                     Stock Clerk               08/2011        817                                                                                                                            
Radha Patel                     Stock Clerk               09/2011        700                                                                                                                            
Radha Patel                     Stock Clerk               10/2011        810                                                                                                                            
Radha Patel                     Stock Clerk               11/2011        724                                                                                                                            
Radha Patel                     Stock Clerk               12/2011        801                                                                                                                            
Roberta Menchu                  Warehouse Manager         01/2011        320                                                                                                                            
Roberta Menchu                  Warehouse Manager         02/2011        325                                                                                                                            
Roberta Menchu                  Warehouse Manager         03/2011        320                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Roberta Menchu                  Warehouse Manager         04/2011        332                                                                                                                            
Roberta Menchu                  Warehouse Manager         05/2011        320                                                                                                                            
Roberta Menchu                  Warehouse Manager         06/2011        512                                                                                                                            
Roberta Menchu                  Warehouse Manager         07/2011        512                                                                                                                            
Roberta Menchu                  Warehouse Manager         08/2011        527                                                                                                                            
Roberta Menchu                  Warehouse Manager         09/2011        630                                                                                                                            
Roberta Menchu                  Warehouse Manager         10/2011        610                                                                                                                            
Roberta Menchu                  Warehouse Manager         11/2011        635                                                                                                                            
Roberta Menchu                  Warehouse Manager         12/2011        680                                                                                                                            
Sylvie Schwartz                 Stock Clerk               01/2011        700                                                                                                                            
Sylvie Schwartz                 Stock Clerk               02/2011        826                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Sylvie Schwartz                 Stock Clerk               03/2011        750                                                                                                                            
Sylvie Schwartz                 Stock Clerk               04/2011        882                                                                                                                            
Sylvie Schwartz                 Stock Clerk               05/2011        787                                                                                                                            
Sylvie Schwartz                 Stock Clerk               06/2011        880                                                                                                                            
Sylvie Schwartz                 Stock Clerk               07/2011        782                                                                                                                            
Sylvie Schwartz                 Stock Clerk               08/2011        817                                                                                                                            
Sylvie Schwartz                 Stock Clerk               09/2011        700                                                                                                                            
Sylvie Schwartz                 Stock Clerk               10/2011        810                                                                                                                            
Sylvie Schwartz                 Stock Clerk               11/2011        724                                                                                                                            
Sylvie Schwartz                 Stock Clerk               12/2011        801                                                                                                                            
Vikram Patel                    Stock Clerk               01/2011        700                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Vikram Patel                    Stock Clerk               02/2011        826                                                                                                                            
Vikram Patel                    Stock Clerk               03/2011        750                                                                                                                            
Vikram Patel                    Stock Clerk               04/2011        882                                                                                                                            
Vikram Patel                    Stock Clerk               05/2011        787                                                                                                                            
Vikram Patel                    Stock Clerk               06/2011        880                                                                                                                            
Vikram Patel                    Stock Clerk               07/2011        782                                                                                                                            
Vikram Patel                    Stock Clerk               08/2011        817                                                                                                                            
Vikram Patel                    Stock Clerk               09/2011        700                                                                                                                            
Vikram Patel                    Stock Clerk               10/2011        810                                                                                                                            
Vikram Patel                    Stock Clerk               11/2011        724                                                                                                                            
Vikram Patel                    Stock Clerk               12/2011        801                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Yasmin Sedeghi                  Sales Representative      01/2011        700                                                                                                                            
Yasmin Sedeghi                  Sales Representative      02/2011        726                                                                                                                            
Yasmin Sedeghi                  Sales Representative      03/2011        750                                                                                                                            
Yasmin Sedeghi                  Sales Representative      04/2011        972                                                                                                                            
Yasmin Sedeghi                  Sales Representative      05/2011        977                                                                                                                            
Yasmin Sedeghi                  Sales Representative      06/2011        970                                                                                                                            
Yasmin Sedeghi                  Sales Representative      07/2011        972                                                                                                                            
Yasmin Sedeghi                  Sales Representative      08/2011        900                                                                                                                            
Yasmin Sedeghi                  Sales Representative      09/2011      15857                                                                                                                            
Yasmin Sedeghi                  Sales Representative      10/2011        900                                                                                                                            
Yasmin Sedeghi                  Sales Representative      11/2011        904                                                                                                                            

EMPLEADO                        CARGO                     MESAYO     SALARIO                                                                                                                            
------------------------------- ------------------------- ------- ----------                                                                                                                            
Yasmin Sedeghi                  Sales Representative      12/2011        901                                                                                                                            

298 filas seleccionadas.

SQL> 
SQL> -- 19) liste todos los empleados que atienden una bodega y que han hecho una venta.
SQL> 
SQL> SELECT (E.first_name || E.last_name) NOMBRE
  2  FROM (SELECT E.id bodega
  3        FROM s_emp E
  4        WHERE LOWER(E.title)LIKE 'warehouse manager%') BO,
  5        (SELECT E.id vendedor
  6        FROM s_emp E, s_ord O
  7        WHERE E.id = O.stock_clerk) VE, s_emp E
  8  WHERE BO.bodega = VE.vendedor;

ninguna fila seleccionada

SQL> 
SQL> -- 20) Liste el numero de orden e item de las ordenes con mas de 2 item.
SQL> 
SQL> SELECT P.orden ORDEN, R.ITE
  2  FROM  (SELECT O.id orden, COUNT(O.id)
  3         FROM s_ord O, s_item IT
  4         WHERE O.id = IT.ord_id
  5         HAVING COUNT(O.id) > 2
  6         GROUP BY O.id) P, (SELECT O.id ORD, IT.item_id ITE
  7                            FROM s_ord O, s_item IT
  8                            WHERE O.id = IT.ord_id) R
  9  WHERE P.orden = R.ORD
 10  ORDER BY P.orden;

     ORDEN        ITE                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
        99          1                                                                                                                                                                                   
        99          2                                                                                                                                                                                   
        99          3                                                                                                                                                                                   
        99          4                                                                                                                                                                                   
       100          1                                                                                                                                                                                   
       100          2                                                                                                                                                                                   
       100          3                                                                                                                                                                                   
       100          5                                                                                                                                                                                   
       100          7                                                                                                                                                                                   
       100          6                                                                                                                                                                                   
       100          4                                                                                                                                                                                   

     ORDEN        ITE                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
       101          1                                                                                                                                                                                   
       101          3                                                                                                                                                                                   
       101          5                                                                                                                                                                                   
       101          6                                                                                                                                                                                   
       101          7                                                                                                                                                                                   
       101          4                                                                                                                                                                                   
       101          2                                                                                                                                                                                   
       104          1                                                                                                                                                                                   
       104          4                                                                                                                                                                                   
       104          2                                                                                                                                                                                   
       104          3                                                                                                                                                                                   

     ORDEN        ITE                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
       105          1                                                                                                                                                                                   
       105          3                                                                                                                                                                                   
       105          2                                                                                                                                                                                   
       106          1                                                                                                                                                                                   
       106          4                                                                                                                                                                                   
       106          5                                                                                                                                                                                   
       106          6                                                                                                                                                                                   
       106          2                                                                                                                                                                                   
       106          3                                                                                                                                                                                   
       107          1                                                                                                                                                                                   
       107          3                                                                                                                                                                                   

     ORDEN        ITE                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
       107          5                                                                                                                                                                                   
       107          4                                                                                                                                                                                   
       107          2                                                                                                                                                                                   
       108          1                                                                                                                                                                                   
       108          6                                                                                                                                                                                   
       108          7                                                                                                                                                                                   
       108          5                                                                                                                                                                                   
       108          2                                                                                                                                                                                   
       108          4                                                                                                                                                                                   
       108          3                                                                                                                                                                                   
       109          1                                                                                                                                                                                   

     ORDEN        ITE                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
       109          5                                                                                                                                                                                   
       109          7                                                                                                                                                                                   
       109          6                                                                                                                                                                                   
       109          4                                                                                                                                                                                   
       109          2                                                                                                                                                                                   
       109          3                                                                                                                                                                                   

50 filas seleccionadas.

SQL> 
SQL> -- 21) Liste el promedio de salario por nombre de cargo, de los cargos con mas de dos trabajadores.
SQL> 
SQL> SELECT A.cargo CARGO, B.PROM "PROMEDIO SALARIO"
  2  FROM (SELECT E.title cargo, COUNT(E.title) C
  3        FROM s_emp E
  4        HAVING COUNT(E.title)>2
  5        GROUP BY E.title) A, (SELECT E.title CA, AVG(S.payment) PROM
  6                              FROM s_emp E, s_salary S
  7                              WHERE E.id = S.id
  8                              GROUP BY E.title) B
  9  WHERE A.cargo = B.CA
 10  ORDER BY A.cargo;

CARGO                     PROMEDIO SALARIO                                                                                                                                                              
------------------------- ----------------                                                                                                                                                              
Sales Representative            868.932203                                                                                                                                                              
Stock Clerk                         788.25                                                                                                                                                              
Warehouse Manager                   470.15                                                                                                                                                              

SQL> 
SQL> -- 22) Liste los clientes y sus representantes de ventas que tienen mas de 2 clientes.
SQL> 
SQL> SELECT A.nombre NOMBRE_REP, B.clientes CLIENTE
  2  FROM (SELECT (E.first_name ||' '|| E.last_name) nombre, COUNT(E.id) CLI
  3        FROM s_emp E, s_customer C
  4        WHERE E.id = C.sales_rep_id
  5        HAVING COUNT(E.id) > 2
  6        GROUP BY (E.first_name ||' '|| E.last_name))A,
  7        (SELECT (E.first_name ||' '|| E.last_name) nom, C.name clientes
  8         FROM s_emp E, s_customer C
  9         WHERE E.id = C.sales_rep_id)B
 10  WHERE A.nombre = B.nom
 11  ORDER BY A.nombre;

NOMBRE_REP                      CLIENTE                                                                                                                                                                 
------------------------------- ------------------------------                                                                                                                                          
Andre Dumas                     Sportique                                                                                                                                                               
Andre Dumas                     Kam's Sporting Goods                                                                                                                                                    
Andre Dumas                     Kuhn's Sports                                                                                                                                                           
Andre Dumas                     Muench Sports                                                                                                                                                           
Andre Dumas                     Sporta Russia                                                                                                                                                           
Colin Magee                     Ojibway Retail                                                                                                                                                          
Colin Magee                     Big John's Sports Emporium                                                                                                                                              
Colin Magee                     Womansport                                                                                                                                                              
Colin Magee                     Beisbol Si!                                                                                                                                                             

9 filas seleccionadas.

SQL> 
SQL> -- 23) Liste el salario promedio de cada representante de ventas (id y nombre), teniendo en cuenta que la comision se asigna sobre las ventas del mes.
SQL> 
SQL> SELECT A.id, A.nombre, (A.AA+B.PAGO)/12 AWS
  2  FROM (SELECT E.id id, E.first_name nombre, SUM(S.payment) AA
  3        FROM s_emp E, s_salary S
  4        WHERE E.id = S.id AND LOWER(E.title) LIKE 'sales representative%'
  5        GROUP BY E.first_name, E.id) A,
  6        (SELECT O.sales_rep_id I, SUM(O.total)*MIN(E.commission_pct)/100 pago
  7        FROM s_emp E, s_ord O
  8        WHERE E.id = O. sales_rep_id
  9        GROUP BY O.sales_rep_id) B
 10  WHERE A.id = B.I;

        ID NOMBRE                 AWS                                                                                                                                                                   
---------- --------------- ----------                                                                                                                                                                   
        11 Colin           14488.8031                                                                                                                                                                   
        12 Henry           1753.41667                                                                                                                                                                   
        13 Yasmin          2127.41667                                                                                                                                                                   
        14 Mai             1184.39917                                                                                                                                                                   
        15 Andre           3461.56458                                                                                                                                                                   

SQL> 
SQL> -- 24) Generar un listado con el promedio devengado (incluidas comisiones) en el 2011 de todos los empleados.
SQL> 
SQL> SELECT A.id CODIGO, (SUM(A.AA)/MIN(V.BB)) promedio
  2  FROM (SELECT E.id id, SUM(S.payment) AA
  3        FROM s_emp E, s_salary S
  4        WHERE E.id = S.id
  5        GROUP BY E.id
  6        UNION
  7        SELECT E.id id, SUM(O.total)*MIN(E.commission_pct)/100 AA
  8        FROM s_emp E, s_ord O
  9        WHERE E.id = O. sales_rep_id
 10        GROUP BY E.id) A, (SELECT E.id WE, COUNT(E.id) BB
 11                           FROM s_emp E, s_salary S
 12                           WHERE E.id = S.id
 13                           GROUP BY E.id) V
 14  WHERE V.WE = A.id
 15  GROUP BY A.id
 16  ORDER BY A.id;

    CODIGO   PROMEDIO                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
         1 25181.8182                                                                                                                                                                                   
         2 10230.7692                                                                                                                                                                                   
         3 8333.33333                                                                                                                                                                                   
         4 8233.33333                                                                                                                                                                                   
         5 6316.66667                                                                                                                                                                                   
         6     467.25                                                                                                                                                                                   
         7 476.916667                                                                                                                                                                                   
         8 494.416667                                                                                                                                                                                   
         9 418.583333                                                                                                                                                                                   
        10 493.583333                                                                                                                                                                                   
        11 14488.8031                                                                                                                                                                                   

    CODIGO   PROMEDIO                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
        12 1912.81818                                                                                                                                                                                   
        13 2127.41667                                                                                                                                                                                   
        14 1184.39917                                                                                                                                                                                   
        15 3461.56458                                                                                                                                                                                   
        16     788.25                                                                                                                                                                                   
        17     788.25                                                                                                                                                                                   
        18     788.25                                                                                                                                                                                   
        19     788.25                                                                                                                                                                                   
        20     788.25                                                                                                                                                                                   
        21     788.25                                                                                                                                                                                   
        22     788.25                                                                                                                                                                                   

    CODIGO   PROMEDIO                                                                                                                                                                                   
---------- ----------                                                                                                                                                                                   
        23     788.25                                                                                                                                                                                   
        24     788.25                                                                                                                                                                                   
        25     788.25                                                                                                                                                                                   

25 filas seleccionadas.

SQL> 
SQL> -- 25) Seleccionar los empleados (nombres) que han ganado en algun mes menos que el promedio del mes.
SQL> 
SQL> SELECT DISTINCT(A.n) EMPLEADO
  2  FROM (SELECT E.first_name n, TO_CHAR(S.datepayment,'MM') m, S.payment pago
  3        FROM s_emp E, s_salary S
  4        WHERE E.id = S.id) A,
  5        (SELECT TO_CHAR(S.datepayment,'MM') MES, AVG(S.payment) prom
  6        FROM s_emp E, s_salary S
  7        WHERE E.id = S.id
  8        GROUP BY TO_CHAR(S.datepayment,'MM')
  9        ORDER BY TO_CHAR(S.datepayment,'MM')) B
 10  WHERE A.m = B.MES AND A.pago < B.prom;

EMPLEADO                                                                                                                                                                                                
---------------                                                                                                                                                                                         
Molly                                                                                                                                                                                                   
Roberta                                                                                                                                                                                                 
Ben                                                                                                                                                                                                     
Antoinette                                                                                                                                                                                              
Marta                                                                                                                                                                                                   
Colin                                                                                                                                                                                                   
Henry                                                                                                                                                                                                   
Yasmin                                                                                                                                                                                                  
Mai                                                                                                                                                                                                     
Andre                                                                                                                                                                                                   
Elena                                                                                                                                                                                                   

EMPLEADO                                                                                                                                                                                                
---------------                                                                                                                                                                                         
George                                                                                                                                                                                                  
Akira                                                                                                                                                                                                   
Vikram                                                                                                                                                                                                  
Chad                                                                                                                                                                                                    
Alexander                                                                                                                                                                                               
Eddie                                                                                                                                                                                                   
Radha                                                                                                                                                                                                   
Bela                                                                                                                                                                                                    
Sylvie                                                                                                                                                                                                  

20 filas seleccionadas.

SQL> 
SQL> -- 26) Seleccionar los empleados que han ganado menos que el promedio de algun departamento.
SQL> 
SQL> SELECT B.NOM EMPLEADOS, B.P PROMEDIO_EM
  2  FROM (SELECT R.id RE, D.name DE, AVG(S.payment) PRO
  3        FROM s_salary S, s_emp E, s_region R, s_dept D
  4        WHERE S.id = E.id AND R.id = D.region_id AND D.id = E.dept_id
  5        AND D.region_id = E.region_id
  6        GROUP BY R.id, D.name) A, (SELECT R.id REG, D.name DEP, E.first_name NOM, AVG(S.payment) P
  7                                   FROM s_salary S, s_emp E, s_region R, s_dept D
  8                                   WHERE S.id = E.id AND R.id = D.region_id AND D.id = E.dept_id
  9                                   AND D.region_id = E.region_id
 10                                   GROUP BY R.id, D.name, E.first_name) B
 11  WHERE A.RE = B.REG AND A.DE = B.DEP AND B.P <ANY A.PRO;

EMPLEADOS       PROMEDIO_EM                                                                                                                                                                             
--------------- -----------                                                                                                                                                                             
Audry            6316.66667                                                                                                                                                                             
Molly                467.25                                                                                                                                                                             
Roberta          476.916667                                                                                                                                                                             
Ben              494.416667                                                                                                                                                                             
Antoinette       418.583333                                                                                                                                                                             
Marta            493.583333                                                                                                                                                                             
Colin                913.25                                                                                                                                                                             
Elena                788.25                                                                                                                                                                             
George               788.25                                                                                                                                                                             
Radha                788.25                                                                                                                                                                             

10 filas seleccionadas.

SQL> 
SQL> -- 27) Seleccionar los productos que han pedido menos que alguna cantidad en stock.
SQL> 
SQL> SELECT DISTINCT(p.name) AS "Producto"
  2  FROM s_product p, s_item i, (SELECT p.id id, i.amount_in_stock cant
  3                               FROM s_product p, s_inventory i
  4      	                     WHERE p.id = product_id
  5                               ORDER BY p.id) t
  6  WHERE p.id = i.product_id AND p.id = t.id
  7  GROUP BY p.name, t.cant
  8  HAVING COUNT(i.quantity) < t.cant;

Producto                                                                                                                                                                                                
--------------------------------------------------                                                                                                                                                      
Bunny Boot                                                                                                                                                                                              
Ace Ski Boot                                                                                                                                                                                            
Pro Ski Boot                                                                                                                                                                                            
Bunny Ski Pole                                                                                                                                                                                          
Ace Ski Pole                                                                                                                                                                                            
Pro Ski Pole                                                                                                                                                                                            
Junior Soccer Ball                                                                                                                                                                                      
World Cup Soccer Ball                                                                                                                                                                                   
World Cup Net                                                                                                                                                                                           
Black Hawk Knee Pads                                                                                                                                                                                    
Black Hawk Elbow Pads                                                                                                                                                                                   

Producto                                                                                                                                                                                                
--------------------------------------------------                                                                                                                                                      
Grand Prix Bicycle                                                                                                                                                                                      
Himalaya Bicycle                                                                                                                                                                                        
Grand Prix Bicycle Tires                                                                                                                                                                                
Himalaya Tires                                                                                                                                                                                          
New Air Pump                                                                                                                                                                                            
Slaker Water Bottle                                                                                                                                                                                     
Safe-T Helmet                                                                                                                                                                                           
Alexeyer Pro Lifting Bar                                                                                                                                                                                
Prostar 10 Pound Weight                                                                                                                                                                                 
Prostar 80 Pound Weight                                                                                                                                                                                 
Prostar 100 Pound Weight                                                                                                                                                                                

Producto                                                                                                                                                                                                
--------------------------------------------------                                                                                                                                                      
Major League Baseball                                                                                                                                                                                   
Chapman Helmet                                                                                                                                                                                          
Griffey Glove                                                                                                                                                                                           
Alomar Glove                                                                                                                                                                                            
Steinbach Glove                                                                                                                                                                                         
Cabrera Bat                                                                                                                                                                                             
Winfield Bat                                                                                                                                                                                            
Puckett Bat                                                                                                                                                                                             
Pro Curling Bar                                                                                                                                                                                         

31 filas seleccionadas.

SQL> 
SQL> -- 28) Seleccionar los clientes que han pedido cantidades mayores que el promedio en US de pedido por orden, por mes o por producto.
SQL> 
SQL> SELECT DISTINCT C.name Cliente
  2  FROM s_customer C, s_ord O, s_item I,
  3       (SELECT avg(I.quantity) Pro1
  4       FROM s_customer C, s_ord O, s_item I
  5   	 WHERE C.id = O.customer_id AND
  6             O.id = I.ord_id AND
  7       	   Upper(C.country) LIKE 'USA%'
  8        	   GROUP BY O.id) TEP1
  9  WHERE C.id = O.customer_id AND
 10        O.id = I.ord_id
 11  GROUP BY C.name, TEP1.Pro1
 12  HAVING sum(I.quantity) >  TEP1.Pro1;

CLIENTE                                                                                                                                                                                                 
------------------------------                                                                                                                                                                          
Unisports                                                                                                                                                                                               
Womansport                                                                                                                                                                                              
Big John's Sports Emporium                                                                                                                                                                              
Delhi Sports                                                                                                                                                                                            
Kam's Sporting Goods                                                                                                                                                                                    
Sportique                                                                                                                                                                                               
Muench Sports                                                                                                                                                                                           
Beisbol Si!                                                                                                                                                                                             
Futbol Sonora                                                                                                                                                                                           
Kuhn's Sports                                                                                                                                                                                           
Hamada Sport                                                                                                                                                                                            

CLIENTE                                                                                                                                                                                                 
------------------------------                                                                                                                                                                          
Ojibway Retail                                                                                                                                                                                          

12 filas seleccionadas.

SQL> WHERE C.id = O.customer_id
SP2-0734: inicio "WHERE C.id..." de comando desconocido - resto de la l�nea ignorado.
SQL> UNION
SP2-0042: comando desconocido "UNION" - resto de la l�nea ignorado.
SQL> SELECT DISTINCT C.name Cliente
  2  FROM s_customer C, s_ord O, s_item I,
  3      (SELECT avg(I.quantity) pro2
  4     	 FROM s_customer C, s_ord O, s_item I
  5  	 WHERE C.id = O.customer_id AND
  6             O.id = I.ord_id AND
  7       	   Upper(C.country) LIKE 'USA%'
  8       	   GROUP BY to_char(O.date_ordered, 'MM')) TEP2
  9  WHERE C.id = O.customer_id AND
 10        O.id = I.ord_id
 11  GROUP BY C.name, TEP2.Pro2
 12  HAVING sum(I.quantity) >  TEP2.Pro2
 13  UNION
 14  SELECT DISTINCT C.name Cliente
 15  FROM s_customer C, s_ord O, s_item I,
 16      (SELECT avg(I.quantity) Pro3
 17      FROM s_customer C, s_ord O, s_item I
 18      WHERE C.id = O.customer_id AND
 19        O.id = I.ord_id AND
 20    	  Upper(C.country) LIKE 'USA%'
 21    	  GROUP BY I.product_id) TEP3
 22  WHERE C.id = O.customer_id AND
 23        O.id = I.ord_id
 24  GROUP BY C.name, TEP3.pro3
 25  HAVING sum(I.quantity) > TEP3.pro3;

CLIENTE                                                                                                                                                                                                 
------------------------------                                                                                                                                                                          
Unisports                                                                                                                                                                                               
Womansport                                                                                                                                                                                              
Big John's Sports Emporium                                                                                                                                                                              
Futbol Sonora                                                                                                                                                                                           
Kuhn's Sports                                                                                                                                                                                           
Hamada Sport                                                                                                                                                                                            
Beisbol Si!                                                                                                                                                                                             
Ojibway Retail                                                                                                                                                                                          
Delhi Sports                                                                                                                                                                                            
Muench Sports                                                                                                                                                                                           
Kam's Sporting Goods                                                                                                                                                                                    

CLIENTE                                                                                                                                                                                                 
------------------------------                                                                                                                                                                          
Sportique                                                                                                                                                                                               

12 filas seleccionadas.

SQL> spool off
