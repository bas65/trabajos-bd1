SQL> -- Andres Felipe Wilches Torres - 20172020114
SQL> -- 1) Liste el apellido, el id del departamento y el nombre del departamento de todos los empleados
SQL> SELECT DISTINCT S_EMP.last_name, S_EMP.dept_Id, S_DEPT.name FROM S_EMP JOIN S_DEPT ON S_EMP.dept_Id = S_DEPT.ID;

LAST_NAME          DEPT_ID NAME                                                                                                   
--------------- ---------- ---------------                                                                                        
Velasquez               50 Administration                                                                                         
Ngao                    40 Operations                                                                                             
Nagayama                30 Sales                                                                                                  
Quick-To-See            10 Finance                                                                                                
Ropeburn                50 Administration                                                                                         
Urguhart                40 Operations                                                                                             
Menchu                  40 Operations                                                                                             
Biri                    40 Operations                                                                                             
Catchpole               40 Operations                                                                                             
Havel                   40 Operations                                                                                             
Magee                   30 Sales                                                                                                  

LAST_NAME          DEPT_ID NAME                                                                                                   
--------------- ---------- ---------------                                                                                        
Giljum                  30 Sales                                                                                                  
Sedeghi                 30 Sales                                                                                                  
Nguyen                  30 Sales                                                                                                  
Dumas                   30 Sales                                                                                                  
Mandell                 40 Operations                                                                                             
Smith                   40 Operations                                                                                             
Nozaki                  40 Operations                                                                                             
Patel                   40 Operations                                                                                             
Newman                  40 Operations                                                                                             
Markarian               40 Operations                                                                                             
Chang                   40 Operations                                                                                             

LAST_NAME          DEPT_ID NAME                                                                                                   
--------------- ---------- ---------------                                                                                        
Patel                   30 Sales                                                                                                  
Dancs                   40 Operations                                                                                             
Schwartz                40 Operations                                                                                             

25 filas seleccionadas.

SQL> -- 2) Liste el id del departamento, el id de la region y el nombre de la region de todos los departamentos
SQL> SELECT S_DEPT.id, S_REGION.ID, S_REGION.name FROM S_REGION JOIN S_DEPT ON S_DEPT.region_id = S_REGION.id;

        ID         ID NAME                                                                                                        
---------- ---------- --------------------------------------------------                                                          
        10          1 North America                                                                                               
        30          1 North America                                                                                               
        30          2 South America                                                                                               
        30          3 Africa / Middle East                                                                                        
        30          4 Asia                                                                                                        
        30          5 Europe                                                                                                      
        40          1 North America                                                                                               
        40          2 South America                                                                                               
        40          3 Africa / Middle East                                                                                        
        40          4 Asia                                                                                                        
        40          5 Europe                                                                                                      

        ID         ID NAME                                                                                                        
---------- ---------- --------------------------------------------------                                                          
        50          1 North America                                                                                               

12 filas seleccionadas.

SQL> -- 3) Liste el apellido, el id del depertamento y el nombre del departamento de los empleados con apellido 'Menchu'
SQL> SELECT DISTINCT S_EMP.last_name, S_EMP.dept_Id,S_DEPT.name FROM S_DEPT JOIN S_EMP ON S_EMP.last_name like '%Menchu%' AND S_DEPT.id = S_EMP.dept_Id;

LAST_NAME          DEPT_ID NAME                                                                                                   
--------------- ---------- ---------------                                                                                        
Menchu                  40 Operations                                                                                             

SQL> /* 4) Liste el id de la region, el nombre de la region, el ide del departamento y el nombre del
SQL> departamento de todos los departamentos que pertenecen a la region de 'North America'*/
SQL> SELECT S_REGION.id "region id", S_REGION.name "region name", S_DEPT.id "dept id", S_DEPT.name "dept name" FROM S_DEPT JOIN S_REGION ON S_REGION.name like '%North America%' AND S_DEPT.region_id = S_REGION.id;

 region id region name                                           dept id dept name                                                
---------- -------------------------------------------------- ---------- ---------------                                          
         1 North America                                              10 Finance                                                  
         1 North America                                              30 Sales                                                    
         1 North America                                              40 Operations                                               
         1 North America                                              50 Administration                                           

SQL> -- 5) Liste los nombres de los representantes de ventas y de todos los clientes, el id y los nombres
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name, S_CUSTOMER.id, S_CUSTOMER.name FROM S_CUSTOMER JOIN S_EMP ON (S_EMP.id = S_CUSTOMER.sales_rep_id) AND S_EMP.title like '%Sales Representative%';

S_EMP.FIRST_NAME||''||S_EMP.LAS         ID NAME                                                                                   
------------------------------- ---------- ------------------------------                                                         
Colin Magee                            204 Womansport                                                                             
Colin Magee                            209 Beisbol Si!                                                                            
Colin Magee                            213 Big John's Sports Emporium                                                             
Colin Magee                            214 Ojibway Retail                                                                         
Henry Giljum                           201 Unisports                                                                              
Henry Giljum                           210 Futbol Sonora                                                                          
Yasmin Sedeghi                         212 Hamada Sport                                                                           
Mai Nguyen                             202 Simms Atheletics                                                                       
Mai Nguyen                             203 Delhi Sports                                                                           
Andre Dumas                            205 Kam's Sporting Goods                                                                   
Andre Dumas                            206 Sportique                                                                              

S_EMP.FIRST_NAME||''||S_EMP.LAS         ID NAME                                                                                   
------------------------------- ---------- ------------------------------                                                         
Andre Dumas                            208 Muench Sports                                                                          
Andre Dumas                            211 Kuhn's Sports                                                                          
Andre Dumas                            215 Sporta Russia                                                                          

14 filas seleccionadas.

SQL> -- 6) Liste el id de los clientes, los nombres de los clientes y el id de las ordenes. De todos los clientes con o sin ordenes
SQL> SELECT S_CUSTOMER.id, S_CUSTOMER.name, S_ORD.id FROM S_CUSTOMER LEFT JOIN S_ORD ON S_CUSTOMER.id = S_ORD.CUSTOMER_ID;

        ID NAME                                   ID                                                                              
---------- ------------------------------ ----------                                                                              
       204 Womansport                            100                                                                              
       205 Kam's Sporting Goods                  101                                                                              
       206 Sportique                             102                                                                              
       208 Muench Sports                         103                                                                              
       208 Muench Sports                         104                                                                              
       209 Beisbol Si!                           105                                                                              
       210 Futbol Sonora                         106                                                                              
       211 Kuhn's Sports                         107                                                                              
       212 Hamada Sport                          108                                                                              
       213 Big John's Sports Emporium            109                                                                              
       214 Ojibway Retail                        110                                                                              

        ID NAME                                   ID                                                                              
---------- ------------------------------ ----------                                                                              
       204 Womansport                            111                                                                              
       201 Unisports                              97                                                                              
       202 Simms Atheletics                       98                                                                              
       203 Delhi Sports                           99                                                                              
       210 Futbol Sonora                         112                                                                              
       215 Sporta Russia                                                                                                          
       207 Sweet Rock Sports                                                                                                      

18 filas seleccionadas.

SQL> -- 7) Selecciona los nombres de los productos con su fecha de recargue
SQL> SELECT S_PRODUCT.name, NVL2(S_INVENTORY.restock_date,to_char(S_INVENTORY.restock_date),'No tiene') "fecha recargue" FROM S_PRODUCT JOIN S_INVENTORY ON S_PRODUCT.ID = S_INVENTORY.product_id;

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Bunny Boot                                         No tiene                                                                       
Ace Ski Boot                                       No tiene                                                                       
Pro Ski Boot                                       No tiene                                                                       
Bunny Ski Pole                                     No tiene                                                                       
Ace Ski Pole                                       No tiene                                                                       
Pro Ski Pole                                       No tiene                                                                       
Junior Soccer Ball                                 No tiene                                                                       
World Cup Soccer Ball                              No tiene                                                                       
World Cup Net                                      No tiene                                                                       
Black Hawk Knee Pads                               No tiene                                                                       
Black Hawk Elbow Pads                              No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Grand Prix Bicycle                                 No tiene                                                                       
Himalaya Bicycle                                   No tiene                                                                       
Grand Prix Bicycle Tires                           No tiene                                                                       
Himalaya Tires                                     No tiene                                                                       
New Air Pump                                       No tiene                                                                       
Slaker Water Bottle                                No tiene                                                                       
Safe-T Helmet                                      No tiene                                                                       
Alexeyer Pro Lifting Bar                           No tiene                                                                       
Pro Curling Bar                                    08/02/11                                                                       
Prostar 10 Pound Weight                            No tiene                                                                       
Prostar 20 Pound Weight                            No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Prostar 50 Pound Weight                            No tiene                                                                       
Prostar 80 Pound Weight                            No tiene                                                                       
Prostar 100 Pound Weight                           No tiene                                                                       
Major League Baseball                              No tiene                                                                       
Chapman Helmet                                     No tiene                                                                       
Griffey Glove                                      No tiene                                                                       
Alomar Glove                                       No tiene                                                                       
Steinbach Glove                                    No tiene                                                                       
Cabrera Bat                                        No tiene                                                                       
Puckett Bat                                        12/04/11                                                                       
Winfield Bat                                       No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Junior Soccer Ball                                 No tiene                                                                       
World Cup Soccer Ball                              No tiene                                                                       
World Cup Net                                      No tiene                                                                       
Black Hawk Knee Pads                               No tiene                                                                       
Black Hawk Elbow Pads                              No tiene                                                                       
Grand Prix Bicycle                                 No tiene                                                                       
Himalaya Bicycle                                   No tiene                                                                       
Grand Prix Bicycle Tires                           No tiene                                                                       
Himalaya Tires                                     No tiene                                                                       
New Air Pump                                       No tiene                                                                       
Slaker Water Bottle                                No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Safe-T Helmet                                      No tiene                                                                       
Major League Baseball                              No tiene                                                                       
Chapman Helmet                                     No tiene                                                                       
Griffey Glove                                      No tiene                                                                       
Alomar Glove                                       No tiene                                                                       
Steinbach Glove                                    No tiene                                                                       
Cabrera Bat                                        No tiene                                                                       
Puckett Bat                                        No tiene                                                                       
Winfield Bat                                       No tiene                                                                       
Black Hawk Knee Pads                               No tiene                                                                       
Black Hawk Elbow Pads                              No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Grand Prix Bicycle                                 No tiene                                                                       
Grand Prix Bicycle Tires                           No tiene                                                                       
New Air Pump                                       No tiene                                                                       
Slaker Water Bottle                                No tiene                                                                       
Safe-T Helmet                                      No tiene                                                                       
Alexeyer Pro Lifting Bar                           No tiene                                                                       
Pro Curling Bar                                    No tiene                                                                       
Prostar 10 Pound Weight                            No tiene                                                                       
Prostar 20 Pound Weight                            No tiene                                                                       
Prostar 50 Pound Weight                            No tiene                                                                       
Prostar 80 Pound Weight                            No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Prostar 100 Pound Weight                           No tiene                                                                       
Black Hawk Knee Pads                               No tiene                                                                       
Black Hawk Elbow Pads                              No tiene                                                                       
Grand Prix Bicycle                                 No tiene                                                                       
Himalaya Bicycle                                   No tiene                                                                       
Grand Prix Bicycle Tires                           No tiene                                                                       
Himalaya Tires                                     No tiene                                                                       
New Air Pump                                       07/09/11                                                                       
Slaker Water Bottle                                No tiene                                                                       
Safe-T Helmet                                      No tiene                                                                       
Alexeyer Pro Lifting Bar                           No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Pro Curling Bar                                    No tiene                                                                       
Prostar 10 Pound Weight                            No tiene                                                                       
Prostar 20 Pound Weight                            No tiene                                                                       
Prostar 50 Pound Weight                            No tiene                                                                       
Prostar 80 Pound Weight                            No tiene                                                                       
Prostar 100 Pound Weight                           No tiene                                                                       
Major League Baseball                              No tiene                                                                       
Chapman Helmet                                     No tiene                                                                       
Griffey Glove                                      No tiene                                                                       
Alomar Glove                                       No tiene                                                                       
Steinbach Glove                                    No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Cabrera Bat                                        No tiene                                                                       
Puckett Bat                                        No tiene                                                                       
Winfield Bat                                       No tiene                                                                       
Ace Ski Boot                                       No tiene                                                                       
Pro Ski Boot                                       No tiene                                                                       
Ace Ski Pole                                       No tiene                                                                       
Pro Ski Pole                                       No tiene                                                                       
Junior Soccer Ball                                 No tiene                                                                       
World Cup Soccer Ball                              No tiene                                                                       
World Cup Net                                      No tiene                                                                       
Black Hawk Knee Pads                               No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Black Hawk Elbow Pads                              No tiene                                                                       
Grand Prix Bicycle                                 No tiene                                                                       
Himalaya Bicycle                                   No tiene                                                                       
Grand Prix Bicycle Tires                           No tiene                                                                       
Himalaya Tires                                     No tiene                                                                       
New Air Pump                                       No tiene                                                                       
Slaker Water Bottle                                No tiene                                                                       
Safe-T Helmet                                      No tiene                                                                       
Alexeyer Pro Lifting Bar                           No tiene                                                                       
Pro Curling Bar                                    No tiene                                                                       
Prostar 10 Pound Weight                            No tiene                                                                       

NAME                                               fecha re                                                                       
-------------------------------------------------- --------                                                                       
Prostar 20 Pound Weight                            No tiene                                                                       
Prostar 50 Pound Weight                            No tiene                                                                       
Prostar 80 Pound Weight                            No tiene                                                                       
Prostar 100 Pound Weight                           No tiene                                                                       

114 filas seleccionadas.

SQL> -- 8) Seleccionar los empleados con sus departamentos y regiones
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name "empleado", S_DEPT.name "departamento", S_REGION.name "region" FROM S_EMP JOIN S_DEPT ON S_EMP.dept_Id = S_DEPT.id JOIN S_REGION ON S_EMP.region_id = S_REGION.id AND S_DEPT.region_id = S_REGION.id;

empleado                        departamento    region                                                                            
------------------------------- --------------- --------------------------------------------------                                
Carmen Velasquez                Administration  North America                                                                     
LaDoris Ngao                    Operations      North America                                                                     
Midori Nagayama                 Sales           North America                                                                     
Mark Quick-To-See               Finance         North America                                                                     
Audry Ropeburn                  Administration  North America                                                                     
Molly Urguhart                  Operations      North America                                                                     
Roberta Menchu                  Operations      South America                                                                     
Ben Biri                        Operations      Africa / Middle East                                                              
Antoinette Catchpole            Operations      Asia                                                                              
Marta Havel                     Operations      Europe                                                                            
Colin Magee                     Sales           North America                                                                     

empleado                        departamento    region                                                                            
------------------------------- --------------- --------------------------------------------------                                
Henry Giljum                    Sales           South America                                                                     
Yasmin Sedeghi                  Sales           Africa / Middle East                                                              
Mai Nguyen                      Sales           Asia                                                                              
Andre Dumas                     Sales           Europe                                                                            
Elena Mandell                   Operations      North America                                                                     
George Smith                    Operations      North America                                                                     
Akira Nozaki                    Operations      South America                                                                     
Vikram Patel                    Operations      South America                                                                     
Chad Newman                     Operations      Africa / Middle East                                                              
Alexander Markarian             Operations      Africa / Middle East                                                              
Eddie Chang                     Operations      Asia                                                                              

empleado                        departamento    region                                                                            
------------------------------- --------------- --------------------------------------------------                                
Radha Patel                     Sales           Asia                                                                              
Bela Dancs                      Operations      Europe                                                                            
Sylvie Schwartz                 Operations      Europe                                                                            

25 filas seleccionadas.

SQL> -- 9) Seleccionar los productos con sus regiones
SQL> SELECT DISTINCT S_PRODUCT.name "producto", S_REGION.name "region" FROM S_PRODUCT JOIN S_INVENTORY ON S_PRODUCT.id = S_INVENTORY.product_id JOIN S_WAREHOUSE ON S_INVENTORY.warehouse_id = S_WAREHOUSE.id JOIN S_REGION ON S_WAREHOUSE.region_id = S_REGION.id;

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Bunny Boot                                         North America                                                                  
Ace Ski Boot                                       North America                                                                  
Pro Ski Boot                                       North America                                                                  
Bunny Ski Pole                                     North America                                                                  
Ace Ski Pole                                       North America                                                                  
Pro Ski Pole                                       North America                                                                  
Junior Soccer Ball                                 North America                                                                  
World Cup Soccer Ball                              North America                                                                  
World Cup Net                                      North America                                                                  
Black Hawk Knee Pads                               North America                                                                  
Black Hawk Elbow Pads                              North America                                                                  

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Grand Prix Bicycle                                 North America                                                                  
Himalaya Bicycle                                   North America                                                                  
Grand Prix Bicycle Tires                           North America                                                                  
Himalaya Tires                                     North America                                                                  
New Air Pump                                       North America                                                                  
Slaker Water Bottle                                North America                                                                  
Safe-T Helmet                                      North America                                                                  
Alexeyer Pro Lifting Bar                           North America                                                                  
Pro Curling Bar                                    North America                                                                  
Prostar 10 Pound Weight                            North America                                                                  
Prostar 20 Pound Weight                            North America                                                                  

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Prostar 50 Pound Weight                            North America                                                                  
Prostar 80 Pound Weight                            North America                                                                  
Prostar 100 Pound Weight                           North America                                                                  
Major League Baseball                              North America                                                                  
Chapman Helmet                                     North America                                                                  
Griffey Glove                                      North America                                                                  
Alomar Glove                                       North America                                                                  
Steinbach Glove                                    North America                                                                  
Cabrera Bat                                        North America                                                                  
Puckett Bat                                        North America                                                                  
Winfield Bat                                       North America                                                                  

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Junior Soccer Ball                                 South America                                                                  
World Cup Soccer Ball                              South America                                                                  
World Cup Net                                      South America                                                                  
Black Hawk Knee Pads                               South America                                                                  
Black Hawk Elbow Pads                              South America                                                                  
Grand Prix Bicycle                                 South America                                                                  
Himalaya Bicycle                                   South America                                                                  
Grand Prix Bicycle Tires                           South America                                                                  
Himalaya Tires                                     South America                                                                  
New Air Pump                                       South America                                                                  
Slaker Water Bottle                                South America                                                                  

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Safe-T Helmet                                      South America                                                                  
Major League Baseball                              South America                                                                  
Chapman Helmet                                     South America                                                                  
Griffey Glove                                      South America                                                                  
Alomar Glove                                       South America                                                                  
Steinbach Glove                                    South America                                                                  
Cabrera Bat                                        South America                                                                  
Puckett Bat                                        South America                                                                  
Winfield Bat                                       South America                                                                  
Black Hawk Knee Pads                               Africa / Middle East                                                           
Black Hawk Elbow Pads                              Africa / Middle East                                                           

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Grand Prix Bicycle                                 Africa / Middle East                                                           
Grand Prix Bicycle Tires                           Africa / Middle East                                                           
New Air Pump                                       Africa / Middle East                                                           
Slaker Water Bottle                                Africa / Middle East                                                           
Safe-T Helmet                                      Africa / Middle East                                                           
Alexeyer Pro Lifting Bar                           Africa / Middle East                                                           
Pro Curling Bar                                    Africa / Middle East                                                           
Prostar 10 Pound Weight                            Africa / Middle East                                                           
Prostar 20 Pound Weight                            Africa / Middle East                                                           
Prostar 50 Pound Weight                            Africa / Middle East                                                           
Prostar 80 Pound Weight                            Africa / Middle East                                                           

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Prostar 100 Pound Weight                           Africa / Middle East                                                           
Black Hawk Knee Pads                               Asia                                                                           
Black Hawk Elbow Pads                              Asia                                                                           
Grand Prix Bicycle                                 Asia                                                                           
Himalaya Bicycle                                   Asia                                                                           
Grand Prix Bicycle Tires                           Asia                                                                           
Himalaya Tires                                     Asia                                                                           
New Air Pump                                       Asia                                                                           
Slaker Water Bottle                                Asia                                                                           
Safe-T Helmet                                      Asia                                                                           
Alexeyer Pro Lifting Bar                           Asia                                                                           

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Pro Curling Bar                                    Asia                                                                           
Prostar 10 Pound Weight                            Asia                                                                           
Prostar 20 Pound Weight                            Asia                                                                           
Prostar 50 Pound Weight                            Asia                                                                           
Prostar 80 Pound Weight                            Asia                                                                           
Prostar 100 Pound Weight                           Asia                                                                           
Major League Baseball                              Asia                                                                           
Chapman Helmet                                     Asia                                                                           
Griffey Glove                                      Asia                                                                           
Alomar Glove                                       Asia                                                                           
Steinbach Glove                                    Asia                                                                           

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Cabrera Bat                                        Asia                                                                           
Puckett Bat                                        Asia                                                                           
Winfield Bat                                       Asia                                                                           
Ace Ski Boot                                       Europe                                                                         
Pro Ski Boot                                       Europe                                                                         
Ace Ski Pole                                       Europe                                                                         
Pro Ski Pole                                       Europe                                                                         
Junior Soccer Ball                                 Europe                                                                         
World Cup Soccer Ball                              Europe                                                                         
World Cup Net                                      Europe                                                                         
Black Hawk Knee Pads                               Europe                                                                         

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Black Hawk Elbow Pads                              Europe                                                                         
Grand Prix Bicycle                                 Europe                                                                         
Himalaya Bicycle                                   Europe                                                                         
Grand Prix Bicycle Tires                           Europe                                                                         
Himalaya Tires                                     Europe                                                                         
New Air Pump                                       Europe                                                                         
Slaker Water Bottle                                Europe                                                                         
Safe-T Helmet                                      Europe                                                                         
Alexeyer Pro Lifting Bar                           Europe                                                                         
Pro Curling Bar                                    Europe                                                                         
Prostar 10 Pound Weight                            Europe                                                                         

producto                                           region                                                                         
-------------------------------------------------- --------------------------------------------------                             
Prostar 20 Pound Weight                            Europe                                                                         
Prostar 50 Pound Weight                            Europe                                                                         
Prostar 80 Pound Weight                            Europe                                                                         
Prostar 100 Pound Weight                           Europe                                                                         

114 filas seleccionadas.

SQL> -- 10) Seleccionar los representantes de ventas que han hecho ordenes de items con mas de 500 unidades
SQL> SELECT DISTINCT S_EMP.first_name, S_EMP.last_name FROM S_EMP JOIN S_ORD ON S_EMP.id = S_ORD.sales_rep_id AND S_EMP.title like '%Sales Representative%' JOIN S_ITEM ON S_ORD.id = S_ITEM.ord_id AND S_ITEM.quantity > 500;

FIRST_NAME      LAST_NAME                                                                                                         
--------------- ---------------                                                                                                   
Colin           Magee                                                                                                             
Henry           Giljum                                                                                                            

SQL> -- 11) No se hace
SQL> -- 12) Seleccionar los productos que se han vendido y cuentan con un stock de mas de 130 unidades
SQL> SELECT DISTINCT S_PRODUCT.name FROM S_PRODUCT  JOIN S_INVENTORY ON S_PRODUCT.id = S_INVENTORY.product_id AND S_INVENTORY.amount_in_stock >130 JOIN S_ITEM ON S_INVENTORY.product_id = S_ITEM.product_id;

NAME                                                                                                                              
--------------------------------------------------                                                                                
Bunny Boot                                                                                                                        
Ace Ski Boot                                                                                                                      
Pro Ski Boot                                                                                                                      
Bunny Ski Pole                                                                                                                    
Ace Ski Pole                                                                                                                      
Pro Ski Pole                                                                                                                      
Junior Soccer Ball                                                                                                                
World Cup Soccer Ball                                                                                                             
World Cup Net                                                                                                                     
Black Hawk Knee Pads                                                                                                              
Black Hawk Elbow Pads                                                                                                             

NAME                                                                                                                              
--------------------------------------------------                                                                                
Grand Prix Bicycle                                                                                                                
Himalaya Bicycle                                                                                                                  
Grand Prix Bicycle Tires                                                                                                          
Himalaya Tires                                                                                                                    
New Air Pump                                                                                                                      
Slaker Water Bottle                                                                                                               
Safe-T Helmet                                                                                                                     
Alexeyer Pro Lifting Bar                                                                                                          
Prostar 10 Pound Weight                                                                                                           
Prostar 80 Pound Weight                                                                                                           
Prostar 100 Pound Weight                                                                                                          

NAME                                                                                                                              
--------------------------------------------------                                                                                
Major League Baseball                                                                                                             
Chapman Helmet                                                                                                                    
Griffey Glove                                                                                                                     
Alomar Glove                                                                                                                      
Steinbach Glove                                                                                                                   
Cabrera Bat                                                                                                                       
Winfield Bat                                                                                                                      
Puckett Bat                                                                                                                       

30 filas seleccionadas.

SQL> -- 13) Seleccionar los nombres completos de los representantes de ventas
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name "representantes de ventas" FROM S_EMP WHERE S_EMP.title like '%Sales Representative%';

representantes de ventas                                                                                                          
-------------------------------                                                                                                   
Colin Magee                                                                                                                       
Henry Giljum                                                                                                                      
Yasmin Sedeghi                                                                                                                    
Mai Nguyen                                                                                                                        
Andre Dumas                                                                                                                       

SQL> -- 14) Seleccionar los nombre completos de los empleados que no son representantes de ventas
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name "representantes de ventas" FROM S_EMP WHERE S_EMP.title not like '%Sales Representative%';

representantes de ventas                                                                                                          
-------------------------------                                                                                                   
Carmen Velasquez                                                                                                                  
LaDoris Ngao                                                                                                                      
Midori Nagayama                                                                                                                   
Mark Quick-To-See                                                                                                                 
Audry Ropeburn                                                                                                                    
Molly Urguhart                                                                                                                    
Roberta Menchu                                                                                                                    
Ben Biri                                                                                                                          
Antoinette Catchpole                                                                                                              
Marta Havel                                                                                                                       
Elena Mandell                                                                                                                     

representantes de ventas                                                                                                          
-------------------------------                                                                                                   
George Smith                                                                                                                      
Akira Nozaki                                                                                                                      
Vikram Patel                                                                                                                      
Chad Newman                                                                                                                       
Alexander Markarian                                                                                                               
Eddie Chang                                                                                                                       
Radha Patel                                                                                                                       
Bela Dancs                                                                                                                        
Sylvie Schwartz                                                                                                                   

20 filas seleccionadas.

SQL> -- 15) Seleccionar los nombres completos y cargo tanto de los empleados con sus subalternos (listar empleado y subalterno)
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo",S_EMP2.first_name||' '||S_EMP2.last_name "Jefe", S_EMP2.title "posicion" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;

Empleado                        cargo                     Jefe                            posicion                                
------------------------------- ------------------------- ------------------------------- -------------------------               
LaDoris Ngao                    VP, Operations            Carmen Velasquez                President                               
Midori Nagayama                 VP, Sales                 Carmen Velasquez                President                               
Mark Quick-To-See               VP, Finance               Carmen Velasquez                President                               
Audry Ropeburn                  VP, Administration        Carmen Velasquez                President                               
Molly Urguhart                  Warehouse Manager         LaDoris Ngao                    VP, Operations                          
Roberta Menchu                  Warehouse Manager         LaDoris Ngao                    VP, Operations                          
Ben Biri                        Warehouse Manager         LaDoris Ngao                    VP, Operations                          
Antoinette Catchpole            Warehouse Manager         LaDoris Ngao                    VP, Operations                          
Marta Havel                     Warehouse Manager         LaDoris Ngao                    VP, Operations                          
Colin Magee                     Sales Representative      Midori Nagayama                 VP, Sales                               
Henry Giljum                    Sales Representative      Midori Nagayama                 VP, Sales                               

Empleado                        cargo                     Jefe                            posicion                                
------------------------------- ------------------------- ------------------------------- -------------------------               
Yasmin Sedeghi                  Sales Representative      Midori Nagayama                 VP, Sales                               
Mai Nguyen                      Sales Representative      Midori Nagayama                 VP, Sales                               
Andre Dumas                     Sales Representative      Midori Nagayama                 VP, Sales                               
Elena Mandell                   Stock Clerk               Molly Urguhart                  Warehouse Manager                       
George Smith                    Stock Clerk               Molly Urguhart                  Warehouse Manager                       
Akira Nozaki                    Stock Clerk               Roberta Menchu                  Warehouse Manager                       
Vikram Patel                    Stock Clerk               Roberta Menchu                  Warehouse Manager                       
Chad Newman                     Stock Clerk               Ben Biri                        Warehouse Manager                       
Alexander Markarian             Stock Clerk               Ben Biri                        Warehouse Manager                       
Eddie Chang                     Stock Clerk               Antoinette Catchpole            Warehouse Manager                       
Radha Patel                     Stock Clerk               Antoinette Catchpole            Warehouse Manager                       

Empleado                        cargo                     Jefe                            posicion                                
------------------------------- ------------------------- ------------------------------- -------------------------               
Bela Dancs                      Stock Clerk               Marta Havel                     Warehouse Manager                       
Sylvie Schwartz                 Stock Clerk               Marta Havel                     Warehouse Manager                       

24 filas seleccionadas.

SQL> -- 16) Seleccionar los nombres completos y cargo de los empleados que tienen subalternos
SQL> SELECT DISTINCT S_EMP2.first_name||' '||S_EMP2.last_name "Jefe", S_EMP2.title "posicion" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;

Jefe                            posicion                                                                                          
------------------------------- -------------------------                                                                         
Carmen Velasquez                President                                                                                         
LaDoris Ngao                    VP, Operations                                                                                    
Midori Nagayama                 VP, Sales                                                                                         
Molly Urguhart                  Warehouse Manager                                                                                 
Roberta Menchu                  Warehouse Manager                                                                                 
Ben Biri                        Warehouse Manager                                                                                 
Antoinette Catchpole            Warehouse Manager                                                                                 
Marta Havel                     Warehouse Manager                                                                                 

8 filas seleccionadas.

SQL> -- 17) Seleccionar los nombres completos y cargos de todos los empleados con sus subalternos tengan o no tengan subalternos
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name "Nombre", S_EMP.title "Posicion",S_EMP2.first_name||' '||S_EMP2.last_name "Subalterno", S_EMP2.title "posicion" FROM S_EMP LEFT JOIN S_EMP S_EMP2 ON S_EMP.id = S_EMP2.Manager_id;

Nombre                          Posicion                  Subalterno                      posicion                                
------------------------------- ------------------------- ------------------------------- -------------------------               
Carmen Velasquez                President                 LaDoris Ngao                    VP, Operations                          
Carmen Velasquez                President                 Midori Nagayama                 VP, Sales                               
Carmen Velasquez                President                 Mark Quick-To-See               VP, Finance                             
Carmen Velasquez                President                 Audry Ropeburn                  VP, Administration                      
LaDoris Ngao                    VP, Operations            Molly Urguhart                  Warehouse Manager                       
LaDoris Ngao                    VP, Operations            Roberta Menchu                  Warehouse Manager                       
LaDoris Ngao                    VP, Operations            Ben Biri                        Warehouse Manager                       
LaDoris Ngao                    VP, Operations            Antoinette Catchpole            Warehouse Manager                       
LaDoris Ngao                    VP, Operations            Marta Havel                     Warehouse Manager                       
Midori Nagayama                 VP, Sales                 Colin Magee                     Sales Representative                    
Midori Nagayama                 VP, Sales                 Henry Giljum                    Sales Representative                    

Nombre                          Posicion                  Subalterno                      posicion                                
------------------------------- ------------------------- ------------------------------- -------------------------               
Midori Nagayama                 VP, Sales                 Yasmin Sedeghi                  Sales Representative                    
Midori Nagayama                 VP, Sales                 Mai Nguyen                      Sales Representative                    
Midori Nagayama                 VP, Sales                 Andre Dumas                     Sales Representative                    
Molly Urguhart                  Warehouse Manager         Elena Mandell                   Stock Clerk                             
Molly Urguhart                  Warehouse Manager         George Smith                    Stock Clerk                             
Roberta Menchu                  Warehouse Manager         Akira Nozaki                    Stock Clerk                             
Roberta Menchu                  Warehouse Manager         Vikram Patel                    Stock Clerk                             
Ben Biri                        Warehouse Manager         Chad Newman                     Stock Clerk                             
Ben Biri                        Warehouse Manager         Alexander Markarian             Stock Clerk                             
Antoinette Catchpole            Warehouse Manager         Eddie Chang                     Stock Clerk                             
Antoinette Catchpole            Warehouse Manager         Radha Patel                     Stock Clerk                             

Nombre                          Posicion                  Subalterno                      posicion                                
------------------------------- ------------------------- ------------------------------- -------------------------               
Marta Havel                     Warehouse Manager         Bela Dancs                      Stock Clerk                             
Marta Havel                     Warehouse Manager         Sylvie Schwartz                 Stock Clerk                             
Radha Patel                     Stock Clerk                                                                                       
Mai Nguyen                      Sales Representative                                                                              
Andre Dumas                     Sales Representative                                                                              
Colin Magee                     Sales Representative                                                                              
Henry Giljum                    Sales Representative                                                                              
George Smith                    Stock Clerk                                                                                       
Alexander Markarian             Stock Clerk                                                                                       
Mark Quick-To-See               VP, Finance                                                                                       
Audry Ropeburn                  VP, Administration                                                                                

Nombre                          Posicion                  Subalterno                      posicion                                
------------------------------- ------------------------- ------------------------------- -------------------------               
Akira Nozaki                    Stock Clerk                                                                                       
Chad Newman                     Stock Clerk                                                                                       
Eddie Chang                     Stock Clerk                                                                                       
Yasmin Sedeghi                  Sales Representative                                                                              
Vikram Patel                    Stock Clerk                                                                                       
Sylvie Schwartz                 Stock Clerk                                                                                       
Elena Mandell                   Stock Clerk                                                                                       
Bela Dancs                      Stock Clerk                                                                                       

41 filas seleccionadas.

SQL> -- 18) Seleccionar los nombres completos y cargos de todos los empleados que no tienen subalternos
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_EMP LEFT JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id MINUS SELECT DISTINCT S_EMP2.first_name||' '||S_EMP2.last_name "Jefe", S_EMP2.title "posicion" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;

Empleado                        cargo                                                                                             
------------------------------- -------------------------                                                                         
Mark Quick-To-See               VP, Finance                                                                                       
Audry Ropeburn                  VP, Administration                                                                                
Colin Magee                     Sales Representative                                                                              
Henry Giljum                    Sales Representative                                                                              
Yasmin Sedeghi                  Sales Representative                                                                              
Mai Nguyen                      Sales Representative                                                                              
Andre Dumas                     Sales Representative                                                                              
Elena Mandell                   Stock Clerk                                                                                       
George Smith                    Stock Clerk                                                                                       
Akira Nozaki                    Stock Clerk                                                                                       
Vikram Patel                    Stock Clerk                                                                                       

Empleado                        cargo                                                                                             
------------------------------- -------------------------                                                                         
Chad Newman                     Stock Clerk                                                                                       
Alexander Markarian             Stock Clerk                                                                                       
Eddie Chang                     Stock Clerk                                                                                       
Radha Patel                     Stock Clerk                                                                                       
Bela Dancs                      Stock Clerk                                                                                       
Sylvie Schwartz                 Stock Clerk                                                                                       

17 filas seleccionadas.

SQL> -- 19) Seleccionar los nombres completos y cargo de los subalternos que no tienen jefes
SQL> -- SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_emp WHERE S_EMP.Manager_id is null;
SQL> SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_EMP LEFT JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id MINUS SELECT S_EMP.first_name||' '||S_EMP.last_name "Empleado", S_EMP.title "cargo" FROM S_EMP JOIN S_EMP S_EMP2 ON S_EMP2.id = S_EMP.Manager_id;

Empleado                        cargo                                                                                             
------------------------------- -------------------------                                                                         
Carmen Velasquez                President                                                                                         

SQL> -- 20) Seleccionar los productos que no estan en el inventario
SQL> SELECT DISTINCT S_PRODUCT.name FROM S_PRODUCT JOIN S_INVENTORY ON S_PRODUCT.id = S_INVENTORY.product_id AND S_INVENTORY.amount_in_stock = 0;

NAME                                                                                                                              
--------------------------------------------------                                                                                
New Air Pump                                                                                                                      
Pro Curling Bar                                                                                                                   
Puckett Bat                                                                                                                       

SQL> -- 21) Seleccionar los productos que nunca se han pedido
SQL> SELECT DISTINCT S_PRODUCT.name FROM S_PRODUCT JOIN S_ITEM ON S_PRODUCT.id = S_ITEM.product_id(+) AND S_ITEM.product_id is NULL;

NAME                                                                                                                              
--------------------------------------------------                                                                                
Prostar 20 Pound Weight                                                                                                           
Prostar 50 Pound Weight                                                                                                           

SQL> -- 22) Seleccionar las regiones que no tienen productos en el inventario
SQL> SELECT DISTINCT S_REGION.name FROM S_REGION JOIN S_WAREHOUSE ON S_REGION.id = S_WAREHOUSe.region_id JOIN S_INVENTORY ON S_WAREHOUSE.id = S_INVENTORY.warehouse_id JOIN S_PRODUCT ON S_INVENTORY.product_id = S_PRODUCT.id AND S_INVENTORY.amount_in_stock = 0;

NAME                                                                                                                              
--------------------------------------------------                                                                                
North America                                                                                                                     
Asia                                                                                                                              

SQL> -- 23) No se hace
SQL> -- 24) Generar un listado de los empleados indicando si ganan o no comision
SQL> SELECT S_EMP.first_name, S_EMP.last_name, nvl2(S_EMP.commission_pct,'Ganan comision','No ganan comision') FROM S_EMP;

FIRST_NAME      LAST_NAME       NVL2(S_EMP.COMMIS                                                                                 
--------------- --------------- -----------------                                                                                 
Carmen          Velasquez       No ganan comision                                                                                 
LaDoris         Ngao            No ganan comision                                                                                 
Midori          Nagayama        No ganan comision                                                                                 
Mark            Quick-To-See    No ganan comision                                                                                 
Audry           Ropeburn        No ganan comision                                                                                 
Molly           Urguhart        No ganan comision                                                                                 
Roberta         Menchu          No ganan comision                                                                                 
Ben             Biri            No ganan comision                                                                                 
Antoinette      Catchpole       No ganan comision                                                                                 
Marta           Havel           No ganan comision                                                                                 
Colin           Magee           Ganan comision                                                                                    

FIRST_NAME      LAST_NAME       NVL2(S_EMP.COMMIS                                                                                 
--------------- --------------- -----------------                                                                                 
Henry           Giljum          Ganan comision                                                                                    
Yasmin          Sedeghi         Ganan comision                                                                                    
Mai             Nguyen          Ganan comision                                                                                    
Andre           Dumas           Ganan comision                                                                                    
Elena           Mandell         No ganan comision                                                                                 
George          Smith           No ganan comision                                                                                 
Akira           Nozaki          No ganan comision                                                                                 
Vikram          Patel           No ganan comision                                                                                 
Chad            Newman          No ganan comision                                                                                 
Alexander       Markarian       No ganan comision                                                                                 
Eddie           Chang           No ganan comision                                                                                 

FIRST_NAME      LAST_NAME       NVL2(S_EMP.COMMIS                                                                                 
--------------- --------------- -----------------                                                                                 
Radha           Patel           No ganan comision                                                                                 
Bela            Dancs           No ganan comision                                                                                 
Sylvie          Schwartz        No ganan comision                                                                                 

25 filas seleccionadas.

SQL> spool off
