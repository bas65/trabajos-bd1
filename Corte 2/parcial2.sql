-- Liste el nombre completo de los representantes de ventas que hicieron ordenes entre el segundo y el sexto día del mes

SELECT DISTINCT e.first_name||' '||e.last_name FROM S_EMP e,S_ORD o WHERE e.title like '%Sales Representative%' AND to_char(o.date_ordered,'dd') > 1 AND to_char(o.date_ordered,'dd') <7;