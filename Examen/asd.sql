-- 1) Seleccionar los nombres de los clientes que tienen por lo menos 30 % de las ordenes de la compañia

-- Subselect con la totalidad de ordenes de la compañia
SELECT COUNT(ord.id) ordenes 
FROM S_ORD ord;

-- Subselect de ordenes por clientes
SELECT cli.id cliente ,COUNT(orde.id) ordenes 
FROM S_ORD orde, S_CUSTOMER cli
WHERE orde.customer_id = cli.id
GROUP BY cli.id;

-- Select principal
SELECT c.name
FROM S_CUSTOMER c, (SELECT COUNT(ord.id) ordenes 
                FROM S_ORD ord) totOrdenes,
                (SELECT cli.id cliente ,COUNT(orde.id) ordenes 
                FROM S_ORD orde, S_CUSTOMER cli
                WHERE orde.customer_id = cli.id
                GROUP BY cli.id) ordClie
WHERE c.id = ordClie.cliente AND ordClie.ordenes >= totOrdenes.ordenes*0.30;

-- 2) Listar nombre completo de los representantes de ventas y
-- promedio de ventas de aquellos cuyo promedio de ventas sea tanto 
-- como las ventas totales de los representantes de sudamerica

-- Subselect de las ventas totales de los representantes de sudamerica
SELECT SUM(ord.total) total
FROM S_EMP emp, S_ORD ord, S_DEPT dept, S_REGION reg
WHERE emp.id = ord.sales_rep_id AND emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id AND LOWER(reg.name) LIKE '%south america%';

-- Select principal
SELECT e.first_name||' '||e.last_name AS "Empleado", AVG(o.total) AS "Promedio"
FROM S_EMP e, S_ORD o, (SELECT SUM(ord.total) total
                        FROM S_EMP emp, S_ORD ord, S_DEPT dept, S_REGION reg
                        WHERE emp.id = ord.sales_rep_id AND emp.dept_id = dept.id AND emp.region_id = reg.id AND dept.region_id = reg.id AND LOWER(reg.name) LIKE '%south america%') ventasSur
WHERE e.id = o.sales_rep_id
HAVING AVG(o.total) >= ventasSur.total
GROUP BY e.first_name||' '||e.last_name, ventasSur.total;

-- 3) Seleccionar los nombres de las  regiones que en promedio han vendio mas productos que los vendidos por la región de asia

-- Subselect de productos vendidos en la region de asia

SELECT SUM(ord.total)
FROM S_PRODUCT pro, S_ITEM ite, S_ORD ord, S_REGION reg, S_WAREHOUSE war, S_INVENTORY inv
WHERE pro.id = ite.product_id AND ite.ord_id = ord.id AND pro.id = inv.product_id AND inv.warehouse_id = war.id AND war.region_id = reg.id AND LOWER(reg.name) LIKE '%asia%';

-- Select regiones promedio ventas
SELECT r.name AS "Region", AVG(o.total)
FROM S_PRODUCT p, S_ITEM i, S_ORD o, S_REGION r, S_WAREHOUSE w, S_INVENTORY inve
WHERE p.id = i.product_id AND i.ord_id = o.id AND p.id = inve.product_id AND inve.warehouse_id = w.id AND w.region_id = r.id
HAVING AVG(o.total) > (SELECT AVG(ord.total)
                        FROM S_PRODUCT pro, S_ITEM ite, S_ORD ord, S_REGION reg, S_WAREHOUSE war, S_INVENTORY inv
                        WHERE pro.id = ite.product_id AND ite.ord_id = ord.id AND pro.id = inv.product_id AND inv.warehouse_id = war.id AND war.region_id = reg.id AND LOWER(reg.name) LIKE '%asia%')
GROUP BY r.name;                        

-- 4) Seleccionar el nombre de las regiones que en promedio vendieron por lo menos tanto como el 30% de la mayor venta hecha enla compañía

-- Subconsulta de la mayor venta hecha en la compañia
SELECT MAX(ord.total)
FROM S_ORD ord;

-- Select principal
SELECT r.name AS "Region", AVG(o.total)
FROM S_REGION r,S_EMP e,S_DEPT d,S_ORD o
WHERE e.id = o.sales_rep_id AND e.dept_id = d.id AND e.region_id = r.id AND d.region_id = r.id
HAVING AVG(o.total) >= (SELECT MAX(ord.total)
                        FROM S_ORD ord) * 0.30
GROUP BY r.name;

-- 5) Liste la identificación de las Bodegas que cuentan en promedio  con por lo menos 
-- tantos productos como el total de los relacionados con ski o el total de los relacionados con bicycle

-- Productos relacionados con ski
SELECT pro.id
FROM S_PRODUCT pro
WHERE LOWER(pro.name) LIKE '%ski%';

SELECT prod.id
FROM S_PRODUCT prod
WHERE LOWER(prod.name) LIKE '%bicycle%';

SELECT w.id AS "Bodega", COUNT(p.id)
FROM S_PRODUCT p, S_INVENTORY inv, S_WAREHOUSE w, (SELECT prod.id prod
                                                    FROM S_PRODUCT prod
                                                    WHERE LOWER(prod.name) LIKE '%bicycle%') bici,
                                                   (SELECT pro.id prod
                                                    FROM S_PRODUCT pro
                                                    WHERE LOWER(pro.name) LIKE '%ski%') ski
WHERE p.id = inv.product_id AND inv.warehouse_id = w.id
HAVING AVG(p.id) >= COUNT(ski.prod) OR AVG(p.id) >= COUNT(bici.prod)
GROUP BY w.id;

--Seleccionar el nombre de los productos cuya cantidad total en stock 
--(del producto) es superior al promedio en stock del mismo producto por region.

-- Promedio en stock del mismo producto por region
SELECT reg.id region, AVG(inv.amount_in_stock) promedio
FROM S_PRODUCT pro, S_INVENTORY inv, S_WAREHOUSE war, S_REGION reg
WHERE pro.id = inv.product_id AND inv.warehouse_id = war.id AND war.region_id = reg.id
GROUP BY reg.id;

-- Select principal
SELECT p.name, SUM(inve.amount_in_stock)
FROM S_PRODUCT p, S_INVENTORY inve, S_WAREHOUSE w
WHERE p.id = inve.product_id AND inve.warehouse_id = w.id
HAVING SUM(inve.amount_in_stock) > ANY (SELECT AVG(inv.amount_in_stock) promedio
                                    FROM S_PRODUCT pro, S_INVENTORY inv, S_WAREHOUSE war, S_REGION reg
                                    WHERE pro.id = inv.product_id AND inv.warehouse_id = war.id AND war.region_id = reg.id
                                    GROUP BY reg.id)
GROUP BY p.name;            