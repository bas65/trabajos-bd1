-- Andres Felipe Wilches Torres - 20172020114
-- Examen final

-- Liste los nombre de los productos que tienen en promedio mas unidades que los que
-- en promedio tiene la bodega 301

-- Subconsulta con el promedio de unidades que tiene la bodega 301
SELECT SUM(inv.amount_in_stock)
FROM S_PRODUCT pro, S_INVENTORY inv, S_WAREHOUSE war
WHERE pro.id = inv.product_id AND inv.warehouse_id = war.id AND war.id LIKE '%301%'
GROUP BY war.id;

-- SELECT PRINCIPAL

SELECT p.name AS "Producto", AVG(i.amount_in_stock)
FROM S_PRODUCT p, S_INVENTORY i
WHERE p.id = i.product_id
HAVING AVG(i.amount_in_stock) >= (SELECT SUM(inv.amount_in_stock)
                                FROM S_PRODUCT pro, S_INVENTORY inv, S_WAREHOUSE war
                                WHERE pro.id = inv.product_id AND inv.warehouse_id = war.id AND war.id LIKE '%301%'
                                GROUP BY war.id)
GROUP BY p.name;                                
